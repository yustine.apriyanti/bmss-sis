﻿Imports MySql.Data.MySqlClient

Public Class test
    Inherits System.Web.UI.Page

    Dim oConn As New MySqlConnection(ConfigurationManager.ConnectionStrings("conn_str").ConnectionString)

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub


    Protected Sub ddl_supplier_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddl_supplier.SelectedIndexChanged
        Dim sqlCmd As New MySqlCommand("select * from supplier where supplier_id = " & ddl_supplier.SelectedValue.ToString() & " ", oConn)
        oConn.Open()
        Dim oDatareader As MySqlDataReader
        oDatareader = sqlCmd.ExecuteReader()
        If oDatareader.HasRows() Then
            If oDatareader.Read() Then
                txt_address.Text = oDatareader("Address").ToString()
                txt_phone.Text = oDatareader("Phone").ToString()
                txt_fax.Text = oDatareader("Fax").ToString()
                txt_contactPerson.Text = oDatareader("contact_name").ToString()
            End If
        End If
        oDatareader.Close()
        oConn.Close()
    End Sub

    Sub LoadRblCatalog()
        'rbl_catalog.ClearSelection()
        ddl_uop.ClearSelection()
        ddl_merk.ClearSelection()
        txt_item_name.Text = ""
        txt_qty.Text = ""
        txt_unit_price.Text = ""

        If rbl_catalog.SelectedValue = "non" Then
            ddl_catalog.Enabled = False
            ddl_catalog.CssClass = "form-control"
            txt_item_name.ReadOnly = False
            ddl_merk.Enabled = True
            ddl_uop.Enabled = True
        Else
            ddl_catalog.Enabled = True
            txt_item_name.ReadOnly = True
            ddl_merk.Enabled = False
            ddl_merk.CssClass = "form-control"
            ddl_uop.Enabled = False
            ddl_uop.CssClass = "form-control"
        End If
    End Sub

    Protected Sub rbl_catalog_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles rbl_catalog.SelectedIndexChanged
        'If rbl_catalog.SelectedValue = "non" Then
        '    ddl_catalog.Enabled = False
        '    ddl_catalog.CssClass = "form-control"
        '    txt_item_name.ReadOnly = False
        '    'ddl_merk.Enabled = True
        '    'ddl_uop.Enabled = True
        'Else
        '    ddl_catalog.Enabled = True
        '    txt_item_name.ReadOnly = True
        '    'ddl_merk.Enabled = False
        '    'ddl_uop.Enabled = False
        'End If
        LoadRblCatalog()
    End Sub

    Protected Sub ddl_catalog_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddl_catalog.SelectedIndexChanged
        Dim sqlCmd As New MySqlCommand("select * from catalog where catalog_id = " & ddl_catalog.SelectedValue.ToString() & " ", oConn)
        oConn.Open()
        Dim oDatareader As MySqlDataReader
        oDatareader = sqlCmd.ExecuteReader()
        If oDatareader.HasRows() Then
            If oDatareader.Read() Then
                txt_item_name.Text = oDatareader("item_name").ToString()
                'ddl_merk.SelectedValue = oDatareader("merk").ToString()
                'ddl_uop.SelectedValue = oDatareader("uop").ToString()
            End If
        End If
        oDatareader.Close()
        oConn.Close()
    End Sub

    Protected Sub btn_addItems_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_addItems.Click
        txt_unit_price.Text = "test"
    End Sub

    Protected Sub btn_calculate_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_calculate.Click
        txt_qty.Text = "test"
    End Sub
End Class