﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/WhiteDesign.Master" CodeBehind="PurchaseOrderModify.aspx.vb" Inherits="BMSS.PurchaseOrderModify" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Purchase Order Modify</h1>
    </div>
</div>
<asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <div class="row">
            <form role="form">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-body" id="panel_search_po" visible="false">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>PO No</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <asp:TextBox ID="txt_po_no" runat="server" class="form-control"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="col-lg-2">
                                        <div class="form-group">
                                            <asp:Button ID="btn_search" runat="server" Text="Search" class="btn btn-primary"/> 
                                            <asp:Label ID="lbl_msg_search" runat="server" Text="" class="text-warning"></asp:Label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-heading">
                            <label>Supplier Data</label>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Company Name</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <asp:DropDownList ID="ddl_supplier" runat="server" class="form-control" EmptyMessage="Select" 
                                            DataSourceID="sql_supplier" DataTextField="CompanyName" 
                                            DataValueField="supplier_id" ResolvedRenderMode="Classic" AutoPostBack="true" autofocus  AppendDataBoundItems="True">
                                                <asp:ListItem Value="-1" Selected="True">-- Select Supplier -- </asp:ListItem>
                                            </asp:DropDownList> 
                                            <asp:SqlDataSource ID="sql_supplier" runat="server" 
                                                ConnectionString="<%$ ConnectionStrings:conn_str %>" 
                                                ProviderName="<%$ ConnectionStrings:conn_str.ProviderName %>" 
                                                SelectCommand="SELECT * from supplier order by CompanyName asc">
                                            </asp:SqlDataSource>  
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Address</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <asp:TextBox ID="txt_address" runat="server" class="form-control" 
                                                TextMode="MultiLine" Height="75px" ReadOnly="True" ></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Phone</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <asp:TextBox ID="txt_phone" runat="server" class="form-control" 
                                                ReadOnly="True"  ></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Fax</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <asp:TextBox ID="txt_fax" runat="server" class="form-control" ReadOnly="True"  ></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Contact Person</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <asp:TextBox ID="txt_contactPerson" runat="server" class="form-control" 
                                                ReadOnly="True"  ></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="panel-heading">
                            <label>Purchase Order Data</label>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>PO No</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <asp:TextBox ID="TextBox1" runat="server" class="form-control" ReadOnly="true" ></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Company Name</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <asp:DropDownList ID="ddl_company" runat="server" class="form-control"
                                            DataSourceID="sql_company" DataTextField="description" 
                                            DataValueField="code" ResolvedRenderMode="Classic" AppendDataBoundItems="True">
                                                <asp:ListItem Value="-1" Selected="True">-- Select company -- </asp:ListItem>
                                            </asp:DropDownList> 
                                            <asp:SqlDataSource ID="sql_company" runat="server" 
                                                ConnectionString="<%$ ConnectionStrings:conn_str %>" 
                                                ProviderName="<%$ ConnectionStrings:conn_str.ProviderName %>" 
                                                SelectCommand="SELECT cat_id, code, description FROM code_list WHERE (cat_id ='COM') and active = 1 order by seq_no ASC">
                                            </asp:SqlDataSource>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>PO Date</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <telerik:RadDatePicker ID="rd_poDate" runat="server" Culture="en-US" CssClass="form-control">
                                                <Calendar ID="Calendar1" UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" ViewSelectorText="x" runat="server"></Calendar>
                                                <DateInput ID="DateInput1" DisplayDateFormat="yyyy-MM-dd" DateFormat="yyyy-MM-dd" runat="server"></DateInput>
                                                <DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>
                                            </telerik:RadDatePicker> 
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>PR No</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <asp:TextBox ID="txt_pr_no" runat="server" class="form-control"  ></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>PR Date</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <telerik:RadDatePicker ID="rd_prDate" runat="server" Culture="en-US" CssClass="form-control">
                                                <Calendar ID="Calendar2" UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" ViewSelectorText="x" runat="server"></Calendar>
                                                <DateInput ID="DateInput2" DisplayDateFormat="yyyy-MM-dd" DateFormat="yyyy-MM-dd" runat="server"></DateInput>
                                                <DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>
                                            </telerik:RadDatePicker>     
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Est. Delivery Date</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <telerik:RadDatePicker ID="rd_deliveryDate" runat="server" Culture="en-US" CssClass="form-control">
                                                <Calendar ID="Calendar3" UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" ViewSelectorText="x" runat="server"></Calendar>
                                                <DateInput ID="DateInput3" DisplayDateFormat="yyyy-MM-dd" DateFormat="yyyy-MM-dd" runat="server"></DateInput>
                                                <DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>
                                            </telerik:RadDatePicker>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Payment Terms</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <asp:TextBox ID="txt_payment_term" runat="server" class="form-control"  ></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Delivery Location</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <asp:TextBox ID="txt_del_loc" runat="server" class="form-control" TextMode="MultiLine" Height="75px"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Notes</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <asp:TextBox ID="txt_notes" runat="server" class="form-control" TextMode="MultiLine" Height="75px"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Goods</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <asp:RadioButtonList ID="rbl_goods_type" runat="server" 
                                                CssClass="radio-inline">
                                                <asp:ListItem Text="Non IT" Value="Non IT"/>
                                                <asp:ListItem Text="IT" Value="IT"/>
                                            </asp:RadioButtonList>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Currency</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <asp:DropDownList ID="ddl_curr" runat="server" class="form-control"
                                            DataSourceID="sql_currency" DataTextField="code" 
                                            DataValueField="code" ResolvedRenderMode="Classic" AppendDataBoundItems="True">
                                                <asp:ListItem Value="-1" Selected="True">-- Select currency -- </asp:ListItem>
                                            </asp:DropDownList> 
                                            <asp:SqlDataSource ID="sql_currency" runat="server" 
                                                ConnectionString="<%$ ConnectionStrings:conn_str %>" 
                                                ProviderName="<%$ ConnectionStrings:conn_str.ProviderName %>" 
                                                SelectCommand="SELECT cat_id, code, description FROM code_list WHERE (cat_id ='CUR') and active = 1 order by seq_no ASC">
                                            </asp:SqlDataSource>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </ContentTemplate>
    <Triggers>
        
    </Triggers>
</asp:UpdatePanel>
    <%--<div class="row">
        <p align="center"><b>PURCHASE ORDER MODIFY</b></p>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                
                <fieldset>
                    <legend>Items Data</legend>
                    <table>
                        <tr>
                            <td colspan="3">
                                <telerik:RadGrid ID="rg_temp" runat="server" ShowFooter="True" 
                                    AllowAutomaticDeletes="True" DataSourceID="sql_temp" >
                                    <MasterTableView AutoGenerateColumns="False" DataKeyNames="po_item_id" 
                                        DataSourceID="sql_temp">
                                        <Columns>
                                            <telerik:GridBoundColumn DataField="po_item_no" 
                                                FilterControlAltText="Filter po_item_no column" HeaderText="No" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                                SortExpression="po_item_no" UniqueName="po_item_no">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="part_desc" 
                                                FilterControlAltText="Filter part_desc column" HeaderText="Description" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left"
                                                SortExpression="part_desc" UniqueName="part_desc">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Left" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="quantity" DataType="System.Int32" 
                                                FilterControlAltText="Filter quantity column" HeaderText="Quantity" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" 
                                                SortExpression="quantity" UniqueName="quantity">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="unit" 
                                                FilterControlAltText="Filter unit column" HeaderText="Unit" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" 
                                                SortExpression="unit" UniqueName="unit">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="price" DataType="System.Decimal" 
                                                FilterControlAltText="Filter price column" HeaderText="Unit Price" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,##0.#0}" 
                                                SortExpression="price" UniqueName="price">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Right" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="subtotal" DataType="System.Decimal" Aggregate="Sum" FooterAggregateFormatString="{0:###,##0.#0}" FooterStyle-HorizontalAlign="Right"
                                                FilterControlAltText="Filter subtotal column" HeaderText="Total Price" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,##0.#0}" HeaderStyle-HorizontalAlign="Center" 
                                                SortExpression="subtotal" UniqueName="subtotal">
                                                <FooterStyle HorizontalAlign="Right" />
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Right" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridButtonColumn Text="Delete" CommandName="Delete" ButtonType="ImageButton" ConfirmText="Delete this data?" ConfirmDialogType="RadWindow" />
                                        </Columns>
                                    </MasterTableView>
                                    <ClientSettings Selecting-AllowRowSelect="true" EnablePostBackOnRowClick="true">
                                        <Selecting AllowRowSelect="True" />
                                    </ClientSettings>
                                </telerik:RadGrid>
                                <asp:SqlDataSource ID="sql_temp" runat="server" 
                                    ConnectionString="<%$ ConnectionStrings:conn_str %>" 
                                    ProviderName="<%$ ConnectionStrings:conn_str.ProviderName %>" 
                                    SelectCommand="SELECT * FROM purchase_order_item where po_no = @po_no"
                                    DeleteCommand="DELETE FROM purchase_order_item WHERE (po_item_id = @po_item_id)" >
                                    <SelectParameters>
                                        <asp:ControlParameter name="po_no" Type="String" ControlID="rt_poNo"/>
                                    </SelectParameters>
                                </asp:SqlDataSource>
                            </td>
                        </tr>
                        <tr>
                            <td class="td_width1">Item No</td>
                            <td class="td_width2">:</td>
                            <td>
                                <telerik:RadTextBox ID="rt_itemNo" runat="server" Width="175px" ReadOnly="true">
                                </telerik:RadTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>Description</td>
                            <td>:</td>
                            <td>                            
                                <telerik:RadTextBox ID="rt_description" runat="server" TextMode="MultiLine" Height="50px" Width="400px">
                                </telerik:RadTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>Quantity</td>
                            <td>:</td>
                            <td>
                                <telerik:RadNumericTextBox ID="rn_quantity" runat="server" Width="175px">
                                </telerik:RadNumericTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>Unit</td>
                            <td>:</td>
                            <td>
                                <telerik:RadComboBox ID="rc_UOM" runat="server" Width="175px"
                                AutoPostBack="True" EmptyMessage="Select UOM" DropDownAutoWidth="Enabled" 
                                    DataSourceID="sql_uom" DataTextField="Description" DataValueField="Code">
                                </telerik:RadComboBox>&nbsp;
                                <asp:SqlDataSource ID="sql_uom" runat="server" 
                                    ConnectionString="<%$ ConnectionStrings:conn_str %>" 
                                    ProviderName="<%$ ConnectionStrings:conn_str.ProviderName %>" 
                                    SelectCommand="SELECT cat_id, code, description FROM code_list WHERE (cat_id ='UOM') and active = 1 order by code ASC"></asp:SqlDataSource>
                            </td>
                        </tr>
                        <tr>
                            <td>Unit Price</td>
                            <td>:</td>
                            <td>                            
                                <telerik:RadNumericTextBox ID="rn_unitPrice" runat="server" Width="175px">
                                </telerik:RadNumericTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <telerik:RadButton ID="rb_update" runat="server" Text="Update" Visible="false">
                                </telerik:RadButton>&nbsp;
                                <telerik:RadButton ID="rb_addItems" runat="server" Text="Add Items">
                                </telerik:RadButton>
                            </td>
                        </tr>
                    </table>
                </fieldset>
                <fieldset>
                    <legend>Total :</legend>
                    <table>
                        <tr>
                            <td>Discount</td>
                            <td>:</td>
                            <td>
                                <telerik:RadNumericTextBox ID="rn_discount" runat="server" Width="175px">
                                </telerik:RadNumericTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>PPN</td>
                            <td>:</td>
                            <td>
                                <telerik:RadNumericTextBox ID="rn_ppn" runat="server" Width="175px">
                                </telerik:RadNumericTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>Shipping Fee</td>
                            <td>:</td>
                            <td>
                                <telerik:RadNumericTextBox ID="rn_shipping" runat="server" Width="175px">
                                </telerik:RadNumericTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>Total</td>
                            <td>:</td>
                            <td>
                                <telerik:RadNumericTextBox ID="rn_total" runat="server" Width="175px" Enabled="false">
                                </telerik:RadNumericTextBox>&nbsp;&nbsp;
                                <telerik:RadButton ID="rb_calculate" runat="server" Text="Calculate">
                                </telerik:RadButton>
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="rc_supplier" EventName="SelectedIndexChanged" />
                <asp:AsyncPostBackTrigger ControlID="rb_addItems" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="rb_clearForm" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="rb_calculate" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="rb_modify" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>
        <telerik:RadButton ID="rb_submit" runat="server" Text="Submit (Modify PO)">
        </telerik:RadButton>&nbsp;
        <telerik:RadButton ID="rb_clearForm" runat="server" Text="Clear Form">
        </telerik:RadButton>
    </div>--%>
</asp:Content>
