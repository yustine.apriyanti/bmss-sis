﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/LoginM.Master" CodeBehind="LoginForm.aspx.vb" Inherits="BMSS.LoginForm" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<table style="width: 500px">
    <tr>
        <th colspan="3" align="left">
            LOGIN
        </th>
    </tr>
    <tr>
        <td style="width: 15%">
            Username    
        </td>
        <td colspan="2">
            : &nbsp <telerik:RadTextBox ID="rt_username" runat="server">
            </telerik:RadTextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server"  CssClass="error_msg"
                ErrorMessage="*" ControlToValidate="rt_username" ValidationGroup="login"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td style="width: 15%">
            Password    
        </td>
        <td colspan="2">        
            : &nbsp <telerik:RadTextBox ID="rt_password" runat="server" TextMode="Password">
            </telerik:RadTextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server"  CssClass="error_msg"
                ErrorMessage="*" ControlToValidate="rt_password" ValidationGroup="login"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td colspan="3" align="left">
            <telerik:RadButton ID="rb_login" runat="server" Text="Submit" ValidationGroup="login">
            </telerik:RadButton>
        </td>
    </tr>
    <tr>
        <td colspan="3">
            <asp:Label ID="lbl_msg" runat="server" Text="" CssClass="error_msg"></asp:Label>
        </td>
    </tr>
</table>
    <telerik:RadScriptManager ID="RadScriptManager1" runat="server">
    </telerik:RadScriptManager>
</asp:Content>
