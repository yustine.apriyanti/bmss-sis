﻿Imports MySql.Data
Imports MySql.Data.MySqlClient

Public Class LoginForm
    Inherits System.Web.UI.Page

    Dim oConn As New MySqlConnection(ConfigurationManager.ConnectionStrings("conn_str").ConnectionString)

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Protected Sub rb_login_Click(ByVal sender As Object, ByVal e As EventArgs) Handles rb_login.Click
        oConn.Open()
        Dim oDatareader As MySqlDataReader
        Dim sql_check_login As New MySqlCommand("Select * from login_user where username = '" & rt_username.Text & "' and password = '" & rt_password.Text & "' and active = 1", oConn)
        oDatareader = sql_check_login.ExecuteReader()
        If oDatareader.HasRows() Then
            Session("login") = True
            Session("username") = rt_username.Text
            oDatareader.Close()
            Dim sqlCmd As New MySqlCommand("SELECT Group_ID FROM sys_user_group_sc where User_ID = '" & rt_username.Text & "'", oConn)
            oDatareader = sqlCmd.ExecuteReader()
            If oDatareader.HasRows() Then
                If oDatareader.Read() Then
                    Session("profile") = oDatareader("Group_ID").ToString()
                End If
            End If
            oDatareader.Close()
            If rt_password.Text = "1234" Then
                Response.Redirect("~/UpdatePassword.aspx")
            Else
                Response.Redirect("~/Default.aspx")
            End If
        Else
            oDatareader.Close()
            Session("login") = False
            lbl_msg.Text = "Invalid username and password"
        End If
        oConn.Close()
    End Sub

End Class