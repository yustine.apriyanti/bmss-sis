﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/WhiteDesign.Master" CodeBehind="test.aspx.vb" Inherits="BMSS.test" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Purchase Order</h1>
    </div>
</div>

<asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
    <ContentTemplate>  
        <div class="row">
            <form role="form">
                <div class="col-lg-12">
                    <div class="panel panel-default">                    
                        <div class="panel-heading">
                            <label>Supplier Data</label>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Company Name</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <asp:DropDownList ID="ddl_supplier" runat="server" class="form-control" EmptyMessage="Select" 
                                            DataSourceID="sql_supplier" DataTextField="CompanyName" 
                                            DataValueField="supplier_id" ResolvedRenderMode="Classic" AutoPostBack="true" autofocus >
                                            </asp:DropDownList> 
                                            <asp:SqlDataSource ID="sql_supplier" runat="server" 
                                                ConnectionString="<%$ ConnectionStrings:conn_str %>" 
                                                ProviderName="<%$ ConnectionStrings:conn_str.ProviderName %>" 
                                                SelectCommand="SELECT * from supplier order by CompanyName asc">
                                            </asp:SqlDataSource>  
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Address</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <asp:TextBox ID="txt_address" runat="server" class="form-control" 
                                                TextMode="MultiLine" Height="75px" ReadOnly="True" ></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Phone</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <asp:TextBox ID="txt_phone" runat="server" class="form-control" 
                                                ReadOnly="True"  ></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Fax</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <asp:TextBox ID="txt_fax" runat="server" class="form-control" ReadOnly="True"  ></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Contact Person</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <asp:TextBox ID="txt_contactPerson" runat="server" class="form-control" 
                                                ReadOnly="True"  ></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>  
                                        
                        <div class="panel-heading">
                            <label>Purchase Order Data</label>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>PO No</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <asp:TextBox ID="txt_po_no" runat="server" class="form-control" ></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>PO Date</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <telerik:RadDatePicker ID="rd_poDate" runat="server" Culture="en-US" CssClass="form-control">
                                                <Calendar ID="Calendar1" UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" ViewSelectorText="x" runat="server"></Calendar>
                                                <DateInput ID="DateInput1" DisplayDateFormat="yyyy-MM-dd" DateFormat="yyyy-MM-dd" runat="server"></DateInput>
                                                <DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>
                                            </telerik:RadDatePicker> 
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>PR No</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <asp:TextBox ID="txt_pr_no" runat="server" class="form-control"  ></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>PR Date</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <telerik:RadDatePicker ID="rd_prDate" runat="server" Culture="en-US" CssClass="form-control">
                                                <Calendar ID="Calendar2" UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" ViewSelectorText="x" runat="server"></Calendar>
                                                <DateInput ID="DateInput2" DisplayDateFormat="yyyy-MM-dd" DateFormat="yyyy-MM-dd" runat="server"></DateInput>
                                                <DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>
                                            </telerik:RadDatePicker>     
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Est. Delivery Date</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <telerik:RadDatePicker ID="rd_deliveryDate" runat="server" Culture="en-US" CssClass="form-control">
                                                <Calendar ID="Calendar3" UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" ViewSelectorText="x" runat="server"></Calendar>
                                                <DateInput ID="DateInput3" DisplayDateFormat="yyyy-MM-dd" DateFormat="yyyy-MM-dd" runat="server"></DateInput>
                                                <DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>
                                            </telerik:RadDatePicker>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Payment Terms</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <asp:TextBox ID="txt_payment_term" runat="server" class="form-control"  ></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Delivery Location</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <asp:TextBox ID="txt_del_loc" runat="server" class="form-control" TextMode="MultiLine" Height="75px"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Notes</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <asp:TextBox ID="txt_notes" runat="server" class="form-control" TextMode="MultiLine" Height="75px"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Goods</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <asp:RadioButtonList ID="rbl_goods_type" runat="server" 
                                                CssClass="radio-inline">
                                                <asp:ListItem Text="Non IT" Value="Non IT"/>
                                                <asp:ListItem Text="IT" Value="IT"/>
                                            </asp:RadioButtonList>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Currency</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <asp:DropDownList ID="ddl_curr" runat="server" class="form-control" EmptyMessage="Select" 
                                            DataSourceID="sql_currency" DataTextField="code" 
                                            DataValueField="code" ResolvedRenderMode="Classic">
                                            </asp:DropDownList> 
                                            <asp:SqlDataSource ID="sql_currency" runat="server" 
                                                ConnectionString="<%$ ConnectionStrings:conn_str %>" 
                                                ProviderName="<%$ ConnectionStrings:conn_str.ProviderName %>" 
                                                SelectCommand="SELECT cat_id, code, description FROM code_list WHERE (cat_id ='CUR') and active = 1 order by seq_no ASC">
                                            </asp:SqlDataSource>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="panel-heading">
                            <label>Item Data</label>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <telerik:RadGrid ID="rg_temp" runat="server" DataSourceID="sql_temp" 
                                        ShowFooter="True" AllowAutomaticDeletes="True" 
                                        CssClass="table table-striped table-bordered table-hover" >
                                        <MasterTableView AutoGenerateColumns="False" DataKeyNames="po_item_id" 
                                            DataSourceID="sql_temp">
                                            <Columns>
                                                <telerik:GridBoundColumn DataField="po_item_no" 
                                                    FilterControlAltText="Filter po_item_no column" HeaderText="No" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                                    SortExpression="po_item_no" UniqueName="po_item_no">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="stock_code" 
                                                    FilterControlAltText="Filter stock_code column" HeaderText="Catalog ID" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left"
                                                    SortExpression="stock_code" UniqueName="stock_code">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="part_desc" 
                                                    FilterControlAltText="Filter part_desc column" HeaderText="Item Name" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left"
                                                    SortExpression="part_desc" UniqueName="part_desc">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="merk" 
                                                    FilterControlAltText="Filter merk column" HeaderText="Merk" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left"
                                                    SortExpression="merk" UniqueName="merk">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="quantity" DataType="System.Int32" 
                                                    FilterControlAltText="Filter quantity column" HeaderText="Quantity" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" 
                                                    SortExpression="quantity" UniqueName="quantity">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="uop" 
                                                    FilterControlAltText="Filter unit column" HeaderText="UOP" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" 
                                                    SortExpression="uop" UniqueName="uop">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="unit" 
                                                    FilterControlAltText="Filter unit column" HeaderText="UOI" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" 
                                                    SortExpression="unit" UniqueName="unit">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="price" DataType="System.Decimal" 
                                                    FilterControlAltText="Filter price column" HeaderText="Unit Price (UOP)" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,##0.#0}" 
                                                    SortExpression="price" UniqueName="price">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="subtotal" DataType="System.Decimal" Aggregate="Sum" FooterAggregateFormatString="{0:###,##0.#0}" FooterStyle-HorizontalAlign="Right"
                                                    FilterControlAltText="Filter subtotal column" HeaderText="Total Price" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,##0.#0}" HeaderStyle-HorizontalAlign="Center" 
                                                    SortExpression="subtotal" UniqueName="subtotal">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridButtonColumn Text="Delete" CommandName="Delete" ButtonType="ImageButton" ConfirmText="Delete this data?" ConfirmDialogType="RadWindow" />
                                            </Columns>
                                        </MasterTableView>
                                        <ClientSettings Selecting-AllowRowSelect="true" EnablePostBackOnRowClick="true">
                                            <Selecting AllowRowSelect="True" />
                                        </ClientSettings>
                                    </telerik:RadGrid>
                                    <asp:SqlDataSource ID="sql_temp" runat="server" 
                                        ConnectionString="<%$ ConnectionStrings:conn_str %>" 
                                        ProviderName="<%$ ConnectionStrings:conn_str.ProviderName %>" 
                                        SelectCommand="SELECT * FROM temp_po"
                                        DeleteCommand="DELETE FROM temp_po WHERE (po_item_id = @po_item_id)" ></asp:SqlDataSource>
                                </div>
                                <div class="col-lg-6">                             
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Item No</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <asp:TextBox ID="txt_item_no" runat="server" class="form-control" ></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Catalog / Non Catalog</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <asp:RadioButtonList ID="rbl_catalog" runat="server" AutoPostBack="true" CssClass="radio-inline">
                                                <asp:ListItem Text="Non Catalog" Value="non" Selected="True"/>
                                                <asp:ListItem Text="Catalog" Value="catalog"/>
                                            </asp:RadioButtonList>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Catalog</label>
                                        </div>
                                    </div> 
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <asp:DropDownList ID="ddl_catalog" runat="server" class="form-control" EmptyMessage="Select" 
                                            DataSourceID="sql_catalog" DataTextField="DescX" AutoPostBack="true"
                                            DataValueField="catalog_id" ResolvedRenderMode="Classic">
                                            </asp:DropDownList> 
                                            <asp:SqlDataSource ID="sql_catalog" runat="server" 
                                                ConnectionString="<%$ ConnectionStrings:conn_str %>" 
                                                ProviderName="<%$ ConnectionStrings:conn_str.ProviderName %>" 
                                                SelectCommand="SELECT catalog_id, item_name, concat(IF(ISNULL(merk),'', CONCAT('[', merk, '] - ')), item_name) as DescX FROM catalog WHERE active = 1 order by concat(merk, ' - ', item_name) ASC">
                                            </asp:SqlDataSource>
                                        </div>
                                    </div>
                                        
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Item Name</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <asp:TextBox ID="txt_item_name" runat="server" class="form-control"  TextMode="MultiLine" Height="75px" ></asp:TextBox>
                                        </div>
                                    </div>                                    

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Merk</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <asp:DropDownList ID="ddl_merk" runat="server" class="form-control" EmptyMessage="Select" 
                                            DataSourceID="sql_merk" DataTextField="description" 
                                            DataValueField="code" ResolvedRenderMode="Classic">
                                            </asp:DropDownList> 
                                            <asp:SqlDataSource ID="sql_merk" runat="server" 
                                                ConnectionString="<%$ ConnectionStrings:conn_str %>" 
                                                ProviderName="<%$ ConnectionStrings:conn_str.ProviderName %>" 
                                                SelectCommand="SELECT cat_id, code, description FROM code_list WHERE (cat_id ='MRK') and active = 1 order by code ASC">
                                            </asp:SqlDataSource>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Quantity</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <asp:TextBox ID="txt_qty" runat="server" class="form-control"  ></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Unit Price</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <asp:TextBox ID="txt_unit_price" runat="server" class="form-control"  ></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Unit of Purchase (UOP)</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <asp:DropDownList ID="ddl_uop" runat="server" class="form-control" EmptyMessage="Select" 
                                            DataSourceID="sql_uom" DataTextField="Description" 
                                            DataValueField="Code" ResolvedRenderMode="Classic">
                                            </asp:DropDownList> 
                                            <asp:SqlDataSource ID="sql_uom" runat="server" 
                                                ConnectionString="<%$ ConnectionStrings:conn_str %>" 
                                                ProviderName="<%$ ConnectionStrings:conn_str.ProviderName %>" 
                                                SelectCommand="SELECT cat_id, code, description FROM code_list WHERE (cat_id ='UOM') and active = 1 order by code ASC">
                                            </asp:SqlDataSource>
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <asp:Button ID="btn_addItems" runat="server" Text="Add Item" class="btn btn-default"/> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="panel-heading">
                            <label>Total</label>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Discount</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <asp:TextBox ID="txt_discount" runat="server" class="form-control" ></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>PPN</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <asp:TextBox ID="txt_ppn" runat="server" class="form-control" ></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Shipping Fee</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <asp:TextBox ID="txt_shipping" runat="server" class="form-control"  ></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                    
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <asp:Button ID="btn_calculate" runat="server" Text="Calculate" class="btn btn-default"/> 
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Total PO</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <asp:TextBox ID="txt_total_po" runat="server" class="form-control"  ></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="panel-heading">
                            <div class="form-group">
                                <asp:Button ID="btn_submit_po" runat="server" Text="Submit" class="btn btn-primary"/> 
                                <asp:Button ID="btn_clear" runat="server" Text="Clear" class="btn btn-primary"/> 
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </ContentTemplate>
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="ddl_supplier" EventName="SelectedIndexChanged" />
        <asp:AsyncPostBackTrigger ControlID="ddl_catalog" EventName="SelectedIndexChanged" />
        <asp:AsyncPostBackTrigger ControlID="rbl_catalog" EventName="SelectedIndexChanged" />
        <asp:AsyncPostBackTrigger ControlID="btn_addItems" EventName="Click" />
        <asp:AsyncPostBackTrigger ControlID="btn_calculate" EventName="Click" />
    </Triggers>
</asp:UpdatePanel>

</asp:Content>
