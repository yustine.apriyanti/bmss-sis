﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class PurchaseOrderModify

    '''<summary>
    '''UpdatePanel2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents UpdatePanel2 As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''txt_po_no control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txt_po_no As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''btn_search control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btn_search As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''lbl_msg_search control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lbl_msg_search As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''ddl_supplier control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddl_supplier As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''sql_supplier control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents sql_supplier As Global.System.Web.UI.WebControls.SqlDataSource

    '''<summary>
    '''txt_address control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txt_address As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txt_phone control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txt_phone As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txt_fax control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txt_fax As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txt_contactPerson control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txt_contactPerson As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''TextBox1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents TextBox1 As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''ddl_company control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddl_company As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''sql_company control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents sql_company As Global.System.Web.UI.WebControls.SqlDataSource

    '''<summary>
    '''rd_poDate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rd_poDate As Global.Telerik.Web.UI.RadDatePicker

    '''<summary>
    '''txt_pr_no control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txt_pr_no As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''rd_prDate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rd_prDate As Global.Telerik.Web.UI.RadDatePicker

    '''<summary>
    '''rd_deliveryDate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rd_deliveryDate As Global.Telerik.Web.UI.RadDatePicker

    '''<summary>
    '''txt_payment_term control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txt_payment_term As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txt_del_loc control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txt_del_loc As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txt_notes control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txt_notes As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''rbl_goods_type control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rbl_goods_type As Global.System.Web.UI.WebControls.RadioButtonList

    '''<summary>
    '''ddl_curr control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddl_curr As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''sql_currency control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents sql_currency As Global.System.Web.UI.WebControls.SqlDataSource
End Class
