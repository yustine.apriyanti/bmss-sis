﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Login.aspx.vb" Inherits="BMSS.Login" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>

    <meta charset="utf-8" content="" >
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="description" content="" />
    <meta name="author" content="" />

    <title>SIS-BMSS</title>

    <!-- Bootstrap Core CSS -->
    <link href="Styles/css/bootstrap.min.css" rel="stylesheet" />

    <!-- MetisMenu CSS -->
    <link href="Styles/css/plugins/metisMenu/metisMenu.min.css" rel="stylesheet" />

    <!-- Custom CSS -->
    <link href="Styles/css/sb-admin-2.css" rel="stylesheet" />

    <!-- Custom Fonts -->
    <link href="Styles/font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">STOCK INVENTORY SYSTEM - BMSS</h3>
                    </div>
                    <div class="panel-body">
                        <form id="form" runat="server">
                            <fieldset>
                                <div class="form-group">
                                    <asp:TextBox ID="txt_username" runat="server" placeholder="Username" autofocus class="form-control"></asp:TextBox>
                                    <%--<input class="form-control" placeholder="Username" name="rt_username" type="text" autofocus />--%>
                                </div>
                                <div class="form-group">
                                    <asp:TextBox ID="txt_password" runat="server" placeholder="Password" class="form-control" TextMode="Password"></asp:TextBox>
                                    <%--<input class="form-control" placeholder="Password" name="rt_password" type="password" value="" />--%>
                                </div>
                                <div class="form-group">
                                    <asp:Label ID="lbl_msg" runat="server" Text="" class="text-warning"></asp:Label>
                                </div>
                                <!-- Change this to a button or input when using this as a form -->
                                <asp:Button ID="btn_login" runat="server" Text="Login" class="btn btn-lg btn-success btn-block"/>
                                <%--<a href="index.html" class="btn btn-lg btn-success btn-block">Login</a>--%>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- jQuery Version 1.11.0 -->
    <script src="Styles/js/jquery-1.11.0.js" type="text/javascript"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="Styles/js/bootstrap.min.js" type="text/javascript"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="Styles/js/plugins/metisMenu/metisMenu.min.js" type="text/javascript"></script>

    <!-- Custom Theme JavaScript -->
    <script src="Styles/js/sb-admin-2.js" type="text/javascript"></script>
</body>
</html>
