﻿
Public Class BaseClass
    Inherits System.Web.UI.Page

    Public Sub MessageException(ByVal msg_type As String, ByVal ex As Exception, ByVal lbl_msg As Label, ByVal css_class As String)
        Dim st = New StackTrace(ex, True)
        Dim frame = st.GetFrame(0)
        Dim line = frame.GetFileLineNumber()

        lbl_msg.Text = msg_type & " :(" & ex.Message.ToString() & ") === st:" & st.ToString() & " === frame:" & frame.ToString() & " === line:" & line.ToString()
        lbl_msg.CssClass = css_class
    End Sub

    Public Sub GetTempTableCatalog()
        Dim dt As DataTable = New DataTable()
        dt.Columns.Add("catalog_id", GetType(String))
        dt.Columns.Add("description", GetType(String))
        dt.Columns.Add("stock", GetType(Integer))
        dt.Columns.Add("unit", GetType(String))
        dt.Columns.Add("quantity", GetType(Integer))
        dt.Columns.Add("price", GetType(Integer))

        Session("dt") = dt
    End Sub
End Class
