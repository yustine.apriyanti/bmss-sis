﻿Public Class ReportTransactionDetail
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("login") Is Nothing Then
            Response.Redirect("Login.aspx")
        Else
            If Not IsPostBack Then
                ddl_period.SelectedValue = Date.Now.ToString("yyyy-MM")
                'If rcb_catalog.Text = "" Then
                '    sql_data.SelectParameters.Item("ParCatalogID").DefaultValue = ""
                'End If
                'rg_data.Rebind()
            End If
        End If
    End Sub

End Class