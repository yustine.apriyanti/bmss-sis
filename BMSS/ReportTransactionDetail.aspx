﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/WhiteDesign.Master" CodeBehind="ReportTransactionDetail.aspx.vb" Inherits="BMSS.ReportTransactionDetail" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Transaction Report - Detail</h1>
    </div>
</div>

<asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
    <ContentTemplate> 
        <div class="row">
            <form role="form">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Period</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <asp:DropDownList ID="ddl_period" runat="server" class="form-control" EmptyMessage="Select" 
                                            DataSourceID="sql_period" DataTextField="period" 
                                            DataValueField="period" ResolvedRenderMode="Classic" autofocus 
                                                AutoPostBack="True" >
                                            </asp:DropDownList> 
                                            <asp:SqlDataSource ID="sql_period" runat="server" 
                                                ConnectionString="<%$ ConnectionStrings:conn_str %>" 
                                                ProviderName="<%$ ConnectionStrings:conn_str.ProviderName %>" 
                                                SelectCommand="SELECT DISTINCT DATE_FORMAT(trans_date,'%Y-%m') as period from transaction 
                                                                UNION 
                                                                SELECT DATE_FORMAT(DATE_ADD(max(trans_date),INTERVAL 1 MONTH),'%Y-%m') as period from transaction">
                                            </asp:SqlDataSource>  
                                        </div>
                                    </div>
                                    
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Catalog</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">                                        
                                            <telerik:RadComboBox ID="rcb_catalog" runat="server" AutoPostBack="true" 
                                            EnableAutomaticLoadOnDemand="True" EnableVirtualScrolling="True" 
                                            EmptyMessage="Select catalog" DropDownAutoWidth="Enabled" 
                                                DataSourceID="sql_catalog" DataTextField="DescX" 
                                                DataValueField="catalog_id" Filter="Contains" Skin="Bootstrap" Width="100%">
                                            </telerik:RadComboBox>
                                            <asp:SqlDataSource ID="sql_catalog" runat="server" 
                                                ConnectionString="<%$ ConnectionStrings:conn_str %>" 
                                                ProviderName="<%$ ConnectionStrings:conn_str.ProviderName %>" 
                                                SelectCommand="SELECT catalog_id, item_name, concat(IF(ISNULL(merk),'', CONCAT('[', merk, '] - ')), item_name) as DescX FROM catalog WHERE active = 1 order by catalog_id ASC">
                                            </asp:SqlDataSource> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server">
                                    </telerik:RadAjaxLoadingPanel>
                                    <telerik:RadGrid ID="rg_data" runat="server" DataSourceID="sql_data" 
                                        Skin="Bootstrap">
                                        <ClientSettings>
                                            <Scrolling AllowScroll="True" ScrollHeight="500px" UseStaticHeaders="True" FrozenColumnsCount="2"/>
                                        </ClientSettings>
                                        <MasterTableView AutoGenerateColumns="false" DataSourceID="sql_data" 
                                            ShowGroupFooter="true">
                                            <GroupByExpressions>
                                                <telerik:GridGroupByExpression>
                                                    <SelectFields>
                                                        <telerik:GridGroupByField FieldName="trans_date_str" HeaderText="Trans Date"></telerik:GridGroupByField>
                                                    </SelectFields>
                                                    <GroupByFields>
                                                        <telerik:GridGroupByField FieldName="trans_date_str"></telerik:GridGroupByField>
                                                    </GroupByFields>
                                                </telerik:GridGroupByExpression>
                                            </GroupByExpressions>
                                            <Columns>
                                                <telerik:GridBoundColumn DataField="trans_id" Visible="false"
                                                    FilterControlAltText="Filter catalog_id column" HeaderText="Trans ID" 
                                                    HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" SortExpression="trans_id" UniqueName="trans_id">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="trans_date_str" Visible="false"
                                                    FilterControlAltText="Filter trans_date_str column" HeaderText="Trans Date" 
                                                    HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" SortExpression="trans_date_str" UniqueName="trans_date_str">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="trans_type"
                                                    FilterControlAltText="Filter trans_type column" HeaderText="Trans Type" 
                                                    HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" SortExpression="trans_type" UniqueName="trans_type">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="seq_no" Visible="false"
                                                    FilterControlAltText="Filter seq_no column" HeaderText="Seq No" 
                                                    HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" SortExpression="seq_no" UniqueName="seq_no">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="catalog_id" Visible="false"
                                                    FilterControlAltText="Filter catalog_id column" HeaderText="Catalog ID" 
                                                    HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" SortExpression="catalog_id" UniqueName="catalog_id">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="item_name"
                                                    FilterControlAltText="Filter item_name column" HeaderText="Item Name" 
                                                    HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" SortExpression="item_name" UniqueName="item_name">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="merk"
                                                    FilterControlAltText="Filter merk column" HeaderText="Merk" 
                                                    HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" SortExpression="merk" UniqueName="merk">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="unit"
                                                    FilterControlAltText="Filter unit column" HeaderText="UOI" 
                                                    HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" SortExpression="unit" UniqueName="unit">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="quantity"
                                                    FilterControlAltText="Filter quantity column" HeaderText="Quantity" 
                                                    HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" SortExpression="quantity" UniqueName="quantity">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="stock"
                                                    FilterControlAltText="Filter stock column" HeaderText="Stock" 
                                                    HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" SortExpression="stock" UniqueName="stock">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="price"
                                                    FilterControlAltText="Filter price column" HeaderText="Price" 
                                                    HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" SortExpression="price" UniqueName="price">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="po_no"
                                                    FilterControlAltText="Filter po_no column" HeaderText="PO No" 
                                                    HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" SortExpression="po_no" UniqueName="po_no">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="ir_no"
                                                    FilterControlAltText="Filter ir_no column" HeaderText="IR No" 
                                                    HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" SortExpression="ir_no" UniqueName="ir_no">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="division"
                                                    FilterControlAltText="Filter division column" HeaderText="Division" 
                                                    HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" SortExpression="division" UniqueName="division">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="user_request_name"
                                                    FilterControlAltText="Filter user_request_name column" HeaderText="User Request Name" 
                                                    HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" SortExpression="user_request_name" UniqueName="user_request_name">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="notes"
                                                    FilterControlAltText="Filter notes column" HeaderText="Notes" 
                                                    HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" SortExpression="notes" UniqueName="notes">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="create_by"
                                                    FilterControlAltText="Filter create_by column" HeaderText="Create By" 
                                                    HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" SortExpression="create_by" UniqueName="create_by">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="create_date"
                                                    FilterControlAltText="Filter create_date column" HeaderText="Create Date" 
                                                    HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" SortExpression="create_date" UniqueName="create_date">
                                                </telerik:GridBoundColumn>
                                            </Columns>
                                        </MasterTableView>
                                    </telerik:RadGrid>
                                    <asp:SqlDataSource ID="sql_data" runat="server"
                                        ConnectionString="<%$ ConnectionStrings:conn_str %>"
                                        SelectCommand="sp_report_transaction_detail" SelectCommandType="StoredProcedure" 
                                        ProviderName="<%$ ConnectionStrings:conn_str.ProviderName %>">
		                                <selectparameters>
			                                <asp:ControlParameter ControlID="ddl_period" Name="ParPeriod"/>
			                                <asp:ControlParameter ControlID="rcb_catalog" Name="ParCatalogID" ConvertEmptyStringToNull="true"/>
		                                </selectparameters>
	                                </asp:SqlDataSource>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </ContentTemplate>
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="ddl_period" EventName="SelectedIndexChanged" />
    </Triggers>
</asp:UpdatePanel>
</asp:Content>
