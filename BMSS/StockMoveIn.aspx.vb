﻿Imports MySql.Data.MySqlClient
Imports Telerik.Web.UI

Public Class StockMoveIn
    Inherits BaseClass

    Dim oConn As New MySqlConnection(ConfigurationManager.ConnectionStrings("conn_str").ConnectionString)
    Dim create_by As String = ""
    Dim str_type As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("login") Is Nothing Then
            Response.Redirect("Login.aspx")
        Else
            If Not IsPostBack Then
                rd_date.SelectedDate = Date.Now()
                rd_date_2.SelectedDate = Date.Now()
                str_type = Request.QueryString("type")
                lbl_header.Text = "Stock Movement IN"
                If str_type = "pos" Then 'receive PO dari system
                    'lbl_header.Text = "Stock Movement IN - PO System"
                    panel_po_search.Visible = True
                    panel_po_input.Visible = False
                ElseIf str_type = "pox" Or str_type = "" Then 'receive PO bukan dari system
                    'lbl_header.Text = "Stock Movement IN - Input PO Data"
                    GetTempTableCatalog()
                    BindGrid()
                    panel_po_search.Visible = False
                    panel_po_input.Visible = True
                End If
            End If
        End If
    End Sub

    Sub LoadData()
        Try
            Dim sql_search As New MySqlCommand("SELECT s.supplier_name, p.po_remark, d.po_item_id, d.po_item_no, d.catalog_id, d.item_name, d.merk, d.quantity, d.uoi, d.uop, d.conv_factor, d.qty_outstanding, d.qty_receive, d.price FROM purchase_order p LEFT JOIN purchase_order_item d ON p.po_no = d.po_no LEFT JOIN supplier s ON p.supplier_id = s.supplier_id where p.po_no = '" & txt_po_no_search.Text & "' ORDER BY d.po_item_no ASC", oConn)
            oConn.Open()
            Dim oDatareader As MySqlDataReader
            oDatareader = sql_search.ExecuteReader()
            If oDatareader.HasRows() Then
                rg_data.DataSource = oDatareader
                rg_data.DataBind()
                txt_supplier.Text = oDatareader("supplier_name").ToString()
                txt_po_notes.Text = oDatareader("po_remark").ToString()
                lbl_msg_search.Text = ""
            Else
                ClearData()
                oDatareader.Close()
                lbl_msg_search.Text = "PO No " & txt_po_no_search.Text & "not found"
            End If
            oConn.Close()
        Catch ex As Exception
            Dim st = New StackTrace(ex, True)
            Dim frame = st.GetFrame(0)
            Dim line = frame.GetFileLineNumber()

            lbl_msg.Text = "Load data failed :(" & ex.Message.ToString() & ") === st:" & st.ToString() & " === frame:" & frame.ToString() & " === line:" & line.ToString()
        End Try
    End Sub

    Sub ClearData()
        txt_po_no_search.Text = ""
        rd_date.SelectedDate = Date.Now()
        txt_supplier.Text = ""
        txt_po_notes.Text = ""
        rg_data.Rebind()

        txt_po_no.Text = ""
        rd_date_2.SelectedDate = Date.Now()
        ClearItem()

        txt_notes.Text = ""
        lbl_msg_search.Text = ""
        lbl_msg.Text = ""
    End Sub

    Sub ClearItem()
        lbl_index.Text = ""
        rcb_catalog.Text = ""
        rcb_catalog.ClearSelection()
        txt_soh.Text = ""
        txt_uoi.Text = ""
        rn_qty.Text = ""
        rn_price.Text = ""
    End Sub

    Protected Sub btn_search_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_search.Click
        lbl_msg_search.Text = ""
        lbl_msg.Text = ""
        LoadData()
    End Sub

    Protected Sub btn_receive_po_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_receive_po.Click
        Dim seq_no, last_stock, stock As Integer
        create_by = Session("username").ToString()
        Dim create_date As String = Date.Now.ToString("yyyy-MM-dd HH:mm:ss")
        Dim trans_date As String = Format(rd_date.SelectedDate, "yyyy-MM-dd")
        Dim oDatareader As MySqlDataReader

        Try
            oConn.Open()
            If panel_po_search.Visible = True Then 'str_type = "pos" -- 'receive PO dari system
                For Each item As GridDataItem In rg_data.MasterTableView.Items
                    Dim chk As CheckBox = DirectCast(item.FindControl("cb_choose"), CheckBox)
                    Dim catalog_id As String = item.Item("catalog_id").Text
                    If chk.Checked = True Then
                        Dim sql_catalog As New MySqlCommand("select * from catalog where catalog_id = '" & catalog_id & "'", oConn)
                        oDatareader = sql_catalog.ExecuteReader()
                        If oDatareader.HasRows() Then
                            If oDatareader.Read() Then
                                last_stock = Integer.Parse(oDatareader("soh").ToString())
                            End If
                        End If
                        oDatareader.Close()

                        Dim sql_seq_no As New MySqlCommand("select IF(ISNULL(seq_no),0, MAX(seq_no)+1) from transaction where catalog_id = '" & catalog_id & "'", oConn)
                        oDatareader = sql_seq_no.ExecuteReader()
                        If oDatareader.HasRows() Then
                            If oDatareader.Read() Then
                                seq_no = Int32.Parse(oDatareader(0).ToString())
                            End If
                        End If
                        oDatareader.Close()

                        Dim qty_receive As Integer = Integer.Parse(DirectCast(item.FindControl("txt_qty_received"), TextBox).Text) * Integer.Parse(DirectCast(item.FindControl("txt_conv_factor"), TextBox).Text)
                        stock = last_stock + qty_receive

                        If seq_no = 0 Then
                            Dim sql_insert_ic As New MySqlCommand("INSERT INTO transaction(trans_date, trans_type, seq_no, catalog_id, item_name, unit, quantity, stock, price, merk, create_by, create_date, notes) values(DATE_FORMAT('" & trans_date & "','%Y-%m-%d'), 'IC', 0, '" & catalog_id & "', '" & item.Item("item_name").Text & "', '" & item.Item("uoi").Text & "', 0, 0, 0, '" & item.Item("merk").Text & "', UPPER('" & create_by & "'), '" & create_date & "', 'DAFTAR CATALOG BARU')", oConn)
                            sql_insert_ic.ExecuteNonQuery()
                            seq_no = seq_no + 1
                        End If

                        Dim sql_insert As New MySqlCommand("INSERT INTO transaction (trans_date, trans_type, seq_no, catalog_id, item_name, unit, quantity, stock, price, merk, po_no, ir_no, division, user_request_name, notes, create_by, create_date) VALUES ('" & trans_date & "', 'IN', '" & seq_no & "', '" & catalog_id & "', '" & item.Item("item_name").Text & "', '" & item.Item("uoi").Text & "', " & qty_receive & ", " & stock & ", " & Integer.Parse(item.Item("price").Text) & ", '" & item.Item("merk").Text & "', UPPER('" & txt_po_no_search.Text & "'), '', '', '', UPPER('" & txt_notes.Text & "'), UPPER('" & create_by & "'), '" & create_date & "')", oConn)
                        sql_insert.ExecuteNonQuery()

                        Dim sql_update As New MySqlCommand("UPDATE catalog SET soh = " & stock & " WHERE catalog_id = '" & catalog_id & "'; UPDATE purchase_order_item SET qty_outstanding = " & Integer.Parse(item.Item("qty_outstanding").Text) - Integer.Parse(DirectCast(item.FindControl("txt_qty_received"), TextBox).Text) & ", qty_receive = " & Integer.Parse(DirectCast(item.FindControl("txt_qty_received"), TextBox).Text) & ", receive_date = '" & create_date & "' WHERE po_no = '" & txt_po_no_search.Text & "' and po_item_no = '" & item.Item("po_item_no").Text & "'", oConn)
                        sql_update.ExecuteNonQuery()
                    End If
                Next

                ClearData()
                lbl_msg.Text = "Insert transaction success"
                lbl_msg.CssClass = "text-success"
            ElseIf panel_po_input.Visible = True Then 'str_type = "pox" -- 'receive PO bukan dari system
                Dim item_name As String = ""
                Dim merk As String = ""

                ViewState("dt") = Session("dt")
                Dim dt As DataTable = TryCast((ViewState("dt")), DataTable)
                Dim dr As DataRow = dt.NewRow()

                If dt.Rows.Count > 0 Then
                    For Each dr In dt.Rows
                        Dim catalog_id As String = dr.Field(Of String)("catalog_id")
                        Dim unit As String = dr.Field(Of String)("unit")
                        Dim quantity As Integer = dr.Field(Of Integer)("quantity")
                        Dim price As Integer = dr.Field(Of Integer)("price")

                        Dim sql_catalog As New MySqlCommand("select * from catalog where catalog_id = '" & catalog_id & "'", oConn)
                        oDatareader = sql_catalog.ExecuteReader()
                        If oDatareader.HasRows() Then
                            If oDatareader.Read() Then
                                item_name = oDatareader("item_name").ToString()
                                merk = oDatareader("merk").ToString()
                                last_stock = Integer.Parse(oDatareader("soh").ToString())
                            End If
                        End If
                        oDatareader.Close()

                        Dim sql_seq_no As New MySqlCommand("select IF(ISNULL(seq_no),0, MAX(seq_no)+1) from transaction where catalog_id = '" & catalog_id & "'", oConn)
                        oDatareader = sql_seq_no.ExecuteReader()
                        If oDatareader.HasRows() Then
                            If oDatareader.Read() Then
                                seq_no = Int32.Parse(oDatareader(0).ToString())
                            End If
                        End If
                        oDatareader.Close()

                        stock = last_stock + quantity
                        If seq_no = 0 Then
                            Dim sql_insert_ic As New MySqlCommand("INSERT INTO transaction(trans_date, trans_type, seq_no, catalog_id, item_name, unit, quantity, stock, price, merk, create_by, create_date, notes) values(DATE_FORMAT('" & trans_date & "','%Y-%m-%d'), 'IC', 0, '" & catalog_id & "', '" & item_name & "', '" & unit & "', 0, 0, 0, '" & merk & "', UPPER('" & create_by & "'), '" & create_date & "', 'DAFTAR CATALOG BARU')", oConn)
                            sql_insert_ic.ExecuteNonQuery()
                            seq_no = seq_no + 1
                        End If

                        Dim sql_insert As New MySqlCommand("INSERT INTO transaction (trans_date, trans_type, seq_no, catalog_id, item_name, unit, quantity, stock, price, merk, po_no, ir_no, division, user_request_name, notes, create_by, create_date) VALUES ('" & trans_date & "', 'IN', '" & seq_no & "', '" & catalog_id & "', '" & item_name & "', '" & unit & "', " & quantity & ", " & stock & ", " & price & ", '" & merk & "', UPPER('" & txt_po_no.Text & "'), '', '', '', UPPER('" & txt_notes.Text & "'), UPPER('" & create_by & "'), '" & create_date & "')", oConn)
                        sql_insert.ExecuteNonQuery()

                        Dim sql_update As New MySqlCommand("UPDATE catalog SET soh = " & stock & " WHERE catalog_id = '" & catalog_id & "' ", oConn)
                        sql_update.ExecuteNonQuery()
                    Next
                    GetTempTableCatalog()
                    BindGrid()

                    ClearData()
                    lbl_msg.Text = "Insert transaction success"
                    lbl_msg.CssClass = "text-success"
                Else
                    Dim message As String = "Data Item empty"
                    RadWindowManager1.RadAlert(message, 300, 100, "Message", "refreshToItemData")
                End If
            End If
            oConn.Close()
        Catch ex As Exception
            oConn.Close()
            MessageException("Insert transaction failed", ex, lbl_msg, "text-warning")
        End Try
    End Sub

    Protected Sub btn_clear_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_clear.Click
        GetTempTableCatalog()
        BindGrid()
        ClearData()
    End Sub

    Protected Sub rg_data_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles rg_data.ItemDataBound
        If TypeOf e.Item Is GridDataItem Then
            Dim item As GridDataItem = DirectCast(e.Item, GridDataItem)
            Dim catalog_id As String = item("catalog_id").Text
            Dim qty_outstanding As Integer = Integer.Parse(item("qty_outstanding").Text)
            Dim cb_choose As CheckBox = DirectCast(item.FindControl("cb_choose"), CheckBox)
            Dim txt_qty_received As TextBox = DirectCast(item.FindControl("txt_qty_received"), TextBox)
            Dim txt_conv_factor As TextBox = DirectCast(item.FindControl("txt_conv_factor"), TextBox)

            If catalog_id = "&nbsp;" Or qty_outstanding = 0 Then
                cb_choose.Enabled = False
                txt_qty_received.Enabled = False
                txt_conv_factor.Enabled = False
            Else
                cb_choose.Enabled = True
                txt_qty_received.Enabled = True
                txt_conv_factor.Enabled = True
            End If
        End If
    End Sub

    Protected Sub rg_temp_catalog_ItemCommand(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs) Handles rg_temp_catalog.ItemCommand
        ViewState("dt") = Session("dt")
        Dim dt As DataTable = TryCast((ViewState("dt")), DataTable)
        Dim i As Integer = e.Item.ItemIndex

        If e.CommandName = "cancel" Then
            dt.Rows.RemoveAt(i)
            Session("dt") = dt
            BindGrid()
        ElseIf e.CommandName = "choose" Then
            lbl_index.Text = i
            rcb_catalog.Text = ""
            rcb_catalog.ClearSelection()
            rcb_catalog.SelectedValue = dt.Rows(i)("catalog_id")
            rcb_catalog.Text = dt.Rows(i)("description")
            txt_soh.Text = dt.Rows(i)("stock")
            txt_uoi.Text = dt.Rows(i)("unit")
            rn_qty.Text = dt.Rows(i)("quantity")
            rn_price.Text = dt.Rows(i)("price")
            btn_update.Visible = True
            btn_addItems.Text = "Cancel"
        End If
    End Sub

    Protected Sub BindGrid()
        rg_temp_catalog.DataSource = TryCast(Session("dt"), DataTable)
        rg_temp_catalog.DataBind()
        ClearItem()
    End Sub

    Protected Sub rcb_catalog_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles rcb_catalog.SelectedIndexChanged
        Dim sqlCmd As New MySqlCommand("select * from catalog where catalog_id = '" & rcb_catalog.SelectedValue.ToString() & "' ", oConn)
        oConn.Open()
        Dim oDatareader As MySqlDataReader
        oDatareader = sqlCmd.ExecuteReader()
        If oDatareader.HasRows() Then
            If oDatareader.Read() Then
                txt_soh.Text = Integer.Parse(oDatareader("soh").ToString())
                txt_uoi.Text = oDatareader("uoi").ToString()
            End If
        End If
        oDatareader.Close()
        oConn.Close()
    End Sub

    Protected Sub btn_addItems_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_addItems.Click
        If btn_addItems.Text = "Cancel" Then
            btn_addItems.Text = "Add Item"
            btn_update.Visible = False
            ClearItem()
        Else
            Dim dt As DataTable = TryCast((Session("dt")), DataTable)
            Dim dr As DataRow = dt.NewRow()

            dr("catalog_id") = rcb_catalog.SelectedValue.ToString()
            dr("description") = rcb_catalog.Text
            dr("stock") = txt_soh.Text
            dr("unit") = txt_uoi.Text
            dr("quantity") = rn_qty.Text
            dr("price") = rn_price.Text

            dt.Rows.Add(dr)
            Session("dt") = dt
            BindGrid()
        End If
    End Sub

    Protected Sub btn_update_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_update.Click
        If lbl_index.Text <> "" Then
            ViewState("dt") = Session("dt")
            Dim dt As DataTable = TryCast((ViewState("dt")), DataTable)
            Dim i As Integer = lbl_index.Text

            dt.Rows(i)("catalog_id") = rcb_catalog.SelectedValue.ToString()
            dt.Rows(i)("description") = rcb_catalog.Text
            dt.Rows(i)("stock") = txt_soh.Text
            dt.Rows(i)("unit") = txt_uoi.Text
            dt.Rows(i)("quantity") = rn_qty.Text
            dt.Rows(i)("price") = rn_price.Text

            Session("dt") = dt
            BindGrid()
        Else
            lbl_index.Text = ""
        End If

        btn_update.Visible = False
        btn_addItems.Text = "Add Item"
    End Sub
End Class