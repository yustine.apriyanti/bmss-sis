Partial Class PrintPO

    'NOTE: The following procedure is required by the telerik Reporting Designer
    'It can be modified using the telerik Reporting Designer.  
    'Do not modify it using the code editor.
    Private Sub InitializeComponent()
        Dim Group1 As Telerik.Reporting.Group = New Telerik.Reporting.Group()
        Dim ReportParameter1 As Telerik.Reporting.ReportParameter = New Telerik.Reporting.ReportParameter()
        Dim StyleRule1 As Telerik.Reporting.Drawing.StyleRule = New Telerik.Reporting.Drawing.StyleRule()
        Dim StyleRule2 As Telerik.Reporting.Drawing.StyleRule = New Telerik.Reporting.Drawing.StyleRule()
        Dim StyleRule3 As Telerik.Reporting.Drawing.StyleRule = New Telerik.Reporting.Drawing.StyleRule()
        Dim StyleRule4 As Telerik.Reporting.Drawing.StyleRule = New Telerik.Reporting.Drawing.StyleRule()
        Me.labelsGroupFooterSection = New Telerik.Reporting.GroupFooterSection()
        Me.TextBox23 = New Telerik.Reporting.TextBox()
        Me.TextBox29 = New Telerik.Reporting.TextBox()
        Me.TextBox28 = New Telerik.Reporting.TextBox()
        Me.TextBox27 = New Telerik.Reporting.TextBox()
        Me.TextBox26 = New Telerik.Reporting.TextBox()
        Me.TextBox25 = New Telerik.Reporting.TextBox()
        Me.TextBox24 = New Telerik.Reporting.TextBox()
        Me.TextBox30 = New Telerik.Reporting.TextBox()
        Me.labelsGroupHeaderSection = New Telerik.Reporting.GroupHeaderSection()
        Me.po_item_noCaptionTextBox = New Telerik.Reporting.TextBox()
        Me.item_nameCaptionTextBox = New Telerik.Reporting.TextBox()
        Me.merkCaptionTextBox = New Telerik.Reporting.TextBox()
        Me.quantityCaptionTextBox = New Telerik.Reporting.TextBox()
        Me.priceCaptionTextBox = New Telerik.Reporting.TextBox()
        Me.subtotal_itemCaptionTextBox = New Telerik.Reporting.TextBox()
        Me.TextBox21 = New Telerik.Reporting.TextBox()
        Me.sql_print_po = New Telerik.Reporting.SqlDataSource()
        Me.reportFooter = New Telerik.Reporting.ReportFooterSection()
        Me.TextBox31 = New Telerik.Reporting.TextBox()
        Me.TextBox32 = New Telerik.Reporting.TextBox()
        Me.TextBox33 = New Telerik.Reporting.TextBox()
        Me.TextBox34 = New Telerik.Reporting.TextBox()
        Me.TextBox35 = New Telerik.Reporting.TextBox()
        Me.TextBox36 = New Telerik.Reporting.TextBox()
        Me.TextBox37 = New Telerik.Reporting.TextBox()
        Me.TextBox38 = New Telerik.Reporting.TextBox()
        Me.TextBox39 = New Telerik.Reporting.TextBox()
        Me.TextBox40 = New Telerik.Reporting.TextBox()
        Me.TextBox41 = New Telerik.Reporting.TextBox()
        Me.TextBox42 = New Telerik.Reporting.TextBox()
        Me.TextBox43 = New Telerik.Reporting.TextBox()
        Me.TextBox44 = New Telerik.Reporting.TextBox()
        Me.TextBox1 = New Telerik.Reporting.TextBox()
        Me.pageFooter = New Telerik.Reporting.PageFooterSection()
        Me.reportHeader = New Telerik.Reporting.ReportHeaderSection()
        Me.titleTextBox = New Telerik.Reporting.TextBox()
        Me.TextBox2 = New Telerik.Reporting.TextBox()
        Me.TextBox3 = New Telerik.Reporting.TextBox()
        Me.TextBox4 = New Telerik.Reporting.TextBox()
        Me.TextBox5 = New Telerik.Reporting.TextBox()
        Me.TextBox6 = New Telerik.Reporting.TextBox()
        Me.TextBox7 = New Telerik.Reporting.TextBox()
        Me.TextBox8 = New Telerik.Reporting.TextBox()
        Me.TextBox9 = New Telerik.Reporting.TextBox()
        Me.TextBox10 = New Telerik.Reporting.TextBox()
        Me.TextBox11 = New Telerik.Reporting.TextBox()
        Me.TextBox12 = New Telerik.Reporting.TextBox()
        Me.TextBox13 = New Telerik.Reporting.TextBox()
        Me.TextBox14 = New Telerik.Reporting.TextBox()
        Me.TextBox15 = New Telerik.Reporting.TextBox()
        Me.TextBox16 = New Telerik.Reporting.TextBox()
        Me.TextBox17 = New Telerik.Reporting.TextBox()
        Me.TextBox18 = New Telerik.Reporting.TextBox()
        Me.TextBox19 = New Telerik.Reporting.TextBox()
        Me.TextBox20 = New Telerik.Reporting.TextBox()
        Me.detail = New Telerik.Reporting.DetailSection()
        Me.po_item_noDataTextBox = New Telerik.Reporting.TextBox()
        Me.item_nameDataTextBox = New Telerik.Reporting.TextBox()
        Me.merkDataTextBox = New Telerik.Reporting.TextBox()
        Me.quantityDataTextBox = New Telerik.Reporting.TextBox()
        Me.priceDataTextBox = New Telerik.Reporting.TextBox()
        Me.subtotal_itemDataTextBox = New Telerik.Reporting.TextBox()
        Me.TextBox22 = New Telerik.Reporting.TextBox()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'labelsGroupFooterSection
        '
        Me.labelsGroupFooterSection.Height = Telerik.Reporting.Drawing.Unit.Inch(1.6999998092651367R)
        Me.labelsGroupFooterSection.Items.AddRange(New Telerik.Reporting.ReportItemBase() {Me.TextBox23, Me.TextBox29, Me.TextBox28, Me.TextBox27, Me.TextBox26, Me.TextBox25, Me.TextBox24, Me.TextBox30})
        Me.labelsGroupFooterSection.Name = "labelsGroupFooterSection"
        Me.labelsGroupFooterSection.Style.Visible = True
        '
        'TextBox23
        '
        Me.TextBox23.CanGrow = True
        Me.TextBox23.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.7000002861022949R), Telerik.Reporting.Drawing.Unit.Inch(0.000039418537198798731R))
        Me.TextBox23.Name = "TextBox23"
        Me.TextBox23.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0R), Telerik.Reporting.Drawing.Unit.Inch(0.40000000596046448R))
        Me.TextBox23.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left
        Me.TextBox23.StyleName = "Data"
        Me.TextBox23.Value = "Discount"
        '
        'TextBox29
        '
        Me.TextBox29.CanGrow = True
        Me.TextBox29.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.7026562690734863R), Telerik.Reporting.Drawing.Unit.Inch(1.2064462900161743R))
        Me.TextBox29.Name = "TextBox29"
        Me.TextBox29.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0499210357666016R), Telerik.Reporting.Drawing.Unit.Inch(0.40000000596046448R))
        Me.TextBox29.Style.Font.Bold = True
        Me.TextBox29.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right
        Me.TextBox29.StyleName = "Data"
        Me.TextBox29.Value = "= Fields.total"
        '
        'TextBox28
        '
        Me.TextBox28.CanGrow = True
        Me.TextBox28.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.7000789642333984R), Telerik.Reporting.Drawing.Unit.Inch(0.000039418537198798731R))
        Me.TextBox28.Name = "TextBox28"
        Me.TextBox28.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0499210357666016R), Telerik.Reporting.Drawing.Unit.Inch(0.40000000596046448R))
        Me.TextBox28.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right
        Me.TextBox28.StyleName = "Data"
        Me.TextBox28.Value = "= Fields.discount"
        '
        'TextBox27
        '
        Me.TextBox27.CanGrow = True
        Me.TextBox27.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.7000789642333984R), Telerik.Reporting.Drawing.Unit.Inch(0.40628942847251892R))
        Me.TextBox27.Name = "TextBox27"
        Me.TextBox27.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0499210357666016R), Telerik.Reporting.Drawing.Unit.Inch(0.40000000596046448R))
        Me.TextBox27.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right
        Me.TextBox27.StyleName = "Data"
        Me.TextBox27.Value = "= Fields.ppn"
        '
        'TextBox26
        '
        Me.TextBox26.CanGrow = True
        Me.TextBox26.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.7000789642333984R), Telerik.Reporting.Drawing.Unit.Inch(0.80636787414550781R))
        Me.TextBox26.Name = "TextBox26"
        Me.TextBox26.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0499210357666016R), Telerik.Reporting.Drawing.Unit.Inch(0.40000000596046448R))
        Me.TextBox26.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right
        Me.TextBox26.StyleName = "Data"
        Me.TextBox26.Value = "= Fields.transportation"
        '
        'TextBox25
        '
        Me.TextBox25.CanGrow = True
        Me.TextBox25.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.7000002861022949R), Telerik.Reporting.Drawing.Unit.Inch(0.80636787414550781R))
        Me.TextBox25.Name = "TextBox25"
        Me.TextBox25.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0R), Telerik.Reporting.Drawing.Unit.Inch(0.40000000596046448R))
        Me.TextBox25.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left
        Me.TextBox25.StyleName = "Data"
        Me.TextBox25.Value = "Shipping"
        '
        'TextBox24
        '
        Me.TextBox24.CanGrow = True
        Me.TextBox24.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.7000002861022949R), Telerik.Reporting.Drawing.Unit.Inch(0.40628942847251892R))
        Me.TextBox24.Name = "TextBox24"
        Me.TextBox24.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0R), Telerik.Reporting.Drawing.Unit.Inch(0.40000000596046448R))
        Me.TextBox24.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left
        Me.TextBox24.StyleName = "Data"
        Me.TextBox24.Value = "PPN"
        '
        'TextBox30
        '
        Me.TextBox30.CanGrow = True
        Me.TextBox30.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.7000002861022949R), Telerik.Reporting.Drawing.Unit.Inch(1.2064462900161743R))
        Me.TextBox30.Name = "TextBox30"
        Me.TextBox30.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0R), Telerik.Reporting.Drawing.Unit.Inch(0.40000000596046448R))
        Me.TextBox30.Style.Font.Bold = True
        Me.TextBox30.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left
        Me.TextBox30.StyleName = "Data"
        Me.TextBox30.Value = "Total"
        '
        'labelsGroupHeaderSection
        '
        Me.labelsGroupHeaderSection.Height = Telerik.Reporting.Drawing.Unit.Inch(0.44166669249534607R)
        Me.labelsGroupHeaderSection.Items.AddRange(New Telerik.Reporting.ReportItemBase() {Me.po_item_noCaptionTextBox, Me.item_nameCaptionTextBox, Me.merkCaptionTextBox, Me.quantityCaptionTextBox, Me.priceCaptionTextBox, Me.subtotal_itemCaptionTextBox, Me.TextBox21})
        Me.labelsGroupHeaderSection.Name = "labelsGroupHeaderSection"
        Me.labelsGroupHeaderSection.PrintOnEveryPage = True
        '
        'po_item_noCaptionTextBox
        '
        Me.po_item_noCaptionTextBox.CanGrow = True
        Me.po_item_noCaptionTextBox.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.02083333395421505R), Telerik.Reporting.Drawing.Unit.Inch(0.02083333395421505R))
        Me.po_item_noCaptionTextBox.Name = "po_item_noCaptionTextBox"
        Me.po_item_noCaptionTextBox.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.37908801436424255R), Telerik.Reporting.Drawing.Unit.Inch(0.40000000596046448R))
        Me.po_item_noCaptionTextBox.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center
        Me.po_item_noCaptionTextBox.StyleName = "Caption"
        Me.po_item_noCaptionTextBox.Value = "No"
        '
        'item_nameCaptionTextBox
        '
        Me.item_nameCaptionTextBox.CanGrow = True
        Me.item_nameCaptionTextBox.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.40000000596046448R), Telerik.Reporting.Drawing.Unit.Inch(0.02083333395421505R))
        Me.item_nameCaptionTextBox.Name = "item_nameCaptionTextBox"
        Me.item_nameCaptionTextBox.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.5000002384185791R), Telerik.Reporting.Drawing.Unit.Inch(0.40000000596046448R))
        Me.item_nameCaptionTextBox.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center
        Me.item_nameCaptionTextBox.StyleName = "Caption"
        Me.item_nameCaptionTextBox.Value = "Item Name"
        '
        'merkCaptionTextBox
        '
        Me.merkCaptionTextBox.CanGrow = True
        Me.merkCaptionTextBox.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.9000790119171143R), Telerik.Reporting.Drawing.Unit.Inch(0.02083333395421505R))
        Me.merkCaptionTextBox.Name = "merkCaptionTextBox"
        Me.merkCaptionTextBox.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1999210119247437R), Telerik.Reporting.Drawing.Unit.Inch(0.40000000596046448R))
        Me.merkCaptionTextBox.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center
        Me.merkCaptionTextBox.StyleName = "Caption"
        Me.merkCaptionTextBox.Value = "Merk"
        '
        'quantityCaptionTextBox
        '
        Me.quantityCaptionTextBox.CanGrow = True
        Me.quantityCaptionTextBox.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.9988884925842285R), Telerik.Reporting.Drawing.Unit.Inch(0.02083333395421505R))
        Me.quantityCaptionTextBox.Name = "quantityCaptionTextBox"
        Me.quantityCaptionTextBox.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.70111149549484253R), Telerik.Reporting.Drawing.Unit.Inch(0.40000000596046448R))
        Me.quantityCaptionTextBox.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center
        Me.quantityCaptionTextBox.StyleName = "Caption"
        Me.quantityCaptionTextBox.Value = "Quantity"
        '
        'priceCaptionTextBox
        '
        Me.priceCaptionTextBox.CanGrow = True
        Me.priceCaptionTextBox.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.7000002861022949R), Telerik.Reporting.Drawing.Unit.Inch(0.02083333395421505R))
        Me.priceCaptionTextBox.Name = "priceCaptionTextBox"
        Me.priceCaptionTextBox.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0R), Telerik.Reporting.Drawing.Unit.Inch(0.40000000596046448R))
        Me.priceCaptionTextBox.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center
        Me.priceCaptionTextBox.StyleName = "Caption"
        Me.priceCaptionTextBox.Value = "Price"
        '
        'subtotal_itemCaptionTextBox
        '
        Me.subtotal_itemCaptionTextBox.CanGrow = True
        Me.subtotal_itemCaptionTextBox.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.7000789642333984R), Telerik.Reporting.Drawing.Unit.Inch(0.02083333395421505R))
        Me.subtotal_itemCaptionTextBox.Name = "subtotal_itemCaptionTextBox"
        Me.subtotal_itemCaptionTextBox.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0499211549758911R), Telerik.Reporting.Drawing.Unit.Inch(0.40000000596046448R))
        Me.subtotal_itemCaptionTextBox.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center
        Me.subtotal_itemCaptionTextBox.StyleName = "Caption"
        Me.subtotal_itemCaptionTextBox.Value = "Subtotal"
        '
        'TextBox21
        '
        Me.TextBox21.CanGrow = True
        Me.TextBox21.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.0999999046325684R), Telerik.Reporting.Drawing.Unit.Inch(0.02083333395421505R))
        Me.TextBox21.Name = "TextBox21"
        Me.TextBox21.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.898809552192688R), Telerik.Reporting.Drawing.Unit.Inch(0.40000000596046448R))
        Me.TextBox21.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center
        Me.TextBox21.StyleName = "Caption"
        Me.TextBox21.Value = "Unit of Purchase"
        '
        'sql_print_po
        '
        Me.sql_print_po.ConnectionString = "conn_str"
        Me.sql_print_po.Name = "sql_print_po"
        Me.sql_print_po.Parameters.AddRange(New Telerik.Reporting.SqlDataSourceParameter() {New Telerik.Reporting.SqlDataSourceParameter("@po_par", System.Data.DbType.[String], "=Parameters.popar.Value")})
        Me.sql_print_po.SelectCommand = "bmss.sp_print_po"
        Me.sql_print_po.SelectCommandType = Telerik.Reporting.SqlDataSourceCommandType.StoredProcedure
        '
        'reportFooter
        '
        Me.reportFooter.Height = Telerik.Reporting.Drawing.Unit.Inch(2.7999999523162842R)
        Me.reportFooter.Items.AddRange(New Telerik.Reporting.ReportItemBase() {Me.TextBox31, Me.TextBox32, Me.TextBox33, Me.TextBox34, Me.TextBox35, Me.TextBox36, Me.TextBox37, Me.TextBox38, Me.TextBox39, Me.TextBox40, Me.TextBox41, Me.TextBox42, Me.TextBox43, Me.TextBox44})
        Me.reportFooter.Name = "reportFooter"
        Me.reportFooter.Style.Visible = True
        '
        'TextBox31
        '
        Me.TextBox31.CanGrow = True
        Me.TextBox31.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.0R), Telerik.Reporting.Drawing.Unit.Inch(0.000039418537198798731R))
        Me.TextBox31.Name = "TextBox31"
        Me.TextBox31.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1999999284744263R), Telerik.Reporting.Drawing.Unit.Inch(0.59996050596237183R))
        Me.TextBox31.StyleName = "Data"
        Me.TextBox31.Value = " Notes"
        '
        'TextBox32
        '
        Me.TextBox32.CanGrow = True
        Me.TextBox32.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.2000788450241089R), Telerik.Reporting.Drawing.Unit.Inch(0.000039418537198798731R))
        Me.TextBox32.Name = "TextBox32"
        Me.TextBox32.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(6.5499210357666016R), Telerik.Reporting.Drawing.Unit.Inch(0.59996050596237183R))
        Me.TextBox32.StyleName = "Data"
        Me.TextBox32.Value = "= "" "" + Fields.po_remark"
        '
        'TextBox33
        '
        Me.TextBox33.CanGrow = True
        Me.TextBox33.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.0R), Telerik.Reporting.Drawing.Unit.Inch(0.60007858276367188R))
        Me.TextBox33.Name = "TextBox33"
        Me.TextBox33.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1999999284744263R), Telerik.Reporting.Drawing.Unit.Inch(0.30000001192092896R))
        Me.TextBox33.StyleName = "Data"
        Me.TextBox33.Value = " Payment Terms"
        '
        'TextBox34
        '
        Me.TextBox34.CanGrow = True
        Me.TextBox34.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.2000789642333984R), Telerik.Reporting.Drawing.Unit.Inch(0.60007858276367188R))
        Me.TextBox34.Name = "TextBox34"
        Me.TextBox34.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(6.5499210357666016R), Telerik.Reporting.Drawing.Unit.Inch(0.30000001192092896R))
        Me.TextBox34.StyleName = "Data"
        Me.TextBox34.Value = "= "" ("" + Fields.payment_code + "") "" + Fields.payment_term"
        '
        'TextBox35
        '
        Me.TextBox35.CanGrow = True
        Me.TextBox35.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.0R), Telerik.Reporting.Drawing.Unit.Inch(0.90015727281570435R))
        Me.TextBox35.Name = "TextBox35"
        Me.TextBox35.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1999212503433228R), Telerik.Reporting.Drawing.Unit.Inch(0.59996050596237183R))
        Me.TextBox35.StyleName = "Data"
        Me.TextBox35.Value = " Delivery" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & " Location"
        '
        'TextBox36
        '
        Me.TextBox36.CanGrow = True
        Me.TextBox36.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.2000001668930054R), Telerik.Reporting.Drawing.Unit.Inch(0.90015727281570435R))
        Me.TextBox36.Name = "TextBox36"
        Me.TextBox36.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(6.5499997138977051R), Telerik.Reporting.Drawing.Unit.Inch(0.59996050596237183R))
        Me.TextBox36.StyleName = "Data"
        Me.TextBox36.Value = "= "" "" + Fields.delivery_location"
        '
        'TextBox37
        '
        Me.TextBox37.CanGrow = True
        Me.TextBox37.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.0R), Telerik.Reporting.Drawing.Unit.Inch(1.5999997854232788R))
        Me.TextBox37.Name = "TextBox37"
        Me.TextBox37.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.9375R), Telerik.Reporting.Drawing.Unit.Inch(0.30000001192092896R))
        Me.TextBox37.Style.BackgroundColor = System.Drawing.Color.LightGray
        Me.TextBox37.Style.BorderColor.Default = System.Drawing.Color.Black
        Me.TextBox37.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid
        Me.TextBox37.Style.Color = System.Drawing.Color.Black
        Me.TextBox37.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center
        Me.TextBox37.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle
        Me.TextBox37.StyleName = "Caption"
        Me.TextBox37.Value = "Approve By"
        '
        'TextBox38
        '
        Me.TextBox38.CanGrow = True
        Me.TextBox38.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.9375787973403931R), Telerik.Reporting.Drawing.Unit.Inch(1.5999997854232788R))
        Me.TextBox38.Name = "TextBox38"
        Me.TextBox38.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.9375R), Telerik.Reporting.Drawing.Unit.Inch(0.30000001192092896R))
        Me.TextBox38.Style.BackgroundColor = System.Drawing.Color.LightGray
        Me.TextBox38.Style.BorderColor.Default = System.Drawing.Color.Black
        Me.TextBox38.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid
        Me.TextBox38.Style.Color = System.Drawing.Color.Black
        Me.TextBox38.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center
        Me.TextBox38.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle
        Me.TextBox38.StyleName = "Caption"
        Me.TextBox38.Value = "Approve By"
        '
        'TextBox39
        '
        Me.TextBox39.CanGrow = True
        Me.TextBox39.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.8751575946807861R), Telerik.Reporting.Drawing.Unit.Inch(1.5999997854232788R))
        Me.TextBox39.Name = "TextBox39"
        Me.TextBox39.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.9375R), Telerik.Reporting.Drawing.Unit.Inch(0.30000001192092896R))
        Me.TextBox39.Style.BackgroundColor = System.Drawing.Color.LightGray
        Me.TextBox39.Style.BorderColor.Default = System.Drawing.Color.Black
        Me.TextBox39.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid
        Me.TextBox39.Style.Color = System.Drawing.Color.Black
        Me.TextBox39.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center
        Me.TextBox39.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle
        Me.TextBox39.StyleName = "Caption"
        Me.TextBox39.Value = "Approve By"
        '
        'TextBox40
        '
        Me.TextBox40.CanGrow = True
        Me.TextBox40.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.812736988067627R), Telerik.Reporting.Drawing.Unit.Inch(1.6000000238418579R))
        Me.TextBox40.Name = "TextBox40"
        Me.TextBox40.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.93722403049469R), Telerik.Reporting.Drawing.Unit.Inch(0.30000001192092896R))
        Me.TextBox40.Style.BackgroundColor = System.Drawing.Color.LightGray
        Me.TextBox40.Style.BorderColor.Default = System.Drawing.Color.Black
        Me.TextBox40.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid
        Me.TextBox40.Style.Color = System.Drawing.Color.Black
        Me.TextBox40.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center
        Me.TextBox40.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle
        Me.TextBox40.StyleName = "Caption"
        Me.TextBox40.Value = "Approve By"
        '
        'TextBox41
        '
        Me.TextBox41.CanGrow = True
        Me.TextBox41.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.0R), Telerik.Reporting.Drawing.Unit.Inch(1.9000784158706665R))
        Me.TextBox41.Name = "TextBox41"
        Me.TextBox41.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.9375R), Telerik.Reporting.Drawing.Unit.Inch(0.800000011920929R))
        Me.TextBox41.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid
        Me.TextBox41.StyleName = "Data"
        Me.TextBox41.Value = ""
        '
        'TextBox42
        '
        Me.TextBox42.CanGrow = True
        Me.TextBox42.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.9375787973403931R), Telerik.Reporting.Drawing.Unit.Inch(1.9000784158706665R))
        Me.TextBox42.Name = "TextBox42"
        Me.TextBox42.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.9375R), Telerik.Reporting.Drawing.Unit.Inch(0.800000011920929R))
        Me.TextBox42.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid
        Me.TextBox42.StyleName = "Data"
        Me.TextBox42.Value = ""
        '
        'TextBox43
        '
        Me.TextBox43.CanGrow = True
        Me.TextBox43.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.8751578330993652R), Telerik.Reporting.Drawing.Unit.Inch(1.9000784158706665R))
        Me.TextBox43.Name = "TextBox43"
        Me.TextBox43.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.9373420476913452R), Telerik.Reporting.Drawing.Unit.Inch(0.800000011920929R))
        Me.TextBox43.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid
        Me.TextBox43.StyleName = "Data"
        Me.TextBox43.Value = ""
        '
        'TextBox44
        '
        Me.TextBox44.CanGrow = True
        Me.TextBox44.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.8124604225158691R), Telerik.Reporting.Drawing.Unit.Inch(1.9000784158706665R))
        Me.TextBox44.Name = "TextBox44"
        Me.TextBox44.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.9375R), Telerik.Reporting.Drawing.Unit.Inch(0.800000011920929R))
        Me.TextBox44.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid
        Me.TextBox44.StyleName = "Data"
        Me.TextBox44.Value = ""
        '
        'TextBox1
        '
        Me.TextBox1.CanGrow = True
        Me.TextBox1.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.0R), Telerik.Reporting.Drawing.Unit.Inch(0.4479166567325592R))
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.875R), Telerik.Reporting.Drawing.Unit.Inch(0.30000001192092896R))
        Me.TextBox1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left
        Me.TextBox1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle
        Me.TextBox1.StyleName = "Caption"
        Me.TextBox1.Value = ""
        '
        'pageFooter
        '
        Me.pageFooter.Height = Telerik.Reporting.Drawing.Unit.Inch(0.12301517277956009R)
        Me.pageFooter.Name = "pageFooter"
        '
        'reportHeader
        '
        Me.reportHeader.Height = Telerik.Reporting.Drawing.Unit.Inch(2.46875R)
        Me.reportHeader.Items.AddRange(New Telerik.Reporting.ReportItemBase() {Me.titleTextBox, Me.TextBox2, Me.TextBox1, Me.TextBox3, Me.TextBox4, Me.TextBox5, Me.TextBox6, Me.TextBox7, Me.TextBox8, Me.TextBox9, Me.TextBox10, Me.TextBox11, Me.TextBox12, Me.TextBox13, Me.TextBox14, Me.TextBox15, Me.TextBox16, Me.TextBox17, Me.TextBox18, Me.TextBox19, Me.TextBox20})
        Me.reportHeader.Name = "reportHeader"
        '
        'titleTextBox
        '
        Me.titleTextBox.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.0R), Telerik.Reporting.Drawing.Unit.Inch(0.000039418537198798731R))
        Me.titleTextBox.Name = "titleTextBox"
        Me.titleTextBox.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.75R), Telerik.Reporting.Drawing.Unit.Inch(0.34999999403953552R))
        Me.titleTextBox.Style.Font.Bold = True
        Me.titleTextBox.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(14.0R)
        Me.titleTextBox.Style.Font.Strikeout = False
        Me.titleTextBox.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center
        Me.titleTextBox.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle
        Me.titleTextBox.StyleName = "Title"
        Me.titleTextBox.Value = "PURCHASE ORDER"
        '
        'TextBox2
        '
        Me.TextBox2.CanGrow = True
        Me.TextBox2.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.0R), Telerik.Reporting.Drawing.Unit.Inch(0.74791669845581055R))
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.99992114305496216R), Telerik.Reporting.Drawing.Unit.Inch(0.30000001192092896R))
        Me.TextBox2.StyleName = "Data"
        Me.TextBox2.Value = " Name"
        '
        'TextBox3
        '
        Me.TextBox3.CanGrow = True
        Me.TextBox3.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.0R), Telerik.Reporting.Drawing.Unit.Inch(0.74799537658691406R))
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.8750002384185791R), Telerik.Reporting.Drawing.Unit.Inch(0.30000001192092896R))
        Me.TextBox3.StyleName = "Data"
        Me.TextBox3.Value = "= Fields.supplier_name"
        '
        'TextBox4
        '
        Me.TextBox4.CanGrow = True
        Me.TextBox4.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.0R), Telerik.Reporting.Drawing.Unit.Inch(1.0480742454528809R))
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.875R), Telerik.Reporting.Drawing.Unit.Inch(0.30000001192092896R))
        Me.TextBox4.StyleName = "Data"
        Me.TextBox4.Value = "= Fields.contact_name"
        '
        'TextBox5
        '
        Me.TextBox5.CanGrow = True
        Me.TextBox5.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.0R), Telerik.Reporting.Drawing.Unit.Inch(1.0479955673217773R))
        Me.TextBox5.Name = "TextBox5"
        Me.TextBox5.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.99873077869415283R), Telerik.Reporting.Drawing.Unit.Inch(0.30000001192092896R))
        Me.TextBox5.StyleName = "Data"
        Me.TextBox5.Value = " Contact"
        '
        'TextBox6
        '
        Me.TextBox6.CanGrow = True
        Me.TextBox6.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.001190185546875R), Telerik.Reporting.Drawing.Unit.Inch(1.3483105897903442R))
        Me.TextBox6.Name = "TextBox6"
        Me.TextBox6.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.873809814453125R), Telerik.Reporting.Drawing.Unit.Inch(0.59976369142532349R))
        Me.TextBox6.StyleName = "Data"
        Me.TextBox6.Value = "= Fields.address"
        '
        'TextBox7
        '
        Me.TextBox7.CanGrow = True
        Me.TextBox7.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.0R), Telerik.Reporting.Drawing.Unit.Inch(1.3480744361877441R))
        Me.TextBox7.Name = "TextBox7"
        Me.TextBox7.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.99992120265960693R), Telerik.Reporting.Drawing.Unit.Inch(0.60000002384185791R))
        Me.TextBox7.StyleName = "Data"
        Me.TextBox7.Value = " Address"
        '
        'TextBox8
        '
        Me.TextBox8.CanGrow = True
        Me.TextBox8.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.0R), Telerik.Reporting.Drawing.Unit.Inch(1.9481531381607056R))
        Me.TextBox8.Name = "TextBox8"
        Me.TextBox8.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.99873077869415283R), Telerik.Reporting.Drawing.Unit.Inch(0.30000001192092896R))
        Me.TextBox8.StyleName = "Data"
        Me.TextBox8.Value = " Phone / Fax"
        '
        'TextBox9
        '
        Me.TextBox9.CanGrow = True
        Me.TextBox9.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.99999970197677612R), Telerik.Reporting.Drawing.Unit.Inch(1.9481531381607056R))
        Me.TextBox9.Name = "TextBox9"
        Me.TextBox9.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.8750004768371582R), Telerik.Reporting.Drawing.Unit.Inch(0.30000001192092896R))
        Me.TextBox9.StyleName = "Data"
        Me.TextBox9.Value = "= Fields.phone + "" / "" + Fields.fax"
        '
        'TextBox10
        '
        Me.TextBox10.CanGrow = True
        Me.TextBox10.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.875R), Telerik.Reporting.Drawing.Unit.Inch(0.44791674613952637R))
        Me.TextBox10.Name = "TextBox10"
        Me.TextBox10.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.875R), Telerik.Reporting.Drawing.Unit.Inch(0.30000001192092896R))
        Me.TextBox10.Style.Font.Bold = True
        Me.TextBox10.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right
        Me.TextBox10.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle
        Me.TextBox10.StyleName = "Caption"
        Me.TextBox10.Value = "= Fields.description + ""  """
        '
        'TextBox11
        '
        Me.TextBox11.CanGrow = True
        Me.TextBox11.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.875R), Telerik.Reporting.Drawing.Unit.Inch(0.74791669845581055R))
        Me.TextBox11.Name = "TextBox11"
        Me.TextBox11.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0000001192092896R), Telerik.Reporting.Drawing.Unit.Inch(0.30000001192092896R))
        Me.TextBox11.StyleName = "Data"
        Me.TextBox11.Value = " PO No"
        '
        'TextBox12
        '
        Me.TextBox12.CanGrow = True
        Me.TextBox12.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.8750786781311035R), Telerik.Reporting.Drawing.Unit.Inch(0.74799555540084839R))
        Me.TextBox12.Name = "TextBox12"
        Me.TextBox12.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.8749210834503174R), Telerik.Reporting.Drawing.Unit.Inch(0.30000001192092896R))
        Me.TextBox12.StyleName = "Data"
        Me.TextBox12.Value = "= Fields.po_no"
        '
        'TextBox13
        '
        Me.TextBox13.CanGrow = True
        Me.TextBox13.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.875R), Telerik.Reporting.Drawing.Unit.Inch(1.0480743646621704R))
        Me.TextBox13.Name = "TextBox13"
        Me.TextBox13.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0000001192092896R), Telerik.Reporting.Drawing.Unit.Inch(0.30000001192092896R))
        Me.TextBox13.StyleName = "Data"
        Me.TextBox13.Value = " PO Date"
        '
        'TextBox14
        '
        Me.TextBox14.CanGrow = True
        Me.TextBox14.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.8750786781311035R), Telerik.Reporting.Drawing.Unit.Inch(1.0480743646621704R))
        Me.TextBox14.Name = "TextBox14"
        Me.TextBox14.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.8749210834503174R), Telerik.Reporting.Drawing.Unit.Inch(0.30000001192092896R))
        Me.TextBox14.StyleName = "Data"
        Me.TextBox14.Value = "= Fields.po_date"
        '
        'TextBox15
        '
        Me.TextBox15.CanGrow = True
        Me.TextBox15.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.875R), Telerik.Reporting.Drawing.Unit.Inch(1.3480744361877441R))
        Me.TextBox15.Name = "TextBox15"
        Me.TextBox15.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0R), Telerik.Reporting.Drawing.Unit.Inch(0.30000001192092896R))
        Me.TextBox15.StyleName = "Data"
        Me.TextBox15.Value = " Delivery Date"
        '
        'TextBox16
        '
        Me.TextBox16.CanGrow = True
        Me.TextBox16.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.8750786781311035R), Telerik.Reporting.Drawing.Unit.Inch(1.3483105897903442R))
        Me.TextBox16.Name = "TextBox16"
        Me.TextBox16.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.8749210834503174R), Telerik.Reporting.Drawing.Unit.Inch(0.30000001192092896R))
        Me.TextBox16.StyleName = "Data"
        Me.TextBox16.Value = "= Fields.delivery_date"
        '
        'TextBox17
        '
        Me.TextBox17.CanGrow = True
        Me.TextBox17.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.875R), Telerik.Reporting.Drawing.Unit.Inch(1.648389458656311R))
        Me.TextBox17.Name = "TextBox17"
        Me.TextBox17.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0000001192092896R), Telerik.Reporting.Drawing.Unit.Inch(0.30000001192092896R))
        Me.TextBox17.StyleName = "Data"
        Me.TextBox17.Value = " PR No"
        '
        'TextBox18
        '
        Me.TextBox18.CanGrow = True
        Me.TextBox18.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.8749997615814209R), Telerik.Reporting.Drawing.Unit.Inch(1.9484683275222778R))
        Me.TextBox18.Name = "TextBox18"
        Me.TextBox18.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0000001192092896R), Telerik.Reporting.Drawing.Unit.Inch(0.30000001192092896R))
        Me.TextBox18.StyleName = "Data"
        Me.TextBox18.Value = ""
        '
        'TextBox19
        '
        Me.TextBox19.CanGrow = True
        Me.TextBox19.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.8750786781311035R), Telerik.Reporting.Drawing.Unit.Inch(1.6483896970748901R))
        Me.TextBox19.Name = "TextBox19"
        Me.TextBox19.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.8749210834503174R), Telerik.Reporting.Drawing.Unit.Inch(0.30000001192092896R))
        Me.TextBox19.StyleName = "Data"
        Me.TextBox19.Value = "= Fields.pr_no"
        '
        'TextBox20
        '
        Me.TextBox20.CanGrow = True
        Me.TextBox20.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.8750786781311035R), Telerik.Reporting.Drawing.Unit.Inch(1.9484685659408569R))
        Me.TextBox20.Name = "TextBox20"
        Me.TextBox20.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.8749210834503174R), Telerik.Reporting.Drawing.Unit.Inch(0.30000001192092896R))
        Me.TextBox20.StyleName = "Data"
        Me.TextBox20.Value = ""
        '
        'detail
        '
        Me.detail.Height = Telerik.Reporting.Drawing.Unit.Inch(0.4895835816860199R)
        Me.detail.Items.AddRange(New Telerik.Reporting.ReportItemBase() {Me.po_item_noDataTextBox, Me.item_nameDataTextBox, Me.merkDataTextBox, Me.quantityDataTextBox, Me.priceDataTextBox, Me.subtotal_itemDataTextBox, Me.TextBox22})
        Me.detail.Name = "detail"
        '
        'po_item_noDataTextBox
        '
        Me.po_item_noDataTextBox.CanGrow = True
        Me.po_item_noDataTextBox.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.02083333395421505R), Telerik.Reporting.Drawing.Unit.Inch(0.02083333395421505R))
        Me.po_item_noDataTextBox.Name = "po_item_noDataTextBox"
        Me.po_item_noDataTextBox.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.37999999523162842R), Telerik.Reporting.Drawing.Unit.Inch(0.40000000596046448R))
        Me.po_item_noDataTextBox.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center
        Me.po_item_noDataTextBox.StyleName = "Data"
        Me.po_item_noDataTextBox.Value = "=Fields.po_item_no"
        '
        'item_nameDataTextBox
        '
        Me.item_nameDataTextBox.CanGrow = True
        Me.item_nameDataTextBox.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.40091195702552795R), Telerik.Reporting.Drawing.Unit.Inch(0.02083333395421505R))
        Me.item_nameDataTextBox.Name = "item_nameDataTextBox"
        Me.item_nameDataTextBox.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.4990882873535156R), Telerik.Reporting.Drawing.Unit.Inch(0.40000000596046448R))
        Me.item_nameDataTextBox.StyleName = "Data"
        Me.item_nameDataTextBox.Value = "=Fields.item_name"
        '
        'merkDataTextBox
        '
        Me.merkDataTextBox.CanGrow = True
        Me.merkDataTextBox.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.9000790119171143R), Telerik.Reporting.Drawing.Unit.Inch(0.02083333395421505R))
        Me.merkDataTextBox.Name = "merkDataTextBox"
        Me.merkDataTextBox.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1999210119247437R), Telerik.Reporting.Drawing.Unit.Inch(0.40000000596046448R))
        Me.merkDataTextBox.StyleName = "Data"
        Me.merkDataTextBox.Value = "=Fields.merk"
        '
        'quantityDataTextBox
        '
        Me.quantityDataTextBox.CanGrow = True
        Me.quantityDataTextBox.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.0002365112304687R), Telerik.Reporting.Drawing.Unit.Inch(0.02083333395421505R))
        Me.quantityDataTextBox.Name = "quantityDataTextBox"
        Me.quantityDataTextBox.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.69976359605789185R), Telerik.Reporting.Drawing.Unit.Inch(0.40000000596046448R))
        Me.quantityDataTextBox.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right
        Me.quantityDataTextBox.StyleName = "Data"
        Me.quantityDataTextBox.Value = "=Fields.quantity"
        '
        'priceDataTextBox
        '
        Me.priceDataTextBox.CanGrow = True
        Me.priceDataTextBox.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.7000002861022949R), Telerik.Reporting.Drawing.Unit.Inch(0.02083333395421505R))
        Me.priceDataTextBox.Name = "priceDataTextBox"
        Me.priceDataTextBox.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0R), Telerik.Reporting.Drawing.Unit.Inch(0.40000000596046448R))
        Me.priceDataTextBox.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right
        Me.priceDataTextBox.StyleName = "Data"
        Me.priceDataTextBox.Value = "=Fields.price"
        '
        'subtotal_itemDataTextBox
        '
        Me.subtotal_itemDataTextBox.CanGrow = True
        Me.subtotal_itemDataTextBox.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.7013874053955078R), Telerik.Reporting.Drawing.Unit.Inch(0.02083333395421505R))
        Me.subtotal_itemDataTextBox.Name = "subtotal_itemDataTextBox"
        Me.subtotal_itemDataTextBox.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0486127138137817R), Telerik.Reporting.Drawing.Unit.Inch(0.40000000596046448R))
        Me.subtotal_itemDataTextBox.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right
        Me.subtotal_itemDataTextBox.StyleName = "Data"
        Me.subtotal_itemDataTextBox.Value = "=Fields.subtotal_item"
        '
        'TextBox22
        '
        Me.TextBox22.CanGrow = True
        Me.TextBox22.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.0999999046325684R), Telerik.Reporting.Drawing.Unit.Inch(0.02083333395421505R))
        Me.TextBox22.Name = "TextBox22"
        Me.TextBox22.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.89999997615814209R), Telerik.Reporting.Drawing.Unit.Inch(0.40000000596046448R))
        Me.TextBox22.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center
        Me.TextBox22.StyleName = "Data"
        Me.TextBox22.Value = "= Fields.uop"
        '
        'PrintPO
        '
        Me.DataSource = Me.sql_print_po
        Group1.GroupFooter = Me.labelsGroupFooterSection
        Group1.GroupHeader = Me.labelsGroupHeaderSection
        Group1.Name = "labelsGroup"
        Me.Groups.AddRange(New Telerik.Reporting.Group() {Group1})
        Me.Items.AddRange(New Telerik.Reporting.ReportItemBase() {Me.labelsGroupHeaderSection, Me.labelsGroupFooterSection, Me.reportFooter, Me.pageFooter, Me.reportHeader, Me.detail})
        Me.Name = "Report1"
        Me.PageSettings.Margins = New Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Inch(0.25R), Telerik.Reporting.Drawing.Unit.Inch(0.25R), Telerik.Reporting.Drawing.Unit.Inch(0.25R), Telerik.Reporting.Drawing.Unit.Inch(0.25R))
        Me.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4
        ReportParameter1.Name = "popar"
        ReportParameter1.Text = "popar"
        ReportParameter1.Visible = True
        Me.ReportParameters.Add(ReportParameter1)
        Me.Style.BackgroundColor = System.Drawing.Color.White
        StyleRule1.Selectors.AddRange(New Telerik.Reporting.Drawing.ISelector() {New Telerik.Reporting.Drawing.StyleSelector("Title")})
        StyleRule1.Style.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(241, Byte), Integer), CType(CType(238, Byte), Integer), CType(CType(232, Byte), Integer))
        StyleRule1.Style.Color = System.Drawing.Color.FromArgb(CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer))
        StyleRule1.Style.Font.Name = "Verdana"
        StyleRule1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(18.0R)
        StyleRule2.Selectors.AddRange(New Telerik.Reporting.Drawing.ISelector() {New Telerik.Reporting.Drawing.StyleSelector("Caption")})
        StyleRule2.Style.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer))
        StyleRule2.Style.Color = System.Drawing.Color.FromArgb(CType(CType(227, Byte), Integer), CType(CType(222, Byte), Integer), CType(CType(209, Byte), Integer))
        StyleRule2.Style.Font.Name = "Verdana"
        StyleRule2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10.0R)
        StyleRule2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle
        StyleRule3.Selectors.AddRange(New Telerik.Reporting.Drawing.ISelector() {New Telerik.Reporting.Drawing.StyleSelector("Data")})
        StyleRule3.Style.Font.Name = "Verdana"
        StyleRule3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9.0R)
        StyleRule3.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle
        StyleRule4.Selectors.AddRange(New Telerik.Reporting.Drawing.ISelector() {New Telerik.Reporting.Drawing.StyleSelector("PageInfo")})
        StyleRule4.Style.Color = System.Drawing.Color.FromArgb(CType(CType(160, Byte), Integer), CType(CType(160, Byte), Integer), CType(CType(160, Byte), Integer))
        StyleRule4.Style.Font.Name = "Verdana"
        StyleRule4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.0R)
        StyleRule4.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle
        Me.StyleSheet.AddRange(New Telerik.Reporting.Drawing.StyleRule() {StyleRule1, StyleRule2, StyleRule3, StyleRule4})
        Me.Width = Telerik.Reporting.Drawing.Unit.Inch(7.75R)
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Friend WithEvents sql_print_po As Telerik.Reporting.SqlDataSource
    Friend WithEvents labelsGroupHeaderSection As Telerik.Reporting.GroupHeaderSection
    Friend WithEvents po_item_noCaptionTextBox As Telerik.Reporting.TextBox
    Friend WithEvents item_nameCaptionTextBox As Telerik.Reporting.TextBox
    Friend WithEvents merkCaptionTextBox As Telerik.Reporting.TextBox
    Friend WithEvents quantityCaptionTextBox As Telerik.Reporting.TextBox
    Friend WithEvents priceCaptionTextBox As Telerik.Reporting.TextBox
    Friend WithEvents subtotal_itemCaptionTextBox As Telerik.Reporting.TextBox
    Friend WithEvents labelsGroupFooterSection As Telerik.Reporting.GroupFooterSection
    Friend WithEvents reportFooter As Telerik.Reporting.ReportFooterSection
    Friend WithEvents TextBox1 As Telerik.Reporting.TextBox
    Friend WithEvents pageFooter As Telerik.Reporting.PageFooterSection
    Friend WithEvents reportHeader As Telerik.Reporting.ReportHeaderSection
    Friend WithEvents titleTextBox As Telerik.Reporting.TextBox
    Friend WithEvents detail As Telerik.Reporting.DetailSection
    Friend WithEvents po_item_noDataTextBox As Telerik.Reporting.TextBox
    Friend WithEvents item_nameDataTextBox As Telerik.Reporting.TextBox
    Friend WithEvents merkDataTextBox As Telerik.Reporting.TextBox
    Friend WithEvents quantityDataTextBox As Telerik.Reporting.TextBox
    Friend WithEvents priceDataTextBox As Telerik.Reporting.TextBox
    Friend WithEvents subtotal_itemDataTextBox As Telerik.Reporting.TextBox
    Friend WithEvents TextBox2 As Telerik.Reporting.TextBox
    Friend WithEvents TextBox3 As Telerik.Reporting.TextBox
    Friend WithEvents TextBox4 As Telerik.Reporting.TextBox
    Friend WithEvents TextBox5 As Telerik.Reporting.TextBox
    Friend WithEvents TextBox6 As Telerik.Reporting.TextBox
    Friend WithEvents TextBox7 As Telerik.Reporting.TextBox
    Friend WithEvents TextBox8 As Telerik.Reporting.TextBox
    Friend WithEvents TextBox9 As Telerik.Reporting.TextBox
    Friend WithEvents TextBox10 As Telerik.Reporting.TextBox
    Friend WithEvents TextBox11 As Telerik.Reporting.TextBox
    Friend WithEvents TextBox12 As Telerik.Reporting.TextBox
    Friend WithEvents TextBox13 As Telerik.Reporting.TextBox
    Friend WithEvents TextBox14 As Telerik.Reporting.TextBox
    Friend WithEvents TextBox15 As Telerik.Reporting.TextBox
    Friend WithEvents TextBox16 As Telerik.Reporting.TextBox
    Friend WithEvents TextBox17 As Telerik.Reporting.TextBox
    Friend WithEvents TextBox18 As Telerik.Reporting.TextBox
    Friend WithEvents TextBox19 As Telerik.Reporting.TextBox
    Friend WithEvents TextBox20 As Telerik.Reporting.TextBox
    Friend WithEvents TextBox21 As Telerik.Reporting.TextBox
    Friend WithEvents TextBox22 As Telerik.Reporting.TextBox
    Friend WithEvents TextBox31 As Telerik.Reporting.TextBox
    Friend WithEvents TextBox32 As Telerik.Reporting.TextBox
    Friend WithEvents TextBox33 As Telerik.Reporting.TextBox
    Friend WithEvents TextBox34 As Telerik.Reporting.TextBox
    Friend WithEvents TextBox35 As Telerik.Reporting.TextBox
    Friend WithEvents TextBox36 As Telerik.Reporting.TextBox
    Friend WithEvents TextBox37 As Telerik.Reporting.TextBox
    Friend WithEvents TextBox38 As Telerik.Reporting.TextBox
    Friend WithEvents TextBox39 As Telerik.Reporting.TextBox
    Friend WithEvents TextBox40 As Telerik.Reporting.TextBox
    Friend WithEvents TextBox41 As Telerik.Reporting.TextBox
    Friend WithEvents TextBox42 As Telerik.Reporting.TextBox
    Friend WithEvents TextBox43 As Telerik.Reporting.TextBox
    Friend WithEvents TextBox44 As Telerik.Reporting.TextBox
    Friend WithEvents TextBox30 As Telerik.Reporting.TextBox
    Friend WithEvents TextBox29 As Telerik.Reporting.TextBox
    Friend WithEvents TextBox28 As Telerik.Reporting.TextBox
    Friend WithEvents TextBox27 As Telerik.Reporting.TextBox
    Friend WithEvents TextBox26 As Telerik.Reporting.TextBox
    Friend WithEvents TextBox25 As Telerik.Reporting.TextBox
    Friend WithEvents TextBox24 As Telerik.Reporting.TextBox
    Friend WithEvents TextBox23 As Telerik.Reporting.TextBox
End Class