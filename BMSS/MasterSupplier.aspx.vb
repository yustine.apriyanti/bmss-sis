﻿Imports MySql.Data
Imports MySql.Data.MySqlClient
Imports Telerik.Web.UI

Public Class MasterSupplier
    Inherits BaseClass

    Dim oConn As New MySqlConnection(ConfigurationManager.ConnectionStrings("conn_str").ConnectionString)
    Dim create_by As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("login") Is Nothing Then
            Response.Redirect("Login.aspx")
        Else
            If Not IsPostBack Then
                txt_supplier_id.Text = GenerateNo()
            End If
        End If
    End Sub

    Function GenerateNo() As String
        Dim kode As Integer
        Dim no As String
        oConn.Open()
        Dim sqlCmd2 As New MySqlCommand("select * from supplier", oConn)
        Dim oDatareader As MySqlDataReader
        oDatareader = sqlCmd2.ExecuteReader()
        If oDatareader.HasRows() Then
            oDatareader.Close()
            Dim sqlCmd As New MySqlCommand("SELECT MAX(RIGHT(supplier_id,4)) + 1 FROM supplier LIMIT 1", oConn)
            kode = sqlCmd.ExecuteScalar().ToString()
            oConn.Close()
            If kode.ToString() = "" Then
                no = 1
            Else
                no = Convert.ToInt16(kode.ToString())
            End If
        Else
            no = 1
            oConn.Close()
        End If
        Return "S" & Right("0000" + no.ToString(), 4)
    End Function

    Sub ClearData()
        txt_supplier_id.Text = GenerateNo()
        txt_supplier_name.Text = ""
        txt_address.Text = ""
        txt_contact_name.Text = ""
        txt_phone.Text = ""
        txt_fax.Text = ""
        txt_email.Text = ""
        rg_supplier.Rebind()
        lbl_msg.Text = ""
        cb_active.Enabled = False
        cb_active.Checked = True
        btn_submit.Text = "Submit"
        btn_clear.Text = "Clear"
    End Sub

    'Protected Sub rg_supplier_ItemCreated(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles rg_supplier.ItemCreated
    '    If TypeOf e.Item Is GridEditableItem AndAlso e.Item.IsInEditMode Then
    '        If Not (TypeOf e.Item Is GridEditFormInsertItem) Then
    '            Dim item As GridEditableItem = TryCast(e.Item, GridEditableItem)
    '            Dim manager As GridEditManager = item.EditManager
    '            Dim editor As GridTextBoxColumnEditor = TryCast(manager.GetColumnEditor("supplier_id"), GridTextBoxColumnEditor)
    '            editor.TextBoxControl.Enabled = False
    '        End If
    '    End If
    'End Sub

    'Protected Sub rg_supplier_ItemInserted(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridInsertedEventArgs) Handles rg_supplier.ItemInserted
    '    If e.Exception IsNot Nothing Then

    '        e.ExceptionHandled = True

    '        SetMessage("Data cannot be inserted. Reason: " + e.Exception.Message)
    '    Else
    '        SetMessage("New data is inserted!")
    '    End If
    'End Sub

    'Private Sub DisplayMessage(ByVal text As String)
    '    rg_supplier.Controls.Add(New LiteralControl(String.Format("<span style='color:red'>{0}</span>", text)))
    'End Sub

    'Private Sub SetMessage(ByVal message As String)
    '    gridMessage = message
    'End Sub

    'Private gridMessage As String = Nothing

    'Protected Sub rg_supplier_PreRender(ByVal sender As Object, ByVal e As EventArgs) Handles rg_supplier.PreRender
    '    If Not String.IsNullOrEmpty(gridMessage) Then
    '        DisplayMessage(gridMessage)
    '    End If
    'End Sub

    'Protected Sub rg_supplier_ItemDeleted(ByVal source As Object, ByVal e As Telerik.Web.UI.GridDeletedEventArgs) Handles rg_supplier.ItemDeleted
    '    If Not e.Exception Is Nothing Then
    '        e.ExceptionHandled = True
    '    End If
    'End Sub

    Protected Sub rg_supplier_ItemCommand(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs) Handles rg_supplier.ItemCommand
        If e.CommandName = "choose" Then
            Dim item As GridDataItem = CType(e.Item, GridDataItem)
            txt_supplier_id.Text = item("supplier_id").Text
            txt_supplier_name.Text = item("supplier_name").Text
            txt_address.Text = item("address").Text
            txt_contact_name.Text = item("contact_name").Text
            txt_phone.Text = item("phone").Text
            txt_fax.Text = item("fax").Text
            txt_email.Text = item("email").Text
            If item("active").Text = "0" Then
                cb_active.Checked = False
            Else
                cb_active.Checked = True
            End If
            cb_active.Enabled = True
            btn_submit.Text = "Update"
            btn_clear.Text = "Cancel"
        End If
    End Sub

    Protected Sub btn_submit_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_submit.Click
        create_by = Session("username").ToString()
        Dim create_date As String = Date.Now.ToString("yyyy-MM-dd HH:mm:ss")
        Dim active As String = ""
        Dim msg As String = ""
        If cb_active.Checked = True Then
            active = "1"
        Else
            active = "0"
        End If

        Try
            oConn.Open()
            If btn_submit.Text = "Submit" Then
                msg = "Insert transaction"
                Dim sql_insert As New MySqlCommand("INSERT INTO supplier (supplier_id, supplier_name, address, contact_name, phone, fax, email, create_by, create_date, active) values('" & txt_supplier_id.Text & "', UPPER('" & txt_supplier_name.Text & "'), UPPER('" & txt_address.Text & "'), UPPER('" & txt_contact_name.Text & "'), '" & txt_phone.Text & "', '" & txt_fax.Text & "', UPPER('" & txt_email.Text & "'), UPPER('" & create_by & "'), '" & create_date & "', '" & active & "')", oConn)
                sql_insert.ExecuteNonQuery()
            ElseIf btn_submit.Text = "Update" Then
                msg = "Update transaction"
                Dim sql_update As New MySqlCommand("UPDATE supplier SET supplier_name = UPPER('" & txt_supplier_name.Text & "'), address = UPPER('" & txt_address.Text & "'), contact_name = '" & txt_contact_name.Text & "', phone = '" & txt_phone.Text & "', fax = '" & txt_fax.Text & "', email = UPPER('" & txt_email.Text & "'), active = '" & active & "', update_by = UPPER('" & create_by & "'), update_date = '" & create_date & "' where supplier_id = '" & txt_supplier_id.Text & "' ", oConn)
                sql_update.ExecuteNonQuery()
            End If
            oConn.Close()

            ClearData()
            lbl_msg.Text = msg & " success"
            lbl_msg.CssClass = "text-success"
        Catch ex As Exception
            oConn.Close()
            MessageException(msg & " failed", ex, lbl_msg, "text-warning")
        End Try
    End Sub

    Protected Sub btn_clear_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_clear.Click
        ClearData()
    End Sub
End Class