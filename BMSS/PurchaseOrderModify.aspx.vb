﻿Imports MySql.Data
Imports MySql.Data.MySqlClient
Imports Telerik.Web.UI

Public Class PurchaseOrderModify
    Inherits System.Web.UI.Page

    Dim oConn As New MySqlConnection(ConfigurationManager.ConnectionStrings("conn_str").ConnectionString)

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("login") Is Nothing Then
            Response.Redirect("LoginForm.aspx")
        Else
            If Not IsPostBack Then
                rn_discount.Text = 0
                rn_ppn.Text = 0
                rn_shipping.Text = 0
                rn_total.Text = 0
            End If
        End If
    End Sub

    Function GetItemNo() As String
        oConn.Open()
        Dim kode As Integer
        Dim no As String
        Dim sqlCmd2 As New MySqlCommand("select * from purchase_order_item where po_no = '" & rt_poNO.Text & "'", oConn)
        Dim oDatareader As MySqlDataReader
        oDatareader = sqlCmd2.ExecuteReader()
        If oDatareader.HasRows() Then
            oDatareader.Close()
            Dim sqlCmd As New MySqlCommand("SELECT MAX(po_item_no) + 1 FROM purchase_order_item where po_no = '" & rt_poNO.Text & "' LIMIT 1", oConn)
            kode = sqlCmd.ExecuteScalar().ToString()
            If kode.ToString() = "" Then
                no = 1
            Else
                no = Convert.ToInt16(kode.ToString()).ToString()
            End If
        Else
            oDatareader.Close()
            no = 1
        End If
        oConn.Close()
        Return no
    End Function

    Sub ClearItem()
        rt_description.Text = ""
        rc_UOM.ClearSelection()
        rn_quantity.Text = ""
        rn_unitPrice.Text = ""
        rt_itemNo.Text = GetItemNo()
        'rg_temp.Rebind()
    End Sub

    Protected Sub rg_temp_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles rg_temp.SelectedIndexChanged
        rb_update.Visible = True
        rb_addItems.Text = "Cancel"
        Dim item As GridDataItem = DirectCast(rg_temp.SelectedItems(0), GridDataItem)
        rt_itemNo.Text = item.Item("po_item_no").Text
        rt_description.Text = item.Item("part_desc").Text
        rn_quantity.Text = item.Item("quantity").Text
        rn_unitPrice.Text = item.Item("price").Text
        rc_UOM.SelectedValue = item.Item("unit").Text
    End Sub

    Protected Sub rg_temp_ItemDeleted(ByVal source As Object, ByVal e As Telerik.Web.UI.GridDeletedEventArgs) Handles rg_temp.ItemDeleted
        If Not e.Exception Is Nothing Then
            e.ExceptionHandled = True
        Else
            rt_itemNo.Text = GetItemNo()
        End If
    End Sub

    Protected Sub rb_addItems_Click(ByVal sender As Object, ByVal e As EventArgs) Handles rb_addItems.Click
        If rb_addItems.Text = "Cancel" Then
            rb_addItems.Text = "Add Items"
            rb_update.Visible = False
            rt_itemNo.Text = GetItemNo()
            ClearItem()
        Else
            oConn.Open()
            Dim sqlCmd As New MySqlCommand("Insert into purchase_order_item (po_no, po_item_no, part_desc, quantity, unit, price, subtotal, created_by, created_date) " & _
                                           "values ('" & rt_poNO.Text & "', '" & rt_itemNo.Text & "', '" & rt_description.Text & "', " & Decimal.Parse(rn_quantity.Text) & ", '" & rc_UOM.SelectedValue.ToString() & "', " & Decimal.Parse(rn_unitPrice.Text) & ", " & Decimal.Parse(rn_quantity.Text) * Decimal.Parse(rn_unitPrice.Text) & ", '" & Session("username").ToString() & "', '" & DateTime.Now.ToString("yyyy-MM-dd") & "') ", oConn)
            sqlCmd.ExecuteNonQuery()
            oConn.Close()
            rg_temp.Rebind()
            ClearItem()
            rt_itemNo.Text = GetItemNo()
        End If
    End Sub

    Protected Sub rb_update_Click(ByVal sender As Object, ByVal e As EventArgs) Handles rb_update.Click
        oConn.Open()
        'Dim oDatareader As MySqlDataReader
        Dim sqlCmd As New MySqlCommand("update purchase_order_item set part_desc = '" & rt_description.Text & "', quantity = " & Decimal.Parse(rn_quantity.Text) & ", " & _
                                       "unit = '" & rc_UOM.SelectedValue.ToString() & "', price = " & Decimal.Parse(rn_unitPrice.Text) & ", subtotal = " & Decimal.Parse(rn_quantity.Text) * Decimal.Parse(rn_unitPrice.Text) & " " & _
                                       "where po_no = '" & rt_poNO.Text & "' and po_item_no = '" & rt_itemNo.Text & "'", oConn)
        sqlCmd.ExecuteNonQuery()
        oConn.Close()
        rg_temp.Rebind()
        'Dim sqlCmd21 As New MySqlCommand("select * from purchase_order_item where po_no = '" & rt_poNO.Text & "'", oConn)
        'oDatareader = sqlCmd21.ExecuteReader()
        'rg_temp.DataSource = oDatareader
        'rg_temp.DataBind()
        'oDatareader.Close()
        oConn.Close()


        ClearItem()
        rb_update.Visible = False
        rb_addItems.Text = "Add Items"
        rt_itemNo.Text = GetItemNo()
    End Sub

    Protected Sub rb_calculate_Click(ByVal sender As Object, ByVal e As EventArgs) Handles rb_calculate.Click
        Dim subtotal As Decimal = 0
        oConn.Open()
        Dim oDatareader As MySqlDataReader
        Dim sqlCmd As New MySqlCommand("select sum(subtotal) as subtotal from purchase_order_item where po_no = '" & rt_poNO.Text & "'", oConn)
        oDatareader = sqlCmd.ExecuteReader()
        If oDatareader.HasRows() Then
            If oDatareader.Read() Then
                subtotal = Decimal.Parse(oDatareader("subtotal").ToString())
            End If
        Else
            subtotal = 0
        End If
        oConn.Close()
        rn_total.Text = subtotal - Decimal.Parse(rn_discount.Text) + Decimal.Parse(rn_ppn.Text) + Decimal.Parse(rn_shipping.Text)
    End Sub

    Protected Sub rb_modify_Click(ByVal sender As Object, ByVal e As EventArgs) Handles rb_modify.Click
        lbl_msg.Text = ""
        oConn.Open()
        Dim sqlCmd As New MySqlCommand("select p.* from purchase_order p LEFT JOIN purchase_order_item d ON p.po_no = d.po_no where p.po_no = '" & rt_poNO.Text & "' ", oConn)
        Dim oDatareader As MySqlDataReader
        oDatareader = sqlCmd.ExecuteReader()
        If oDatareader.HasRows() Then
            If oDatareader.Read() Then
                rd_poDate.SelectedDate = Date.Parse(oDatareader("po_date").ToString())
                rc_supplier.SelectedValue = oDatareader("SupplierID").ToString()
                rd_deliveryDate.SelectedDate = Date.Parse(oDatareader("delivery_date").ToString())
                rd_prDate.SelectedDate = Date.Parse(oDatareader("pr_date").ToString())
                rt_deliveryLocation.Text = oDatareader("delivery_location").ToString()
                rbl_goods.SelectedValue = oDatareader("goods").ToString()
                rt_prNo.Text = oDatareader("pr_no").ToString()
                rt_paymentTerms.Text = oDatareader("payment_term").ToString()
                rt_notes.Text = oDatareader("po_remark").ToString()
                rn_discount.Text = oDatareader("discount").ToString()
                rn_ppn.Text = oDatareader("ppn").ToString()
                rn_shipping.Text = oDatareader("transportation").ToString()
                rn_total.Text = oDatareader("total").ToString()
                rc_currency.SelectedValue = oDatareader("currency").ToString()
                oDatareader.Close()
                rg_temp.Rebind()
                'Dim sqlCmd21 As New MySqlCommand("select * from purchase_order_item where po_no = '" & rt_poNO.Text & "'", oConn)
                'oDatareader = sqlCmd21.ExecuteReader()
                'rg_temp.DataSource = oDatareader
                'rg_temp.DataBind()
                'oDatareader.Close()

                Dim sqlCmd2 As New MySqlCommand("select * from suppliers where SupplierID = '" & rc_supplier.SelectedValue.ToString() & "'", oConn)
                oDatareader = sqlCmd2.ExecuteReader()
                If oDatareader.HasRows() Then
                    If oDatareader.Read() Then
                        rt_address.Text = oDatareader("Address").ToString()
                        rt_phone.Text = oDatareader("Phone").ToString()
                        rt_fax.Text = oDatareader("Fax").ToString()
                        rt_contactPerson.Text = oDatareader("ContactName").ToString()
                    End If
                End If
                oDatareader.Close()
            End If
        Else
            lbl_msg.Text = "PO No not exist"
        End If
        oConn.Close()
    End Sub

    Protected Sub rb_clearForm_Click(ByVal sender As Object, ByVal e As EventArgs) Handles rb_clearForm.Click
        DropDataTemp()
        ClearItem()
        Response.Redirect("PurchaseOrderModify.aspx")
    End Sub

    Sub DropDataTemp()
        oConn.Open()
        rt_poNO.Text = ""
        rg_temp.Rebind()
        oConn.Close()
    End Sub

    Protected Sub rb_submit_Click(ByVal sender As Object, ByVal e As EventArgs) Handles rb_submit.Click
        oConn.Open()
        Dim subtotal As Decimal = 0
        Dim oDatareader As MySqlDataReader
        Dim sqlCmd1 As New MySqlCommand("select sum(subtotal) as subtotal from purchase_order_item where po_no = '" & rt_poNO.Text & "'", oConn)
        oDatareader = sqlCmd1.ExecuteReader()
        If oDatareader.HasRows() Then
            If oDatareader.Read() Then
                subtotal = Decimal.Parse(oDatareader("subtotal").ToString())
            End If
        Else
            subtotal = 0
        End If
        oDatareader.Close()
        Dim sqlCmd2 As New MySqlCommand("update purchase_order set po_date = '" & Format(rd_poDate.SelectedDate, "yyyy-MM-dd") & "', delivery_date = '" & Format(rd_deliveryDate.SelectedDate, "yyyy-MM-dd HH:mm:ss") & "', " & _
                                        "delivery_location = '" & rt_deliveryLocation.Text & "', payment_term = '" & rt_paymentTerms.Text & "', po_remark = '" & rt_notes.Text & "', " & _
                                        "subtotal = " & subtotal & ", discount = " & Decimal.Parse(rn_discount.Text) & ", ppn = " & Decimal.Parse(rn_ppn.Text) & ", transportation = " & Decimal.Parse(rn_shipping.Text) & ", total = " & Decimal.Parse(rn_total.Text) & ", currency = '" & rc_currency.SelectedValue.ToString() & "', " & _
                                        "update_by = '" & Session("username").ToString() & "', update_date = '" & DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") & "' where po_no = '" & rt_poNO.Text & "'", oConn)
        sqlCmd2.ExecuteNonQuery()

        Dim sqlCmd As New MySqlCommand("update purchase_order_item set update_by = '" & Session("username").ToString() & "', update_date = '" & DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") & "' where po_no = '" & rt_poNO.Text & "'", oConn)
        sqlCmd.ExecuteNonQuery()
        oConn.Close()
        Dim message As String = "PO No " & rt_poNO.Text & " updated!"
        Page.ClientScript.RegisterStartupScript(Me.GetType(), "popup", "alert('" & message & "');window.location='PurchaseOrderModify.aspx';", True)
    End Sub
End Class