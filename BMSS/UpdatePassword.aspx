﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/WhiteDesign.Master" CodeBehind="UpdatePassword.aspx.vb" Inherits="BMSS.UpdatePassword" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">UPDATE PASSWORD</h1>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-6">
                        <form role="form">
                            <div class="form-group">
                                <label>New Password</label>
                                <asp:TextBox ID="txt_new_password" runat="server" placeholder="New Password" autofocus class="form-control" TextMode="Password"></asp:TextBox>
                            </div>
                            <div class="form-group">
                                <label>Confirm New Password</label>
                                <asp:TextBox ID="txt_confirm_password" runat="server" placeholder="Confirm New Password" class="form-control" TextMode="Password"></asp:TextBox>
                                <asp:CompareValidator ID="CompareValidator2" runat="server" ErrorMessage="Incorrect confirm password" 
                                    ValidationGroup="pass" ControlToCompare="txt_new_password" ControlToValidate="txt_confirm_password" class="text-warning"></asp:CompareValidator>
                            </div>
                            <div class="form-group">
                                <asp:Button ID="btn_submit" runat="server" Text="Update" class="btn btn-primary"/> 
                            </div>
                            <div class="form-group">
                                <asp:Label ID="lbl_msg" runat="server" Text="" class="text-success"></asp:Label>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</asp:Content>
