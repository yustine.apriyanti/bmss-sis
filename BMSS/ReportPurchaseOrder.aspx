﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/WhiteDesign.Master" CodeBehind="ReportPurchaseOrder.aspx.vb" Inherits="BMSS.ReportPurchaseOrder" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Purchase Order Report</h1>
    </div>
</div>


<asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
    <ContentTemplate> 
        <div class="row">
            <form role="form">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label>Period</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <asp:DropDownList ID="ddl_period" runat="server" class="form-control" EmptyMessage="Select" 
                                            DataSourceID="sql_period" DataTextField="period" 
                                            DataValueField="period" ResolvedRenderMode="Classic" autofocus 
                                                AutoPostBack="True" >
                                            </asp:DropDownList> 
                                            <asp:SqlDataSource ID="sql_period" runat="server" 
                                                ConnectionString="<%$ ConnectionStrings:conn_str %>" 
                                                ProviderName="<%$ ConnectionStrings:conn_str.ProviderName %>" 
                                                SelectCommand="SELECT DISTINCT DATE_FORMAT(trans_date,'%Y-%m') as period from transaction 
                                                                UNION 
                                                                SELECT DATE_FORMAT(DATE_ADD(max(trans_date),INTERVAL 1 MONTH),'%Y-%m') as period from transaction">
                                            </asp:SqlDataSource>  
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server">
                                    </telerik:RadAjaxLoadingPanel>
                                    <telerik:RadGrid ID="rg_data" runat="server" DataSourceID="sql_data" 
                                        Skin="Bootstrap">
                                        <ClientSettings>
                                            <Scrolling AllowScroll="True" ScrollHeight="500px" UseStaticHeaders="True" />
                                        </ClientSettings>
                                        <MasterTableView AutoGenerateColumns="true" DataSourceID="sql_data" ShowGroupFooter="true">
                                            <Columns>
                                            </Columns>
                                        </MasterTableView>
                                        <FooterStyle Font-Bold="True" />
                                    </telerik:RadGrid>
                                    <asp:SqlDataSource ID="sql_data" runat="server"
                                        ConnectionString="<%$ ConnectionStrings:conn_str %>"
                                        SelectCommand="sp_report_po" SelectCommandType="StoredProcedure" 
                                        ProviderName="<%$ ConnectionStrings:conn_str.ProviderName %>">
		                                <selectparameters>
			                                <asp:ControlParameter ControlID="ddl_period" Name="ParPeriod"/>
		                                </selectparameters>
	                                </asp:SqlDataSource>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </ContentTemplate>
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="ddl_period" EventName="SelectedIndexChanged" />
    </Triggers>
</asp:UpdatePanel>
</asp:Content>
