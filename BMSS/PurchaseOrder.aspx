﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/WhiteDesign.Master" CodeBehind="PurchaseOrder.aspx.vb" Inherits="BMSS.PurchaseOrder" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            <asp:Label runat="server" ID="lbl_header" Text=""/>
        </h1>
    </div>
</div>
<asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
    <ContentTemplate>  
        <div class="row">
            <form role="form">
                <telerik:RadWindowManager RenderMode="Lightweight" runat="server" id="RadWindowManager1"></telerik:RadWindowManager>
                <script type="text/javascript">
                    function refreshToItemData(arg) {
                        window.scrollTo(0, 1190); 
                        window.location.reload();
                    } 
                    function refreshToTop(arg) {
                        window.scrollTo(0, 0);
                        window.location.reload();
                    }
                </script>
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <asp:Panel runat="server" ID="panel_po_search">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>PO No</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <asp:TextBox ID="txt_search_po" runat="server" class="form-control"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="col-lg-2">
                                            <div class="form-group">
                                                <asp:Button ID="btn_search" runat="server" Text="Search" class="btn btn-primary"/> 
                                            </div>
                                        </div>
                                                                                
                                        <div class="col-lg-12">
                                            <asp:Label ID="lbl_msg_search" runat="server" Text="" class="text-warning"></asp:Label>
                                        </div>
                                    </div>
                                </div>
                            </div> 
                        </asp:Panel>
                                          
                        <div class="panel-heading">
                            <label>Supplier Data</label>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Supplier Name</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <asp:DropDownList ID="ddl_supplier" runat="server" class="form-control" EmptyMessage="Select" 
                                            DataSourceID="sql_supplier" DataTextField="supplier_name" 
                                            DataValueField="supplier_id" ResolvedRenderMode="Classic" AutoPostBack="true"  AppendDataBoundItems="True">
                                                <asp:ListItem Value="-1" Selected="True">-- Select Supplier -- </asp:ListItem>
                                            </asp:DropDownList> 
                                            <asp:SqlDataSource ID="sql_supplier" runat="server" 
                                                ConnectionString="<%$ ConnectionStrings:conn_str %>" 
                                                ProviderName="<%$ ConnectionStrings:conn_str.ProviderName %>" 
                                                SelectCommand="SELECT * from supplier order by supplier_name asc">
                                            </asp:SqlDataSource>  
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Address</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <asp:TextBox ID="txt_address" runat="server" class="form-control" 
                                                TextMode="MultiLine" Height="75px" ReadOnly="True" ></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Phone</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <asp:TextBox ID="txt_phone" runat="server" class="form-control" 
                                                ReadOnly="True"  ></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Fax</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <asp:TextBox ID="txt_fax" runat="server" class="form-control" ReadOnly="True"  ></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Contact Person</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <asp:TextBox ID="txt_contactPerson" runat="server" class="form-control" 
                                                ReadOnly="True"  ></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>  
                                        
                        <div class="panel-heading">
                            <label>Purchase Order Data</label>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>PO No</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <asp:TextBox ID="txt_po_no" runat="server" class="form-control" ReadOnly="true" ></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Company Name</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <asp:DropDownList ID="ddl_company" runat="server" class="form-control"
                                            DataSourceID="sql_company" DataTextField="description" 
                                            DataValueField="code" ResolvedRenderMode="Classic" AppendDataBoundItems="True">
                                                <asp:ListItem Value="-1" Selected="True">-- Select company -- </asp:ListItem>
                                            </asp:DropDownList> 
                                            <asp:SqlDataSource ID="sql_company" runat="server" 
                                                ConnectionString="<%$ ConnectionStrings:conn_str %>" 
                                                ProviderName="<%$ ConnectionStrings:conn_str.ProviderName %>" 
                                                SelectCommand="SELECT cat_id, code, description FROM code_list WHERE (cat_id ='COM') and active = 1 order by seq_no ASC">
                                            </asp:SqlDataSource>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>PO Date</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <telerik:RadDatePicker ID="rd_poDate" runat="server" Skin="Bootstrap" Culture="en-US">
                                                <Calendar ID="Calendar4" UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" ViewSelectorText="x" runat="server"></Calendar>
                                                <DateInput ID="DateInput4" DisplayDateFormat="yyyy-MM-dd" DateFormat="yyyy-MM-dd" runat="server"></DateInput>
                                                <DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>
                                            </telerik:RadDatePicker>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>PR No</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <asp:TextBox ID="txt_pr_no" runat="server" class="form-control"  ></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>PR Date</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">  
                                            <telerik:RadDatePicker ID="rd_prDate" runat="server" Skin="Bootstrap" Culture="en-US">
                                                <Calendar ID="Calendar1" UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" ViewSelectorText="x" runat="server"></Calendar>
                                                <DateInput ID="DateInput1" DisplayDateFormat="yyyy-MM-dd" DateFormat="yyyy-MM-dd" runat="server"></DateInput>
                                                <DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>
                                            </telerik:RadDatePicker>  
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Est. Delivery Date</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <telerik:RadDatePicker ID="rd_deliveryDate" runat="server" Skin="Bootstrap" Culture="en-US">
                                                <Calendar ID="Calendar2" UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" ViewSelectorText="x" runat="server"></Calendar>
                                                <DateInput ID="DateInput2" DisplayDateFormat="yyyy-MM-dd" DateFormat="yyyy-MM-dd" runat="server"></DateInput>
                                                <DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>
                                            </telerik:RadDatePicker> 
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Payment Terms</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <asp:TextBox ID="txt_payment_term" runat="server" class="form-control"  ></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Delivery Location</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <asp:TextBox ID="txt_del_loc" runat="server" class="form-control" TextMode="MultiLine" Height="75px"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Notes</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <asp:TextBox ID="txt_notes" runat="server" class="form-control" TextMode="MultiLine" Height="75px"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Goods</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <asp:RadioButtonList ID="rbl_goods_type" runat="server" 
                                                CssClass="radio-inline">
                                                <asp:ListItem Text="Non IT" Value="Non IT"/>
                                                <asp:ListItem Text="IT" Value="IT"/>
                                            </asp:RadioButtonList>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Currency</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <asp:DropDownList ID="ddl_curr" runat="server" class="form-control"
                                            DataSourceID="sql_currency" DataTextField="code" 
                                            DataValueField="code" ResolvedRenderMode="Classic" AppendDataBoundItems="True">
                                                <asp:ListItem Value="-1" Selected="True">-- Select currency -- </asp:ListItem>
                                            </asp:DropDownList> 
                                            <asp:SqlDataSource ID="sql_currency" runat="server" 
                                                ConnectionString="<%$ ConnectionStrings:conn_str %>" 
                                                ProviderName="<%$ ConnectionStrings:conn_str.ProviderName %>" 
                                                SelectCommand="SELECT cat_id, code, description FROM code_list WHERE (cat_id ='CUR') and active = 1 order by seq_no ASC">
                                            </asp:SqlDataSource>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="panel-heading">
                            <label>Item Data</label>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <telerik:RadGrid ID="rg_temp" runat="server" DataSourceID="sql_temp" 
                                        ShowFooter="True" AllowAutomaticDeletes="True" 
                                        CssClass="table table-striped table-bordered table-hover" >
                                        <MasterTableView AutoGenerateColumns="False" DataKeyNames="po_item_id" 
                                            DataSourceID="sql_temp">
                                            <Columns>
                                                <telerik:GridBoundColumn DataField="po_item_no" 
                                                    FilterControlAltText="Filter po_item_no column" HeaderText="No" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                                    SortExpression="po_item_no" UniqueName="po_item_no">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="catalog_id" 
                                                    FilterControlAltText="Filter catalog_id column" HeaderText="Catalog ID" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left"
                                                    SortExpression="catalog_id" UniqueName="catalog_id">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="item_name" 
                                                    FilterControlAltText="Filter item_name column" HeaderText="Item Name" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left"
                                                    SortExpression="item_name" UniqueName="item_name">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="merk" 
                                                    FilterControlAltText="Filter merk column" HeaderText="Merk" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left"
                                                    SortExpression="merk" UniqueName="merk">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="quantity" DataType="System.Int32" 
                                                    FilterControlAltText="Filter quantity column" HeaderText="Quantity" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" 
                                                    SortExpression="quantity" UniqueName="quantity">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="uop" 
                                                    FilterControlAltText="Filter unit column" HeaderText="UOP" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" 
                                                    SortExpression="uop" UniqueName="uop">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="uoi" 
                                                    FilterControlAltText="Filter uoi column" HeaderText="UOI" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" 
                                                    SortExpression="uoi" UniqueName="uoi">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="conv_factor" 
                                                    FilterControlAltText="Filter conv_factor column" HeaderText="Conv factor" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" 
                                                    SortExpression="conv_factor" UniqueName="conv_factor">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="price" DataType="System.Decimal" FilterControlAltText="Filter price column" HeaderText="Unit Price" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" SortExpression="price" UniqueName="price">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="subtotal" DataType="System.Decimal" Aggregate="Sum" FooterAggregateFormatString="{0:###,##0.#0}" FooterStyle-HorizontalAlign="Right"
                                                    FilterControlAltText="Filter subtotal column" HeaderText="Total Price" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,##0.#0}" HeaderStyle-HorizontalAlign="Center" 
                                                    SortExpression="subtotal" UniqueName="subtotal">
                                                </telerik:GridBoundColumn>
                                                <%--<telerik:GridButtonColumn ButtonType="LinkButton" CommandName="choose" UniqueName="Edit" Text="Edit">
                                                    <HeaderStyle HorizontalAlign="Center" /> 
                                                    <ItemStyle HorizontalAlign="Center" /> 
                                                </telerik:GridButtonColumn>--%>
                                                <telerik:GridButtonColumn Text="Delete" CommandName="delete" ButtonType="LinkButton" ConfirmText="Are you sure you want to delete this record?" ConfirmDialogType="RadWindow" UniqueName="delete">
                                                    <HeaderStyle HorizontalAlign="Center" /> 
                                                    <ItemStyle HorizontalAlign="Center" /> 
                                                </telerik:GridButtonColumn>
                                                <telerik:GridBoundColumn DataField="flag" FilterControlAltText="Filter flag column" HeaderText="U/N" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" SortExpression="flag" UniqueName="flag">
                                                </telerik:GridBoundColumn>
                                            </Columns>
                                        </MasterTableView>
                                        <ClientSettings Selecting-AllowRowSelect="true" EnablePostBackOnRowClick="true">
                                            <Selecting AllowRowSelect="True" />
                                        </ClientSettings>
                                    </telerik:RadGrid>
                                    <asp:SqlDataSource ID="sql_temp" runat="server" 
                                        ConnectionString="<%$ ConnectionStrings:conn_str %>" 
                                        ProviderName="<%$ ConnectionStrings:conn_str.ProviderName %>" 
                                        SelectCommand="SELECT * FROM temp_po"
                                        DeleteCommand="DELETE FROM temp_po WHERE (po_item_id = @po_item_id)" ></asp:SqlDataSource>
                                </div>
                                <div class="col-lg-6">                             
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Item No</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <asp:TextBox ID="txt_item_no" runat="server" class="form-control" ReadOnly="true"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Item</label>
                                        </div>
                                    </div> 
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <asp:DropDownList ID="ddl_catalog" runat="server" class="form-control"
                                            DataSourceID="sql_catalog" DataTextField="DescX" AutoPostBack="true"
                                            DataValueField="catalog_id" ResolvedRenderMode="Classic" AppendDataBoundItems="True">
                                                <asp:ListItem Value="-1" Selected="True">-- Select item -- </asp:ListItem>
                                                <asp:ListItem Value="0">Non Catalog</asp:ListItem>
                                            </asp:DropDownList> 
                                            <asp:SqlDataSource ID="sql_catalog" runat="server" 
                                                ConnectionString="<%$ ConnectionStrings:conn_str %>" 
                                                ProviderName="<%$ ConnectionStrings:conn_str.ProviderName %>" 
                                                SelectCommand="SELECT catalog_id, item_name, concat(IF(ISNULL(merk),'', CONCAT('[', merk, '] - ')), item_name) as DescX FROM catalog WHERE active = 1 order by concat(merk, ' - ', item_name) ASC">
                                            </asp:SqlDataSource>
                                        </div>
                                    </div>
                                        
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Item Name</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <asp:TextBox ID="txt_item_name" runat="server" class="form-control"  TextMode="MultiLine" Height="75px"></asp:TextBox>
                                        </div>
                                    </div>                                    

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Merk</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <asp:DropDownList ID="ddl_merk" runat="server" class="form-control"
                                            DataSourceID="sql_merk" DataTextField="description" 
                                            DataValueField="code" ResolvedRenderMode="Classic" AppendDataBoundItems="True">
                                                <asp:ListItem Value="-1" Selected="True">-- Select merk -- </asp:ListItem>
                                            </asp:DropDownList> 
                                            <asp:SqlDataSource ID="sql_merk" runat="server" 
                                                ConnectionString="<%$ ConnectionStrings:conn_str %>" 
                                                ProviderName="<%$ ConnectionStrings:conn_str.ProviderName %>" 
                                                SelectCommand="SELECT cat_id, code, description FROM code_list WHERE (cat_id ='MRK') and active = 1 order by code ASC">
                                            </asp:SqlDataSource>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Quantity</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <asp:TextBox ID="txt_qty" runat="server" class="form-control"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
                                            ErrorMessage="Only Numbers allowed" ValidationExpression="^\d+(\.\d{1,2})?$" 
                                                ControlToValidate="txt_qty" ForeColor="Red"/>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Unit Price</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <asp:TextBox ID="txt_unit_price" runat="server" class="form-control"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" 
                                            ErrorMessage="Only Numbers allowed" ValidationExpression="^\d+(\.\d{1,2})?$" 
                                                ControlToValidate="txt_unit_price" ForeColor="Red"/>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Unit of Issue (UOI)</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <asp:DropDownList ID="ddl_uoi" runat="server" class="form-control" 
                                            DataSourceID="sql_uom" DataTextField="Description" 
                                            DataValueField="Code" ResolvedRenderMode="Classic" AppendDataBoundItems="True">
                                                <asp:ListItem Value="-1" Selected="True">-- Select UOI -- </asp:ListItem>
                                            </asp:DropDownList> 
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Conversion Factor</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <asp:TextBox ID="txt_conv_factor" runat="server" class="form-control"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" 
                                            ErrorMessage="Only Numbers allowed" ValidationExpression="^\d+(\.\d{1,2})?$" 
                                                ControlToValidate="txt_conv_factor" ForeColor="Red"/>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Unit of Purchase (UOP)</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <asp:DropDownList ID="ddl_uop" runat="server" class="form-control" 
                                            DataSourceID="sql_uom" DataTextField="Description" 
                                            DataValueField="Code" ResolvedRenderMode="Classic" AppendDataBoundItems="True">
                                                <asp:ListItem Value="-1" Selected="True">-- Select UOP -- </asp:ListItem>
                                            </asp:DropDownList> 
                                            <asp:SqlDataSource ID="sql_uom" runat="server" 
                                                ConnectionString="<%$ ConnectionStrings:conn_str %>" 
                                                ProviderName="<%$ ConnectionStrings:conn_str.ProviderName %>" 
                                                SelectCommand="SELECT cat_id, code, description FROM code_list WHERE (cat_id ='UOM') and active = 1 order by code ASC">
                                            </asp:SqlDataSource>
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <asp:Button ID="btn_update" runat="server" Text="Update" class="btn btn-primary btn-sm" Visible="false"/>
                                            <asp:Button ID="btn_addItems" runat="server" Text="Add Item" class="btn btn-primary btn-sm"/>  
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="panel-heading">
                            <label>Total</label>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Discount</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <asp:TextBox ID="txt_discount" runat="server" class="form-control"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" 
                                            ErrorMessage="Only Numbers allowed" ValidationExpression="^\d+(\.\d{1,2})?$" 
                                                ControlToValidate="txt_discount" ForeColor="Red"/>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>PPN</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <asp:TextBox ID="txt_ppn" runat="server" class="form-control"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" 
                                            ErrorMessage="Only Numbers allowed" ValidationExpression="^\d+(\.\d{1,2})?$" 
                                                ControlToValidate="txt_ppn" ForeColor="Red"/>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Shipping Fee</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <asp:TextBox ID="txt_shipping" runat="server" class="form-control"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" 
                                            ErrorMessage="Only Numbers allowed" ValidationExpression="^\d+(\.\d{1,2})?$" 
                                                ControlToValidate="txt_shipping" ForeColor="Red"/>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <asp:Button ID="btn_calculate" runat="server" Text="Calculate" class="btn btn-primary btn-sm"/> 
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Total PO</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <asp:TextBox ID="txt_total_po" runat="server" class="form-control" ReadOnly="true"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="panel-footer">
                            <div class="form-group">
                                <asp:Button ID="btn_submit_po" runat="server" Text="Submit" class="btn btn-primary"/> 
                                <asp:Button ID="btn_modify_po" runat="server" Text="Modify" class="btn btn-primary"/> 
                                <asp:Button ID="btn_clear" runat="server" Text="Clear" class="btn btn-primary"/> 
                            </div>
                            <div class="form-group">
                                <asp:Label ID="lbl_msg" runat="server" Text="" class="text-warning"></asp:Label>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </ContentTemplate>
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="ddl_supplier" EventName="SelectedIndexChanged" />
        <asp:AsyncPostBackTrigger ControlID="ddl_catalog" EventName="SelectedIndexChanged" />
        <asp:AsyncPostBackTrigger ControlID="btn_addItems" EventName="Click" />
        <asp:AsyncPostBackTrigger ControlID="btn_update" EventName="Click" />
        <asp:AsyncPostBackTrigger ControlID="btn_calculate" EventName="Click" />
        <asp:AsyncPostBackTrigger ControlID="btn_submit_po" EventName="Click" />
        <asp:AsyncPostBackTrigger ControlID="btn_modify_po" EventName="Click" />
        <asp:AsyncPostBackTrigger ControlID="btn_clear" EventName="Click" />
        <asp:AsyncPostBackTrigger ControlID="rg_temp" EventName="ItemCommand" />
    </Triggers>
</asp:UpdatePanel>
</asp:Content>
