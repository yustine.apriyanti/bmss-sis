﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="PurchaseOrderModify.aspx.vb" Inherits="BMSS.PurchaseOrderModify" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div>
        <p align="center"><b>PURCHASE ORDER MODIFY</b></p>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <fieldset>
                    <legend>PO Modify</legend>
                        <table>
                            <tr>
                                <td class="td_width1">PO NO</td>
                                <td class="td_width2">:</td>
                                <td>
                                    <telerik:RadTextBox ID="rt_poNO" runat="server" Width="175px">
                                    </telerik:RadTextBox>&nbsp;
                                    <telerik:RadButton ID="rb_modify" runat="server" Text="Modify">
                                    </telerik:RadButton>&nbsp;
                                    <%--<telerik:RadButton ID="rb_cancelPO" runat="server" Text="Cancel PO" Visible="false">
                                    </telerik:RadButton>&nbsp;--%>
                                    <asp:Label ID="lbl_msg" runat="server" Text="" CssClass="error_msg"></asp:Label>
                                </td>
                            </tr>
                        </table>
                </fieldset>
                <fieldset>
                    <legend>Supplier Data :</legend>
                        <table>
                            <tr>
                                <td class="td_width1">Company Name</td>
                                <td class="td_width2">:</td>
                                <td>
                                    <telerik:RadComboBox ID="rc_supplier" runat="server" Width="250px" EmptyMessage="Select" 
                                        DataSourceID="sql_supplier" DataTextField="CompanyName" 
                                        DataValueField="SupplierID" ResolvedRenderMode="Classic" Enabled="false">
                                    </telerik:RadComboBox> 
                                    <asp:SqlDataSource ID="sql_supplier" runat="server" 
                                        ConnectionString="<%$ ConnectionStrings:conn_str %>" 
                                        ProviderName="<%$ ConnectionStrings:conn_str.ProviderName %>" 
                                        SelectCommand="SELECT * from suppliers order by CompanyName asc">
                                    </asp:SqlDataSource>                    
                                </td>
                            </tr>
                            <tr>
                                <td>Address</td>
                                <td>:</td>
                                <td>
                                    <telerik:RadTextBox ID="rt_address" runat="server" ReadOnly="true" TextMode="MultiLine" Height="50px" Width="400px">
                                    </telerik:RadTextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>Phone</td>
                                <td>:</td>
                                <td>
                                    <telerik:RadTextBox ID="rt_phone" runat="server"  ReadOnly="true" Width="175px">
                                    </telerik:RadTextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>Fax</td>
                                <td>:</td>
                                <td>
                                    <telerik:RadTextBox ID="rt_fax" runat="server"  ReadOnly="true" Width="175px">
                                    </telerik:RadTextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>Contact Person</td>
                                <td>:</td>
                                <td>
                                    <telerik:RadTextBox ID="rt_contactPerson" runat="server"  ReadOnly="true" Width="175px">
                                    </telerik:RadTextBox>
                                </td>
                            </tr>
                        </table>
                </fieldset>
                <fieldset>
                    <legend>Purchase Order Data :</legend>
                        <table>
                            <%--<tr>
                                <td class="td_width1">PO No</td>
                                <td class="td_width2">:</td>
                                <td>
                                    <telerik:RadTextBox ID="rt_poNo" runat="server" Width="175px" ReadOnly="true">
                                    </telerik:RadTextBox>
                                </td>
                            </tr>--%>
                            <tr>
                                <td class="td_width1">PO Date</td>
                                <td class="td_width2">:</td>
                                <td>
                                    <telerik:RadDatePicker ID="rd_poDate" runat="server" Culture="en-US" Width="175px">
                                        <Calendar ID="Calendar1" UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" ViewSelectorText="x" runat="server"></Calendar>
                                        <DateInput ID="DateInput1" DisplayDateFormat="yyyy-MM-dd" DateFormat="yyyy-MM-dd" runat="server"></DateInput>
                                        <DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>
                                    </telerik:RadDatePicker>
                                </td>
                            </tr>
                            <tr>
                                <td>PR No</td>
                                <td>:</td>
                                <td>
                                    <telerik:RadTextBox ID="rt_prNo" runat="server" Width="175px">
                                    </telerik:RadTextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>PR Date</td>
                                <td>:</td>
                                <td>
                                    <telerik:RadDatePicker ID="rd_prDate" runat="server" Culture="en-US" Width="175px">
                                        <Calendar ID="Calendar2" UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" ViewSelectorText="x" runat="server"></Calendar>
                                        <DateInput ID="DateInput2" DisplayDateFormat="yyyy-MM-dd" DateFormat="yyyy-MM-dd" runat="server"></DateInput>
                                        <DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>
                                    </telerik:RadDatePicker>                                
                                </td>
                            </tr>
                            <tr>
                                <td>Delivery Date (est)</td>
                                <td>:</td>
                                <td>
                                    <telerik:RadDatePicker ID="rd_deliveryDate" runat="server" Culture="en-US" Width="175px">
                                        <Calendar ID="Calendar3" UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" ViewSelectorText="x" runat="server"></Calendar>
                                        <DateInput ID="DateInput3" DisplayDateFormat="yyyy-MM-dd" DateFormat="yyyy-MM-dd" runat="server"></DateInput>
                                        <DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>
                                    </telerik:RadDatePicker>
                                </td>
                            </tr>
                            <tr>
                                <td>Payment Terms</td>
                                <td>:</td>
                                <td>
                                    <telerik:RadTextBox ID="rt_paymentTerms" runat="server" Width="175px">
                                    </telerik:RadTextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>Delivery Location</td>
                                <td>:</td>
                                <td>
                                    <telerik:RadTextBox ID="rt_deliveryLocation" runat="server" TextMode="MultiLine" Height="50px" Width="400px">
                                    </telerik:RadTextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>Notes</td>
                                <td>:</td>
                                <td>
                                    <telerik:RadTextBox ID="rt_notes" runat="server" TextMode="MultiLine" Height="50px" Width="400px">
                                    </telerik:RadTextBox>                                
                                </td>
                            </tr>
                            <tr>
                                <td>Goods</td>
                                <td>:</td>
                                <td> 
                                    <asp:RadioButtonList ID="rbl_goods" runat="server" RepeatDirection="Horizontal">
                                        <asp:ListItem Value="NONIT" Text="NON IT Goods" Selected="True"></asp:ListItem>
                                        <asp:ListItem Value="IT" Text="IT Goods"></asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>
                            </tr>
                            <tr>
                                <td>Currency</td>
                                <td>:</td>
                                <td>
                                    <telerik:RadComboBox ID="rc_currency" runat="server"  Width="175px" EmptyMessage="Select" DropDownAutoWidth="Enabled" 
                                        DataSourceID="sql_currency" DataTextField="id_currency" 
                                        DataValueField="id_currency">
                                    </telerik:RadComboBox>
                                    <asp:SqlDataSource ID="sql_currency" runat="server" 
                                        ConnectionString="<%$ ConnectionStrings:conn_str %>" 
                                        ProviderName="<%$ ConnectionStrings:conn_str.ProviderName %>" 
                                        SelectCommand="select * from cf_currency"></asp:SqlDataSource>
                                </td>
                            </tr>
                        </table>
                </fieldset>
                <fieldset>
                    <legend>Items Data</legend>
                    <table>
                        <tr>
                            <td colspan="3">
                                <telerik:RadGrid ID="rg_temp" runat="server" ShowFooter="True" 
                                    AllowAutomaticDeletes="True" DataSourceID="sql_temp" >
                                    <MasterTableView AutoGenerateColumns="False" DataKeyNames="po_item_id" 
                                        DataSourceID="sql_temp">
                                        <Columns>
                                            <telerik:GridBoundColumn DataField="po_item_no" 
                                                FilterControlAltText="Filter po_item_no column" HeaderText="No" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                                SortExpression="po_item_no" UniqueName="po_item_no">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="part_desc" 
                                                FilterControlAltText="Filter part_desc column" HeaderText="Description" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left"
                                                SortExpression="part_desc" UniqueName="part_desc">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Left" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="quantity" DataType="System.Int32" 
                                                FilterControlAltText="Filter quantity column" HeaderText="Quantity" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" 
                                                SortExpression="quantity" UniqueName="quantity">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="unit" 
                                                FilterControlAltText="Filter unit column" HeaderText="Unit" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" 
                                                SortExpression="unit" UniqueName="unit">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="price" DataType="System.Decimal" 
                                                FilterControlAltText="Filter price column" HeaderText="Unit Price" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,##0.#0}" 
                                                SortExpression="price" UniqueName="price">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Right" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="subtotal" DataType="System.Decimal" Aggregate="Sum" FooterAggregateFormatString="{0:###,##0.#0}" FooterStyle-HorizontalAlign="Right"
                                                FilterControlAltText="Filter subtotal column" HeaderText="Total Price" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:###,##0.#0}" HeaderStyle-HorizontalAlign="Center" 
                                                SortExpression="subtotal" UniqueName="subtotal">
                                                <FooterStyle HorizontalAlign="Right" />
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Right" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridButtonColumn Text="Delete" CommandName="Delete" ButtonType="ImageButton" ConfirmText="Delete this data?" ConfirmDialogType="RadWindow" />
                                        </Columns>
                                    </MasterTableView>
                                    <ClientSettings Selecting-AllowRowSelect="true" EnablePostBackOnRowClick="true">
                                        <Selecting AllowRowSelect="True" />
                                    </ClientSettings>
                                </telerik:RadGrid>
                                <asp:SqlDataSource ID="sql_temp" runat="server" 
                                    ConnectionString="<%$ ConnectionStrings:conn_str %>" 
                                    ProviderName="<%$ ConnectionStrings:conn_str.ProviderName %>" 
                                    SelectCommand="SELECT * FROM purchase_order_item where po_no = @po_no"
                                    DeleteCommand="DELETE FROM purchase_order_item WHERE (po_item_id = @po_item_id)" >
                                    <SelectParameters>
                                        <asp:ControlParameter name="po_no" Type="String" ControlID="rt_poNo"/>
                                    </SelectParameters>
                                </asp:SqlDataSource>
                            </td>
                        </tr>
                        <tr>
                            <td class="td_width1">Item No</td>
                            <td class="td_width2">:</td>
                            <td>
                                <telerik:RadTextBox ID="rt_itemNo" runat="server" Width="175px" ReadOnly="true">
                                </telerik:RadTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>Description</td>
                            <td>:</td>
                            <td>                            
                                <telerik:RadTextBox ID="rt_description" runat="server" TextMode="MultiLine" Height="50px" Width="400px">
                                </telerik:RadTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>Quantity</td>
                            <td>:</td>
                            <td>
                                <telerik:RadNumericTextBox ID="rn_quantity" runat="server" Width="175px">
                                </telerik:RadNumericTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>Unit</td>
                            <td>:</td>
                            <td>
                                <telerik:RadComboBox ID="rc_UOM" runat="server" Width="175px"
                                AutoPostBack="True" EmptyMessage="Select UOM" DropDownAutoWidth="Enabled" 
                                    DataSourceID="sql_uom" DataTextField="Description" DataValueField="Code">
                                </telerik:RadComboBox>&nbsp;
                                <asp:SqlDataSource ID="sql_uom" runat="server" 
                                    ConnectionString="<%$ ConnectionStrings:conn_str %>" 
                                    ProviderName="<%$ ConnectionStrings:conn_str.ProviderName %>" 
                                    SelectCommand="SELECT CatID, Code, Description FROM code_list WHERE (CatID ='UM')"></asp:SqlDataSource>
                            </td>
                        </tr>
                        <tr>
                            <td>Unit Price</td>
                            <td>:</td>
                            <td>                            
                                <telerik:RadNumericTextBox ID="rn_unitPrice" runat="server" Width="175px">
                                </telerik:RadNumericTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <telerik:RadButton ID="rb_update" runat="server" Text="Update" Visible="false">
                                </telerik:RadButton>&nbsp;
                                <telerik:RadButton ID="rb_addItems" runat="server" Text="Add Items">
                                </telerik:RadButton>
                            </td>
                        </tr>
                    </table>
                </fieldset>
                <fieldset>
                    <legend>Total :</legend>
                    <table>
                        <%--<tr>
                            <td class="td_width1">Subtotal</td>
                            <td class="td_width2">:</td>
                            <td>
                                <telerik:RadNumericTextBox ID="rn_subtotal" runat="server" Width="175px" Enabled="false">
                                </telerik:RadNumericTextBox>
                            </td>
                        </tr>--%>
                        <tr>
                            <td>Discount</td>
                            <td>:</td>
                            <td>
                                <telerik:RadNumericTextBox ID="rn_discount" runat="server" Width="175px">
                                </telerik:RadNumericTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>PPN</td>
                            <td>:</td>
                            <td>
                                <telerik:RadNumericTextBox ID="rn_ppn" runat="server" Width="175px">
                                </telerik:RadNumericTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>Shipping Fee</td>
                            <td>:</td>
                            <td>
                                <telerik:RadNumericTextBox ID="rn_shipping" runat="server" Width="175px">
                                </telerik:RadNumericTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>Total</td>
                            <td>:</td>
                            <td>
                                <telerik:RadNumericTextBox ID="rn_total" runat="server" Width="175px" Enabled="false">
                                </telerik:RadNumericTextBox>&nbsp;&nbsp;
                                <telerik:RadButton ID="rb_calculate" runat="server" Text="Calculate">
                                </telerik:RadButton>
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="rc_supplier" EventName="SelectedIndexChanged" />
                <asp:AsyncPostBackTrigger ControlID="rb_addItems" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="rb_clearForm" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="rb_calculate" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="rb_modify" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>
        <telerik:RadButton ID="rb_submit" runat="server" Text="Submit (Modify PO)">
        </telerik:RadButton>&nbsp;
        <telerik:RadButton ID="rb_clearForm" runat="server" Text="Clear Form">
        </telerik:RadButton>
    </div>
</asp:Content>
