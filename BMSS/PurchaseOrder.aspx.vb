﻿Imports MySql.Data
Imports MySql.Data.MySqlClient
Imports Telerik.Web.UI

Public Class PurchaseOrder
    Inherits BMSS.BaseClass

    Dim oConn As New MySqlConnection(ConfigurationManager.ConnectionStrings("conn_str").ConnectionString)
    Dim create_by As String = "ADMIN"
    Dim str_type As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("login") Is Nothing Then
            Response.Redirect("LoginForm.aspx")
        Else
            If Not IsPostBack Then
                str_type = Request.QueryString("type")
                If str_type = "mod" Then
                    lbl_header.Text = "Purchase Order - Modify"
                    panel_po_search.Visible = True
                    btn_submit_po.Visible = False
                    btn_modify_po.Visible = True
                    rg_temp.MasterTableView.GetColumn("flag").Display = True
                Else
                    str_type = "new"
                    lbl_header.Text = "Purchase Order - New"
                    txt_po_no.Text = GenerateNo()
                    ddl_supplier.Focus()
                    txt_item_no.Text = GetItemNo()
                    panel_po_search.Visible = False
                    btn_submit_po.Visible = True
                    btn_modify_po.Visible = False
                    rg_temp.MasterTableView.GetColumn("flag").Display = False
                End If
                rd_poDate.SelectedDate = Date.Now()
                rd_prDate.SelectedDate = Date.Now()
                rd_deliveryDate.SelectedDate = Date.Now()
                txt_discount.Text = 0
                txt_ppn.Text = 0
                txt_shipping.Text = 0
                txt_total_po.Text = 0
            End If
        End If
    End Sub

    Function GenerateNo() As String
        Dim kode As Integer
        Dim no As String
        oConn.Open()
        Dim sqlCmd2 As New MySqlCommand("select * from purchase_order", oConn)
        Dim oDatareader As MySqlDataReader
        oDatareader = sqlCmd2.ExecuteReader()
        If oDatareader.HasRows() Then
            oDatareader.Close()
            Dim sqlCmd As New MySqlCommand("SELECT MAX(RIGHT(PO_NO,5)) + 1 FROM purchase_order LIMIT 1", oConn)
            kode = sqlCmd.ExecuteScalar().ToString()
            oConn.Close()
            If kode.ToString() = "" Then
                no = 1
            Else
                no = Convert.ToInt16(kode.ToString())
            End If
        Else
            no = 1
            oConn.Close()
        End If
        Return "P" & Right("00000" + no.ToString(), 5)
    End Function

    Function GetItemNo() As String
        oConn.Open()
        Dim kode As Integer
        Dim no As String = ""
        Dim sqlCmd2 As New MySqlCommand("select * from temp_po", oConn)
        Dim oDatareader As MySqlDataReader
        oDatareader = sqlCmd2.ExecuteReader()
        If oDatareader.HasRows() Then
            oDatareader.Close()
            Dim sqlCmd As New MySqlCommand("SELECT MAX(po_item_no)+1 from temp_po LIMIT 1", oConn)
            kode = sqlCmd.ExecuteScalar().ToString()
            If kode.ToString() = "" Then
                no = 1
            Else
                no = Convert.ToInt16(kode.ToString()).ToString()
            End If
        Else
            oDatareader.Close()
            no = 1
        End If
        oConn.Close()
        Return no
    End Function

    Sub ClearItem()
        ddl_catalog.ClearSelection()
        txt_item_name.Text = ""
        ddl_merk.ClearSelection()
        txt_qty.Text = ""
        txt_unit_price.Text = ""
        ddl_uoi.ClearSelection()
        txt_conv_factor.Text = ""
        ddl_uop.ClearSelection()
        txt_item_no.Text = GetItemNo()
        rg_temp.Rebind()
    End Sub

    Sub DropDataTemp()
        oConn.Open()
        Dim sqlCmd As New MySqlCommand("delete from temp_po", oConn)
        sqlCmd.ExecuteNonQuery()
        oConn.Close()
    End Sub

    Protected Sub ddl_supplier_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddl_supplier.SelectedIndexChanged
        Dim sqlCmd As New MySqlCommand("select * from supplier where supplier_id = " & ddl_supplier.SelectedValue.ToString() & " ", oConn)
        oConn.Open()
        Dim oDatareader As MySqlDataReader
        oDatareader = sqlCmd.ExecuteReader()
        If oDatareader.HasRows() Then
            If oDatareader.Read() Then
                txt_address.Text = oDatareader("address").ToString()
                txt_phone.Text = oDatareader("phone").ToString()
                txt_fax.Text = oDatareader("fax").ToString()
                txt_contactPerson.Text = oDatareader("contact_name").ToString()
            End If
        End If
        oDatareader.Close()
        oConn.Close()
    End Sub

    Protected Sub ddl_catalog_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddl_catalog.SelectedIndexChanged
        If ddl_catalog.SelectedValue.ToString() = "0" Then
            txt_item_name.ReadOnly = False
            txt_item_name.Text = ""
            ddl_merk.Enabled = True
            ddl_merk.ClearSelection()
            ddl_uoi.Enabled = True
            ddl_uoi.ClearSelection()
            ddl_uop.Enabled = True
            ddl_uop.ClearSelection()
        Else
            txt_item_name.ReadOnly = True
            ddl_merk.Enabled = False
            ddl_merk.CssClass = "form-control"
            ddl_uoi.Enabled = False
            ddl_uoi.CssClass = "form-control"

            Dim sqlCmd As New MySqlCommand("select * from catalog where catalog_id = '" & ddl_catalog.SelectedValue.ToString() & "' ", oConn)
            oConn.Open()
            Dim oDatareader As MySqlDataReader
            oDatareader = sqlCmd.ExecuteReader()
            If oDatareader.HasRows() Then
                If oDatareader.Read() Then
                    txt_item_name.Text = oDatareader("item_name").ToString()
                    ddl_uoi.SelectedValue = oDatareader("uoi").ToString()
                    ddl_uop.SelectedValue = oDatareader("uop").ToString()
                    ddl_merk.SelectedValue = oDatareader("merk").ToString()
                    txt_conv_factor.Text = oDatareader("conv_factor").ToString()
                End If
            End If
            oDatareader.Close()
            oConn.Close()
        End If
    End Sub

    Protected Sub rg_temp_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles rg_temp.SelectedIndexChanged
        btn_update.Visible = True
        btn_addItems.Text = "Cancel"
        Dim item As GridDataItem = DirectCast(rg_temp.SelectedItems(0), GridDataItem)

        If item.Item("catalog_id").Text = "" Or item.Item("catalog_id").Text = "&nbsp;" Then
            ddl_catalog.SelectedValue = 0
            txt_item_name.ReadOnly = False
            ddl_merk.Enabled = True
            ddl_merk.ClearSelection()
            ddl_uoi.Enabled = True
            ddl_uoi.ClearSelection()
            ddl_uop.Enabled = True
            ddl_uop.ClearSelection()
        Else
            ddl_catalog.SelectedValue = item.Item("catalog_id").Text
            txt_item_name.ReadOnly = True
            ddl_merk.Enabled = False
            ddl_merk.CssClass = "form-control"
            ddl_uoi.Enabled = False
            ddl_uoi.CssClass = "form-control"
        End If

        txt_item_no.Text = item.Item("po_item_no").Text
        txt_item_name.Text = item.Item("item_name").Text
        ddl_merk.SelectedValue = item.Item("merk").Text
        txt_qty.Text = item.Item("quantity").Text
        txt_unit_price.Text = item.Item("price").Text
        ddl_uoi.SelectedValue = item.Item("uoi").Text
        txt_conv_factor.Text = item.Item("conv_factor").Text
        ddl_uop.SelectedValue = item.Item("uop").Text
    End Sub

    Protected Sub rg_temp_ItemDeleted(ByVal source As Object, ByVal e As Telerik.Web.UI.GridDeletedEventArgs) Handles rg_temp.ItemDeleted
        If Not e.Exception Is Nothing Then
            e.ExceptionHandled = True
        Else
            txt_item_no.Text = GetItemNo()
        End If
    End Sub

    Protected Sub btn_addItems_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_addItems.Click
        If btn_addItems.Text = "Cancel" Then
            btn_addItems.Text = "Add Item"
            btn_update.Visible = False
            txt_item_no.Text = GetItemNo()
            ClearItem()
        Else
            oConn.Open()
            Dim catalog_id As String = ""
            If ddl_catalog.SelectedValue = "0" Then
                catalog_id = ""
            Else
                catalog_id = ddl_catalog.SelectedValue.ToString()
            End If

            Dim subtotal As Integer = Integer.Parse(txt_qty.Text) * Integer.Parse(txt_unit_price.Text)
            Dim sqlCmd As New MySqlCommand("Insert into temp_po (po_no, po_item_no, catalog_id, item_name, merk, quantity, uoi, uop, conv_factor, price, subtotal, flag) " & _
                                           "values ('" & txt_po_no.Text & "', '" & txt_item_no.Text & "', '" & catalog_id & "', UPPER('" & txt_item_name.Text & "'), '" & ddl_merk.SelectedValue.ToString() & "', " & Integer.Parse(txt_qty.Text) & ", '" & ddl_uoi.SelectedValue.ToString() & "', '" & ddl_uop.SelectedValue.ToString() & "', " & Integer.Parse(txt_conv_factor.Text) & ", " & Integer.Parse(txt_unit_price.Text) & ", " & subtotal & ", 'N') ", oConn)
            sqlCmd.ExecuteNonQuery()
            oConn.Close()
            rg_temp.Rebind()
            ClearItem()
            txt_item_no.Text = GetItemNo()

            txt_item_name.ReadOnly = False
            txt_item_name.Text = ""
            ddl_merk.Enabled = True
            ddl_merk.ClearSelection()
            ddl_uoi.Enabled = True
            ddl_uoi.ClearSelection()
            ddl_uop.Enabled = True
            ddl_uop.ClearSelection()
            
            txt_total_po.Text = GetTotal()
        End If
    End Sub

    Protected Sub btn_update_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_update.Click
        oConn.Open()
        Dim catalog_id As String = ""
        If ddl_catalog.SelectedValue = "0" Then
            catalog_id = ""
        Else
            catalog_id = ddl_catalog.SelectedValue.ToString()
        End If
        Dim sqlCmd As New MySqlCommand("update temp_po set catalog_id = '" & catalog_id & "',item_name = '" & txt_item_name.Text & "', merk = '" & ddl_merk.SelectedValue.ToString() & "', quantity = " & Integer.Parse(txt_qty.Text) & ", uop = '" & ddl_uop.SelectedValue.ToString() & "', uoi = '" & ddl_uoi.SelectedValue.ToString() & "', conv_factor = " & Integer.Parse(txt_conv_factor.Text) & ", price = " & Integer.Parse(txt_unit_price.Text) & ", subtotal = " & Integer.Parse(txt_qty.Text) * Integer.Parse(txt_unit_price.Text) & ", flag = 'U' where po_item_no = '" & txt_item_no.Text & "'", oConn)
        sqlCmd.ExecuteNonQuery()
        oConn.Close()
        rg_temp.Rebind()
        ClearItem()
        btn_update.Visible = False
        btn_addItems.Text = "Add Item"
        txt_item_no.Text = GetItemNo()

        txt_total_po.Text = GetTotal()
    End Sub

    Function GetTotal() As Integer
        Dim subtotal As Integer = 0
        Dim total As Integer = 0
        oConn.Open()
        Dim oDatareader As MySqlDataReader
        Dim sqlCmd As New MySqlCommand("select ifnull(sum(subtotal),0) as subtotal from temp_po", oConn)
        oDatareader = sqlCmd.ExecuteReader()
        If oDatareader.HasRows() Then
            If oDatareader.Read() Then
                subtotal = Integer.Parse(oDatareader("subtotal").ToString())
            End If
        Else
            subtotal = 0
        End If
        oConn.Close()
        total = subtotal - Integer.Parse(txt_discount.Text) + Integer.Parse(txt_ppn.Text) + Integer.Parse(txt_shipping.Text)
        Return total
    End Function

    Protected Sub btn_calculate_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_calculate.Click
        txt_total_po.Text = GetTotal()
    End Sub

    Protected Sub btn_submit_po_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_submit_po.Click
        Dim subtotal As Integer = 0
        Dim create_date As String = Date.Now.ToString("yyyy-MM-dd HH:mm:ss")
        Dim oDatareader As MySqlDataReader

        Try
            oConn.Open()
            Dim sqlCmd1 As New MySqlCommand("select ifnull(sum(subtotal),0) as subtotal from temp_po", oConn)
            oDatareader = sqlCmd1.ExecuteReader()
            If oDatareader.HasRows() Then
                If oDatareader.Read() Then
                    subtotal = Integer.Parse(oDatareader("subtotal").ToString())
                End If
            Else
                subtotal = 0
            End If
            oDatareader.Close()

            Dim sqlCmd_check As New MySqlCommand("select * from temp_po ", oConn)
            oDatareader = sqlCmd_check.ExecuteReader()
            If oDatareader.HasRows() Then
                oDatareader.Close()
                Dim sqlCmd As New MySqlCommand("Insert into purchase_order (supplier_id, supplier_name, company_name, po_no, pr_no, pr_date, create_by, create_date, delivery_date, delivery_location, po_remark, po_date, subtotal, discount, ppn, transportation, total, payment_term, currency, goods)" & _
                                        "values ('" & ddl_supplier.SelectedValue.ToString() & "', '" & ddl_supplier.SelectedItem.ToString() & "', '" & ddl_company.SelectedValue.ToString() & "', '" & txt_po_no.Text & "', UPPER('" & txt_pr_no.Text & "'), '" & Format(rd_prDate.SelectedDate, "yyyy-MM-dd") & "', UPPER('" & create_by & "'), '" & create_date & "', '" & Format(rd_deliveryDate.SelectedDate, "yyyy-MM-dd") & "', UPPER('" & txt_del_loc.Text & "'), UPPER('" & txt_notes.Text & "'), '" & Format(rd_poDate.SelectedDate, "yyyy-MM-dd") & "', " & subtotal & ", " & Integer.Parse(txt_discount.Text) & ", " & Integer.Parse(txt_ppn.Text) & ", " & Integer.Parse(txt_shipping.Text) & ", " & Integer.Parse(txt_total_po.Text) & ", UPPER('" & txt_payment_term.Text & "'), '" & ddl_curr.Text & "', '" & rbl_goods_type.SelectedValue.ToString() & "')", oConn)
                sqlCmd.ExecuteNonQuery()

                Dim sqlCmdDetail As New MySqlCommand("INSERT INTO purchase_order_item (po_no, po_item_no, catalog_id, item_name, quantity, uoi, uop, conv_factor, price, subtotal, create_by, create_date, qty_outstanding, qty_receive, merk) " & _
                                                     "SELECT '" & txt_po_no.Text & "', po_item_no, catalog_id, item_name, quantity, uoi, uop, conv_factor, price, subtotal, UPPER('" & create_by & "'), '" & create_date & "', quantity, '0', merk FROM temp_po", oConn)
                sqlCmdDetail.ExecuteNonQuery()
                oConn.Close()

                DropDataTemp()
                Dim message As String = "PO No = " + txt_po_no.Text
                RadWindowManager1.RadAlert(message, 200, 100, "Message", "refreshToTop")
            Else
                oDatareader.Close()
                Dim message As String = "PO Item empty"
                RadWindowManager1.RadAlert(message, 200, 100, "Message", "refreshToItemData")
            End If
        Catch ex As Exception
            oConn.Close()
            MessageException("Insert transaction failed", ex, lbl_msg, "text-warning")
        End Try
    End Sub

    Protected Sub btn_clear_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_clear.Click
        DropDataTemp()
        Response.Redirect("PurchaseOrder.aspx")
    End Sub

    Protected Sub btn_search_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_search.Click
        Dim oDatareader As MySqlDataReader
        Try
            oConn.Open()
            Dim sqlCmd As New MySqlCommand("select * from purchase_order where po_no = UPPER('" & txt_search_po.Text & "') ", oConn)
            oDatareader = sqlCmd.ExecuteReader()
            If oDatareader.HasRows() Then
                If oDatareader.Read() Then
                    txt_po_no.Text = oDatareader("po_no").ToString()
                    ddl_company.SelectedValue = oDatareader("company_name").ToString()
                    rd_poDate.SelectedDate = Date.Parse(oDatareader("po_date").ToString())
                    ddl_supplier.SelectedValue = oDatareader("supplier_id").ToString()
                    rd_deliveryDate.SelectedDate = Date.Parse(oDatareader("delivery_date").ToString())
                    rd_prDate.SelectedDate = Date.Parse(oDatareader("pr_date").ToString())
                    txt_del_loc.Text = oDatareader("delivery_location").ToString()
                    rbl_goods_type.SelectedValue = oDatareader("goods").ToString()
                    txt_pr_no.Text = oDatareader("pr_no").ToString()
                    txt_payment_term.Text = oDatareader("payment_term").ToString()
                    txt_notes.Text = oDatareader("po_remark").ToString()
                    txt_discount.Text = oDatareader("discount").ToString()
                    txt_ppn.Text = oDatareader("ppn").ToString()
                    txt_shipping.Text = oDatareader("transportation").ToString()
                    txt_total_po.Text = oDatareader("total").ToString()
                    ddl_curr.SelectedValue = oDatareader("currency").ToString()
                    oDatareader.Close()
                End If
                oDatareader.Close()


                Dim sqlCmd_temp As New MySqlCommand("insert into temp_po (po_no, po_item_no, catalog_id, item_name, merk, quantity, uoi, uop, conv_factor, price, subtotal) (select po_no, po_item_no, catalog_id, item_name, merk, quantity, uoi, uop, conv_factor, price, subtotal from purchase_order_item where po_no = '" & txt_search_po.Text & "' )", oConn)
                sqlCmd_temp.ExecuteNonQuery()
                rg_temp.Rebind()

                Dim sqlCmd2 As New MySqlCommand("select * from supplier where supplier_id = '" & ddl_supplier.SelectedValue.ToString() & "'", oConn)
                oDatareader = sqlCmd2.ExecuteReader()
                If oDatareader.HasRows() Then
                    If oDatareader.Read() Then
                        txt_address.Text = oDatareader("dddress").ToString()
                        txt_phone.Text = oDatareader("phone").ToString()
                        txt_fax.Text = oDatareader("fax").ToString()
                        txt_contactPerson.Text = oDatareader("contact_name").ToString()
                    End If
                End If
                oConn.Close()

                txt_item_no.Text = GetItemNo()
            Else
                lbl_msg_search.Text = "PO No not found"
            End If
        Catch ex As Exception
            oConn.Close()
            MessageException("Load data search PO failed", ex, lbl_msg_search, "text-warning")
        End Try
    End Sub

    Protected Sub btn_modify_po_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_modify_po.Click
        Dim subtotal As Integer = 0
        Dim update_date As String = Date.Now.ToString("yyyy-MM-dd HH:mm:ss")
        Dim oDatareader As MySqlDataReader
        Dim po_no As String = txt_po_no.Text

        Try
            oConn.Open()
            Dim sqlCmd1 As New MySqlCommand("select sum(subtotal) as subtotal from temp_po where po_no = UPPER('" & po_no & "')", oConn)
            oDatareader = sqlCmd1.ExecuteReader()
            If oDatareader.HasRows() Then
                If oDatareader.Read() Then
                    subtotal = Integer.Parse(oDatareader("subtotal").ToString())
                End If
            Else
                subtotal = 0
            End If
            oDatareader.Close()
            Dim sqlCmd2 As New MySqlCommand("update purchase_order set supplier_id = '" & ddl_supplier.SelectedValue.ToString() & "', supplier_name = '" & ddl_supplier.SelectedItem.ToString() & "', company_name = '" & ddl_company.SelectedValue.ToString() & "', pr_no = UPPER('" & txt_pr_no.Text & "'), pr_date = '" & Format(rd_prDate.SelectedDate, "yyyy-MM-dd") & "', delivery_date = '" & Format(rd_deliveryDate.SelectedDate, "yyyy-MM-dd") & "', delivery_location = UPPER('" & txt_del_loc.Text & "'), po_remark = UPPER('" & txt_notes.Text & "'), po_date = '" & Format(rd_poDate.SelectedDate, "yyyy-MM-dd") & "', subtotal = " & subtotal & ", discount = " & Integer.Parse(txt_discount.Text) & ", ppn = " & Integer.Parse(txt_ppn.Text) & ", transportation = " & Integer.Parse(txt_shipping.Text) & ", total = " & Integer.Parse(txt_total_po.Text) & ", payment_term = UPPER('" & txt_payment_term.Text & "'), currency = '" & ddl_curr.SelectedValue.ToString() & "', goods = '" & rbl_goods_type.SelectedValue.ToString() & "', update_by = UPPER('" & create_by & "'), update_date = '" & update_date & "' where po_no = '" & po_no & "'", oConn)
            sqlCmd2.ExecuteNonQuery()

            Dim str_query As String = ""
            For Each item As GridDataItem In rg_temp.MasterTableView.Items
                Dim po_item_no As String = item.Item("po_item_no").Text
                Dim catalog_id As String = item.Item("catalog_id").Text
                Dim flag As String = item.Item("flag").Text
                If catalog_id = "&nbsp;" Then
                    catalog_id = ""
                End If

                If flag = "N" Then 'new item
                    str_query = "INSERT INTO purchase_order_item (po_no, po_item_no, catalog_id, item_name, quantity, uoi, uop, conv_factor, price, subtotal, create_by, create_date, qty_outstanding, qty_receive, merk) VALUES('" & po_no & "', '" & po_item_no & "', '" & catalog_id & "', UPPER('" & item.Item("item_name").Text & "'), " & Integer.Parse(item.Item("quantity").Text) & ", '" & item.Item("uoi").Text & "', '" & item.Item("uop").Text & "', " & Integer.Parse(item.Item("conv_factor").Text) & ", " & Integer.Parse(item.Item("price").Text) & ", " & Integer.Parse(item.Item("subtotal").Text) & ", UPPER('" & create_by & "'), '" & update_date & "', " & Integer.Parse(item.Item("quantity").Text) & ", 0, '" & item.Item("merk").Text & "')"
                    Dim sqlCmd_update As New MySqlCommand(str_query, oConn)
                    sqlCmd_update.ExecuteNonQuery()
                ElseIf flag = "U" Then 'update old item
                    str_query = "UPDATE purchase_order_item set catalog_id = '" & catalog_id & "', item_name = UPPER('" & item.Item("item_name").Text & "'), quantity = " & Integer.Parse(item.Item("quantity").Text) & ", uoi = '" & item.Item("uoi").Text & "', uop = '" & item.Item("uop").Text & "', conv_factor = " & Integer.Parse(item.Item("conv_factor").Text) & ", price = " & Integer.Parse(item.Item("price").Text) & ", subtotal = " & Integer.Parse(item.Item("subtotal").Text) & ", qty_outstanding = " & Integer.Parse(item.Item("quantity").Text) & ", merk = '" & item.Item("merk").Text & "', update_by = UPPER('" & create_by & "'), update_date = '" & update_date & "' WHERE po_no = '" & po_no & "' and po_item_no = '" & po_item_no & "'"
                    Dim sqlCmd_update As New MySqlCommand(str_query, oConn)
                    sqlCmd_update.ExecuteNonQuery()
                End If
                oDatareader.Close()
            Next
            oConn.Close()

            DropDataTemp()
            Dim message As String = "PO No " & txt_po_no.Text & " has been updated"
            RadWindowManager1.RadAlert(message, 200, 100, "Message", "refreshToTop")
        Catch ex As Exception
            oConn.Close()
            MessageException("Modify transaction failed", ex, lbl_msg, "text-warning")
        End Try
    End Sub

    Protected Sub rg_temp_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles rg_temp.ItemDataBound
        'If TypeOf e.Item Is GridDataItem Then
        '    Dim item As GridDataItem = DirectCast(e.Item, GridDataItem)
        '    Dim catalog_id As String = item("catalog_id").Text
        '    Dim qty_outstanding As Integer = Integer.Parse(item("qty_outstanding").Text)
        '    Dim cb_choose As CheckBox = DirectCast(item.FindControl("cb_choose"), CheckBox)
        '    Dim txt_qty_received As TextBox = DirectCast(item.FindControl("txt_qty_received"), TextBox)
        '    Dim txt_conv_factor As TextBox = DirectCast(item.FindControl("txt_conv_factor"), TextBox)

        '    If catalog_id = "&nbsp;" Or qty_outstanding = 0 Then
        '        cb_choose.Enabled = False
        '        txt_qty_received.Enabled = False
        '        txt_conv_factor.Enabled = False
        '    Else
        '        cb_choose.Enabled = True
        '        txt_qty_received.Enabled = True
        '        txt_conv_factor.Enabled = True
        '    End If
        'End If
        'If str_type = "mod" Then
        '    If TypeOf e.Item Is GridDataItem Then
        '        Dim item As GridDataItem = DirectCast(e.Item, GridDataItem)
        '        (TryCast(item("delete").Controls(0), ImageButton)).Enabled = False
        '    End If
        'End If
    End Sub
End Class