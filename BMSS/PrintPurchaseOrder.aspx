﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/WhiteDesign.Master" CodeBehind="PrintPurchaseOrder.aspx.vb" Inherits="BMSS.PrintPurchaseOrder" %>

<%@ Register Assembly="Telerik.ReportViewer.WebForms, Version=8.0.14.225, Culture=neutral, PublicKeyToken=a9d7983dfcc261be" Namespace="Telerik.ReportViewer.WebForms" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<%--<script type="text/javascript">  
    function PrintReport()  
    {  
        <%=ReportViewer1.ClientID %>.PrintReport();  
    }  
</script> --%>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Print Purchase Order</h1>
    </div>
</div>
<div class="row">
    <form role="form">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <asp:Button ID="btn_print" runat="server" Text="Print" class="btn btn-primary"/> 
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12" align="center">
                            <telerik:ReportViewer ID="ReportViewer1" runat="server" Height="900px" 
                                Width="900px" ShowDocumentMapButton="False" ShowExportGroup="False" 
                                ShowHistoryButtons="False" ShowNavigationGroup="False" 
                                ShowParametersButton="False" ShowPrintButton="true" 
                                ShowPrintPreviewButton="False" ShowRefreshButton="False" Skin="Office2007">
                            </telerik:ReportViewer>
                            <%--<script type="text/javascript">
                                ReportViewer.prototype.PrintReport = function () {
                                    alert('Printing Report');
                                    this.PrintAs("Default");
                                }  
                            </script> --%>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
</asp:Content>
