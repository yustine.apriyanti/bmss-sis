﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/WhiteDesign.Master" CodeBehind="MasterCatalog.aspx.vb" Inherits="BMSS.MasterCatalog" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Catalog</h1>
    </div>
</div>

<asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <div class="row">
            <form role="form">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <%--<div class="panel-heading">
                            <label></label>
                        </div>--%>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>Catalog ID</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <asp:TextBox ID="txt_catalog_id" runat="server" class="form-control" ReadOnly="true"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Item Name</label>&nbsp;
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="*" 
                                            ControlToValidate="txt_item_name" CssClass="error_msg" ValidationGroup="add">
                                            </asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="form-group">
                                            <asp:TextBox ID="txt_item_name" runat="server" class="form-control" ></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Merk</label>&nbsp;
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*" 
                                            ControlToValidate="rcb_merk" CssClass="error_msg" ValidationGroup="add">
                                            </asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <telerik:RadComboBox ID="rcb_merk" runat="server"
                                            EnableAutomaticLoadOnDemand="false" EnableVirtualScrolling="True" 
                                            EmptyMessage="Select merk"  
                                                DataSourceID="sql_merk" DataTextField="description" 
                                                DataValueField="code" Skin="Bootstrap" Width="100%">
                                            </telerik:RadComboBox>
                                            <asp:SqlDataSource ID="sql_merk" runat="server" 
                                                ConnectionString="<%$ ConnectionStrings:conn_str %>" 
                                                ProviderName="<%$ ConnectionStrings:conn_str.ProviderName %>" 
                                                SelectCommand="SELECT cat_id, code, description FROM code_list WHERE (cat_id ='MRK') and active = 1 order by code ASC">
                                            </asp:SqlDataSource>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Stock (SOH)</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <asp:TextBox ID="txt_soh" runat="server" class="form-control" ReadOnly="true"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>UOI</label>&nbsp;
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*" 
                                            ControlToValidate="rcb_uoi" CssClass="error_msg" ValidationGroup="add">
                                            </asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <telerik:RadComboBox ID="rcb_uoi" runat="server"
                                            EnableAutomaticLoadOnDemand="false" EnableVirtualScrolling="True" 
                                            EmptyMessage="Select UOI"
                                                DataSourceID="sql_uom" DataTextField="description" 
                                                DataValueField="code" Skin="Bootstrap" Width="100%">
                                            </telerik:RadComboBox>
                                            <asp:SqlDataSource ID="sql_uom" runat="server" 
                                                ConnectionString="<%$ ConnectionStrings:conn_str %>" 
                                                ProviderName="<%$ ConnectionStrings:conn_str.ProviderName %>" 
                                                SelectCommand="SELECT cat_id, code, description FROM code_list WHERE (cat_id ='UOM') and active = 1 order by code ASC">
                                            </asp:SqlDataSource>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>UOP</label>&nbsp;
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="*" 
                                            ControlToValidate="rcb_uop" CssClass="error_msg" ValidationGroup="add">
                                            </asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <telerik:RadComboBox ID="rcb_uop" runat="server"
                                            EnableAutomaticLoadOnDemand="false"
                                            EmptyMessage="Select UOP" DropDownAutoWidth="Enabled" 
                                                DataSourceID="sql_uom" DataTextField="description" 
                                                DataValueField="code" Skin="Bootstrap" Width="100%">
                                            </telerik:RadComboBox>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Conversion Factor</label>&nbsp;
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*" 
                                            ControlToValidate="txt_conv_factor" CssClass="error_msg" ValidationGroup="add">
                                            </asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <asp:TextBox ID="txt_conv_factor" runat="server" class="form-control"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
                                            ErrorMessage="Only Numbers allowed" ValidationExpression="^\d+(\.\d{1,2})?$" 
                                                ControlToValidate="txt_conv_factor" ForeColor="Red"/>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Active</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <asp:CheckBox ID="cb_active" runat="server" Enabled="false" Checked="true"/>
                                        </div>
                                    </div>
                                </div> 
                            </div>
                        </div>

                        <div class="panel-body">
                            <div class="form-group">
                                <asp:Button ID="btn_submit" runat="server" Text="Submit" class="btn btn-primary" ValidationGroup="add"/> 
                                <asp:Button ID="btn_clear" runat="server" Text="Clear" class="btn btn-primary"/> 
                            </div>
                            <div class="form-group">
                                <asp:Label ID="lbl_msg" runat="server" Text="" class="text-warning"></asp:Label>
                            </div>
                        </div>

                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <telerik:radgrid ID="rg_catalog" runat="server" DataSourceID="sql_catalog" 
                                     AllowPaging="True" PageSize="25" 
                                        CssClass="table table-striped table-bordered table-hover" Skin="Bootstrap">
                                        <ClientSettings>
                                            <Scrolling AllowScroll="True" ScrollHeight="450px" UseStaticHeaders="True" />
                                        </ClientSettings>
                                        <MasterTableView AutoGenerateColumns="False" DataKeyNames="catalog_id" 
                                            DataSourceID="sql_catalog">
                                            <Columns>
                                                <telerik:GridButtonColumn ButtonType="ImageButton" ImageUrl="~/Styles/edit.png" CommandName="choose" UniqueName="Edit" ButtonCssClass="imageButtonClass">
                                                    <HeaderStyle HorizontalAlign="Center" /> 
                                                    <ItemStyle HorizontalAlign="Center" /> 
                                                </telerik:GridButtonColumn>
                                                <telerik:GridBoundColumn DataField="catalog_id"
                                                    FilterControlAltText="Filter catalog_id column" HeaderText="Catalog ID" 
                                                    HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" SortExpression="catalog_id" UniqueName="catalog_id">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="item_name" 
                                                    FilterControlAltText="Filter item_name column" HeaderText="Item Name" 
                                                    HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" SortExpression="item_name" UniqueName="item_name">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="merk" 
                                                    FilterControlAltText="Filter merk column" HeaderText="Merk" 
                                                    HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" SortExpression="merk" UniqueName="merk">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="uoi" 
                                                    FilterControlAltText="Filter uoi column" HeaderText="UOI" 
                                                    HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" SortExpression="uoi" UniqueName="uoi">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="uop" 
                                                    FilterControlAltText="Filter uop column" HeaderText="UOP" 
                                                    HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" SortExpression="uop" UniqueName="uop">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="conv_factor" 
                                                    FilterControlAltText="Filter conv_factor column" HeaderText="Conv Factor" 
                                                    HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" SortExpression="conv_factor" UniqueName="conv_factor">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="soh" 
                                                    FilterControlAltText="Filter soh column" HeaderText="SOH" 
                                                    HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" SortExpression="soh" UniqueName="soh">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="create_by" 
                                                    FilterControlAltText="Filter create_by column" HeaderText="Create By" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" SortExpression="create_by" UniqueName="create_by">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="create_date" 
                                                    FilterControlAltText="Filter create_date column" HeaderText="Create Date" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" SortExpression="create_date" UniqueName="create_date">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="update_by" 
                                                    FilterControlAltText="Filter update_by column" HeaderText="Update By" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" SortExpression="update_by" UniqueName="update_by">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="update_date" 
                                                    FilterControlAltText="Filter update_date column" HeaderText="Update Date" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" SortExpression="update_date"  UniqueName="update_date">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridCheckBoxColumn UniqueName="active" HeaderText="Active" DataField="active" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                                </telerik:GridCheckBoxColumn>
                                            </Columns>
                                        </MasterTableView>
                                    </telerik:radgrid>
                                    <asp:SqlDataSource ID="sql_catalog" runat="server" 
                                    ConnectionString="<%$ ConnectionStrings:conn_str %>" 
                                    ProviderName="<%$ ConnectionStrings:conn_str.ProviderName %>" 
                                    SelectCommand="SELECT * FROM catalog">
                                    </asp:SqlDataSource>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                    
                <div class="col-lg-12">
                    
                </div>
            </form>
        </div>
    </ContentTemplate>
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="btn_submit" EventName="Click" />
        <asp:AsyncPostBackTrigger ControlID="btn_clear" EventName="Click" />
    </Triggers>
</asp:UpdatePanel>

</asp:Content>
