﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/WhiteDesign.Master" CodeBehind="MasterSupplier.aspx.vb" Inherits="BMSS.MasterSupplier" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Supplier</h1>
    </div>
</div>
<asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <div class="row">
            <form role="form">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <%--<div class="panel-heading">
                            <label></label>
                        </div>--%>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>Supplier ID</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <asp:TextBox ID="txt_supplier_id" runat="server" class="form-control" ReadOnly="true"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Supplier Name</label>
                                        </div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="form-group">
                                            <asp:TextBox ID="txt_supplier_name" runat="server" class="form-control" ></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Address</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <asp:TextBox ID="txt_address" runat="server" class="form-control" 
                                                TextMode="MultiLine" Height="75px"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Contact Name</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <asp:TextBox ID="txt_contact_name" runat="server" class="form-control"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Phone</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <asp:TextBox ID="txt_phone" runat="server" class="form-control"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Fax</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <asp:TextBox ID="txt_fax" runat="server" class="form-control"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Email</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <asp:TextBox ID="txt_email" runat="server" class="form-control"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Active</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <asp:CheckBox ID="cb_active" runat="server" Enabled="false" Checked="true"/>
                                        </div>
                                    </div>
                                </div> 
                            </div>
                        </div>

                        <div class="panel-body">
                            <div class="form-group">
                                <asp:Button ID="btn_submit" runat="server" Text="Submit" class="btn btn-primary"/> 
                                <asp:Button ID="btn_clear" runat="server" Text="Clear" class="btn btn-primary"/> 
                            </div>
                            <div class="form-group">
                                <asp:Label ID="lbl_msg" runat="server" Text="" class="text-warning"></asp:Label>
                            </div>
                        </div>

                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <telerik:RadGrid ID="rg_supplier" runat="server" DataSourceID="sql_supplier"  AllowPaging="True" PageSize="25"
                                    CssClass="table table-striped table-bordered table-hover" >
                                        <ClientSettings>
                                            <Scrolling AllowScroll="True" ScrollHeight="450px" UseStaticHeaders="True" />
                                        </ClientSettings>
                                        <MasterTableView AutoGenerateColumns="False" DataKeyNames="supplier_id" DataSourceID="sql_supplier">
                                            <Columns>
                                                <telerik:GridButtonColumn ButtonType="ImageButton" ImageUrl="~/Styles/edit.png" CommandName="choose" UniqueName="Edit" ButtonCssClass="imageButtonClass">
                                                    <HeaderStyle HorizontalAlign="Center" /> 
                                                    <ItemStyle HorizontalAlign="Center" /> 
                                                </telerik:GridButtonColumn>
                                                <telerik:GridBoundColumn DataField="supplier_id"
                                                    FilterControlAltText="Filter supplier_id column" HeaderText="Supplier ID" 
                                                    SortExpression="supplier_id" UniqueName="supplier_id" >
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="supplier_name" 
                                                    FilterControlAltText="Filter supplier_name column" HeaderText="Supplier Name" 
                                                    SortExpression="supplier_name" UniqueName="supplier_name">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="address" 
                                                    FilterControlAltText="Filter address column" HeaderText="Address" 
                                                    SortExpression="address" UniqueName="address">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="contact_name" 
                                                    FilterControlAltText="Filter contact_name column" HeaderText="Contact Name" 
                                                    SortExpression="contact_name" UniqueName="contact_name">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="phone" 
                                                    FilterControlAltText="Filter phone column" HeaderText="Phone" 
                                                    SortExpression="phone" UniqueName="phone">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="fax" 
                                                    FilterControlAltText="Filter fax column" HeaderText="Fax" SortExpression="fax" 
                                                    UniqueName="fax">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="email" 
                                                    FilterControlAltText="Filter email column" HeaderText="Email" SortExpression="email" 
                                                    UniqueName="email">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="create_by"
                                                    FilterControlAltText="Filter create_by column" HeaderText="Create By" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" 
                                                    SortExpression="create_by" UniqueName="create_by">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="create_date"
                                                    FilterControlAltText="Filter create_date column" HeaderText="Create Date" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" 
                                                    SortExpression="create_date" UniqueName="create_date">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="update_by"
                                                    FilterControlAltText="Filter update_by column" HeaderText="Update By" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" 
                                                    SortExpression="update_by" UniqueName="update_by">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="update_date"
                                                    FilterControlAltText="Filter update_date column" HeaderText="Update Date" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" 
                                                    SortExpression="update_date" UniqueName="update_date">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridCheckBoxColumn UniqueName="active" HeaderText="Active" DataField="active" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                                </telerik:GridCheckBoxColumn>
                                            </Columns>
                                        </MasterTableView>
                                    </telerik:RadGrid>
                                    <asp:SqlDataSource ID="sql_supplier" runat="server" 
                                    ConnectionString="<%$ ConnectionStrings:conn_str %>" 
                                    ProviderName="<%$ ConnectionStrings:conn_str.ProviderName %>" 
                                    SelectCommand="SELECT * FROM supplier" >
                                    </asp:SqlDataSource>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                    
                <div class="col-lg-12">
                    
                </div>
            </form>
        </div>
    </ContentTemplate>
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="btn_submit" EventName="Click" />
        <asp:AsyncPostBackTrigger ControlID="btn_clear" EventName="Click" />        
    </Triggers>
</asp:UpdatePanel>
</asp:Content>
