﻿Imports MySql.Data.MySqlClient
Imports Telerik.Web.UI

Public Class MasterCodeList
    Inherits BaseClass

    Dim oConn As New MySqlConnection(ConfigurationManager.ConnectionStrings("conn_str").ConnectionString)
    Dim create_by As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("login") Is Nothing Then
            Response.Redirect("Login.aspx")
        Else
            If Not IsPostBack Then
                rn_order_no.Text = "0"
            End If
        End If
    End Sub

    Sub ClearData()
        rcb_cat_id.ClearSelection()
        txt_code.Text = ""
        txt_description.Text = ""
        rn_order_no.Text = "0"
        rg_code_list.Rebind()
        lbl_msg.Text = ""
        cb_active.Enabled = False
        cb_active.Checked = True
        btn_submit.Text = "Submit"
        btn_clear.Text = "Clear"
    End Sub

    Sub TransactionData(ByVal cat_id As String)
        create_by = Session("username").ToString()
        Dim create_date As String = Date.Now.ToString("yyyy-MM-dd HH:mm:ss")
        Dim active As String = ""
        Dim msg As String = ""
        If cb_active.Checked = True Then
            active = "1"
        Else
            active = "0"
        End If

        Try
            oConn.Open()
            If btn_submit.Text = "Submit" Then
                msg = "Insert transaction"
                Dim sql_insert As New MySqlCommand("INSERT INTO code_list (cat_id, code, description, seq_no, create_by, create_date, active) values(UPPER('" & cat_id & "'), UPPER('" & txt_code.Text & "'), UPPER('" & txt_description.Text & "'), " & rn_order_no.Text & ", UPPER(UPPER(UPPER('" & create_by & "'))), '" & create_date & "', '" & active & "')", oConn)
                sql_insert.ExecuteNonQuery()
            ElseIf btn_submit.Text = "Update" Then
                msg = "Update transaction"
                Dim sql_update As New MySqlCommand("UPDATE code_list SET code = UPPER('" & txt_code.Text & "'), description = UPPER('" & txt_description.Text & "'), seq_no = " & rn_order_no.Text & ", active = '" & active & "', update_by = UPPER(UPPER('" & create_by & "')), update_date = '" & create_date & "' WHERE cat_id = '" & cat_id & "' and code = '" & lbl_old_code.Text & "'", oConn)
                sql_update.ExecuteNonQuery()
            End If

            oConn.Close()
            ClearData()
            rcb_cat_id.Items.Clear()
            rcb_cat_id.DataBind()
            lbl_msg.Text = msg & " success"
            lbl_msg.CssClass = "text-success"
        Catch ex As Exception
            oConn.Close()
            MessageException(msg & " failed", ex, lbl_msg, "text-warning")
        End Try
    End Sub

    Protected Sub btn_add_new_cat_id_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_add_new_cat_id.Click
        TransactionData("CAT")
    End Sub

    Protected Sub btn_submit_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_submit.Click
        TransactionData(rcb_cat_id.SelectedValue.ToString())
    End Sub

    Protected Sub btn_clear_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_clear.Click
        ClearData()
    End Sub

    Protected Sub rg_code_list_ItemCommand(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs) Handles rg_code_list.ItemCommand
        If e.CommandName = "choose" Then
            lbl_msg.Text = ""
            Dim item As GridDataItem = CType(e.Item, GridDataItem)
            rcb_cat_id.SelectedValue = item("cat_id").Text
            txt_code.Text = item("code").Text
            lbl_old_code.Text = item("code").Text
            txt_description.Text = item("description").Text
            rn_order_no.Text = item("seq_no").Text
            If item("active").Text = "0" Then
                cb_active.Checked = False
            Else
                cb_active.Checked = True
            End If
            cb_active.Enabled = True
            btn_submit.Text = "Update"
            btn_clear.Text = "Cancel"
        End If
    End Sub
End Class