﻿Imports MySql.Data.MySqlClient
Imports Telerik.Web.UI

Public Class MasterCatalog
    Inherits BaseClass

    Dim oConn As New MySqlConnection(ConfigurationManager.ConnectionStrings("conn_str").ConnectionString)
    Dim create_by As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("login") Is Nothing Then
            Response.Redirect("Login.aspx")
        Else
            If Not IsPostBack Then
                txt_catalog_id.Text = GenerateNo()
                txt_soh.Text = "0"
            End If
        End If
    End Sub

    Function GenerateNo() As String
        Dim kode As Integer
        Dim no As String
        oConn.Open()
        Dim sqlCmd2 As New MySqlCommand("select * from catalog", oConn)
        Dim oDatareader As MySqlDataReader
        oDatareader = sqlCmd2.ExecuteReader()
        If oDatareader.HasRows() Then
            oDatareader.Close()
            Dim sqlCmd As New MySqlCommand("SELECT MAX(RIGHT(catalog_id,4)) + 1 FROM catalog LIMIT 1", oConn)
            kode = sqlCmd.ExecuteScalar().ToString()
            oConn.Close()
            If kode.ToString() = "" Then
                no = 1
            Else
                no = Convert.ToInt16(kode.ToString())
            End If
        Else
            no = 1
            oConn.Close()
        End If
        Return "C" & Right("0000" + no.ToString(), 4)
    End Function

    Sub ClearData()
        txt_catalog_id.Text = GenerateNo()
        txt_item_name.Text = ""
        rcb_merk.ClearSelection()
        rcb_uoi.ClearSelection()
        rcb_uop.ClearSelection()
        txt_conv_factor.Text = ""
        txt_soh.Text = "0"
        rg_catalog.Rebind()
        lbl_msg.Text = ""
        cb_active.Enabled = False
        cb_active.Checked = True
        btn_submit.Text = "Submit"
        btn_clear.Text = "Clear"
    End Sub

    Protected Sub btn_submit_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_submit.Click
        create_by = Session("username").ToString()
        Dim create_date As String = Date.Now.ToString("yyyy-MM-dd HH:mm:ss")
        Dim trans_date As String = Date.Now.ToString("yyyy-MM-dd")
        Dim active As String = ""
        Dim msg As String = ""
        If cb_active.Checked = True Then
            active = "1"
        Else
            active = "0"
        End If

        Try
            oConn.Open()
            If btn_submit.Text = "Submit" Then
                msg = "Insert transaction"
                Dim sql_insert As New MySqlCommand("INSERT INTO catalog (catalog_id, item_name, merk, uoi, uop, conv_factor, soh, create_by, create_date, active) values('" & txt_catalog_id.Text & "', UPPER('" & txt_item_name.Text & "'), '" & rcb_merk.SelectedValue.ToString() & "', '" & rcb_uoi.SelectedValue.ToString() & "', '" & rcb_uop.SelectedValue.ToString() & "', " & Integer.Parse(txt_conv_factor.Text) & ", 0, UPPER('" & create_by & "'), '" & create_date & "', '" & active & "')", oConn)
                sql_insert.ExecuteNonQuery()
                Dim sql_insert_ic As New MySqlCommand("INSERT INTO transaction(trans_date, trans_type, seq_no, catalog_id, item_name, unit, quantity, stock, price, merk, create_by, create_date, notes) values(DATE_FORMAT('" & trans_date & "','%Y-%m-%d'), 'IC', 0, '" & txt_catalog_id.Text & "', '" & txt_item_name.Text & "', '" & rcb_uoi.SelectedValue.ToString() & "', 0, 0, 0, '" & rcb_merk.SelectedValue.ToString() & "', UPPER('" & create_by & "'), '" & create_date & "', 'DAFTAR CATALOG BARU')", oConn)
                sql_insert_ic.ExecuteNonQuery()
            ElseIf btn_submit.Text = "Update" Then
                msg = "Update transaction"
                Dim sql_update As New MySqlCommand("UPDATE catalog SET item_name = UPPER('" & txt_item_name.Text & "'), merk = '" & rcb_merk.SelectedValue.ToString() & "', uoi = '" & rcb_uoi.SelectedValue.ToString() & "', uop = '" & rcb_uop.SelectedValue.ToString() & "', conv_factor = " & Integer.Parse(txt_conv_factor.Text) & ", active = '" & active & "', update_by = UPPER('" & create_by & "'), update_date = '" & create_date & "' where catalog_id = '" & txt_catalog_id.Text & "' ", oConn)
                sql_update.ExecuteNonQuery()
            End If
            oConn.Close()

            ClearData()
            lbl_msg.Text = msg & " success"
            lbl_msg.CssClass = "text-success"
        Catch ex As Exception
            oConn.Close()
            MessageException(msg & " failed", ex, lbl_msg, "text-warning")
        End Try
    End Sub

    Protected Sub btn_clear_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_clear.Click
        ClearData()
    End Sub

    Protected Sub rg_catalog_ItemCommand(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs) Handles rg_catalog.ItemCommand
        If e.CommandName = "choose" Then
            lbl_msg.Text = ""
            Dim item As GridDataItem = CType(e.Item, GridDataItem)
            txt_catalog_id.Text = item("catalog_id").Text
            txt_item_name.Text = item("item_name").Text
            rcb_merk.SelectedValue = item("merk").Text
            txt_soh.Text = item("soh").Text
            rcb_uoi.SelectedValue = item("uoi").Text
            rcb_uop.SelectedValue = item("uop").Text
            txt_conv_factor.Text = item("conv_factor").Text
            If item("active").Text = "0" Then
                cb_active.Checked = False
            Else
                cb_active.Checked = True
            End If
            cb_active.Enabled = True
            btn_submit.Text = "Update"
            btn_clear.Text = "Cancel"
        End If
    End Sub
End Class