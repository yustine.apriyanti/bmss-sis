﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="WebForm1.aspx.vb" Inherits="BMSS.WebForm1" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
<telerik:RadScriptManager ID="RadScriptManager1" runat="server">
</telerik:RadScriptManager>
    <telerik:RadGrid ID="RadGrid1" runat="server" CellSpacing="-1" 
        DataSourceID="sql_data" GridLines="Both" Skin="Bootstrap">
<GroupingSettings CollapseAllTooltip="Collapse all groups"></GroupingSettings>
        <MasterTableView AutoGenerateColumns="False" DataSourceID="sql_data">
            <GroupByExpressions>
                <telerik:GridGroupByExpression>
                    <SelectFields>
                        <telerik:GridGroupByField FieldName="descX" HeaderText="Catalog ID"></telerik:GridGroupByField>
                    </SelectFields>
                    <GroupByFields>
                        <telerik:GridGroupByField FieldName="descX"></telerik:GridGroupByField>
                    </GroupByFields>
                </telerik:GridGroupByExpression>
            </GroupByExpressions>
            <Columns>
                <telerik:GridBoundColumn DataField="descX"  Visible="false"
                    FilterControlAltText="Filter descX column" HeaderText="descX" 
                    SortExpression="descX" UniqueName="descX">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="trans_date" 
                    FilterControlAltText="Filter trans_date column" HeaderText="trans_date" 
                    SortExpression="trans_date" UniqueName="trans_date">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="user_request_name" 
                    FilterControlAltText="Filter user_request_name column" 
                    HeaderText="user_request_name" SortExpression="user_request_name" 
                    UniqueName="user_request_name">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="price" DataType="System.Int32" 
                    FilterControlAltText="Filter price column" HeaderText="price" 
                    SortExpression="price" UniqueName="price">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="RECV" DataType="System.Int64" 
                    FilterControlAltText="Filter RECV column" HeaderText="RECV" 
                    SortExpression="RECV" UniqueName="RECV">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="ACC" DataType="System.Decimal" 
                    FilterControlAltText="Filter ACC column" HeaderText="ACC" SortExpression="ACC" 
                    UniqueName="ACC">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="BD" DataType="System.Decimal" 
                    FilterControlAltText="Filter BD column" HeaderText="BD" SortExpression="BD" 
                    UniqueName="BD">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="DIR" DataType="System.Decimal" 
                    FilterControlAltText="Filter DIR column" HeaderText="DIR" SortExpression="DIR" 
                    UniqueName="DIR">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="ENG" DataType="System.Decimal" 
                    FilterControlAltText="Filter ENG column" HeaderText="ENG" SortExpression="ENG" 
                    UniqueName="ENG">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="EXT" DataType="System.Decimal" 
                    FilterControlAltText="Filter EXT column" HeaderText="EXT" SortExpression="EXT" 
                    UniqueName="EXT">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="FIN" DataType="System.Decimal" 
                    FilterControlAltText="Filter FIN column" HeaderText="FIN" SortExpression="FIN" 
                    UniqueName="FIN">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="HRD" DataType="System.Decimal" 
                    FilterControlAltText="Filter HRD column" HeaderText="HRD" SortExpression="HRD" 
                    UniqueName="HRD">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="IT" DataType="System.Decimal" 
                    FilterControlAltText="Filter IT column" HeaderText="IT" SortExpression="IT" 
                    UniqueName="IT">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="LAND" DataType="System.Decimal" 
                    FilterControlAltText="Filter LAND column" HeaderText="LAND" 
                    SortExpression="LAND" UniqueName="LAND">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="LEG" DataType="System.Decimal" 
                    FilterControlAltText="Filter LEG column" HeaderText="LEG" SortExpression="LEG" 
                    UniqueName="LEG">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="MAR" DataType="System.Decimal" 
                    FilterControlAltText="Filter MAR column" HeaderText="MAR" SortExpression="MAR" 
                    UniqueName="MAR">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="PURC" DataType="System.Decimal" 
                    FilterControlAltText="Filter PURC column" HeaderText="PURC" 
                    SortExpression="PURC" UniqueName="PURC">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="QC" DataType="System.Decimal" 
                    FilterControlAltText="Filter QC column" HeaderText="QC" SortExpression="QC" 
                    UniqueName="QC">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="SOP" DataType="System.Decimal" 
                    FilterControlAltText="Filter SOP column" HeaderText="SOP" SortExpression="SOP" 
                    UniqueName="SOP">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="SSU" DataType="System.Decimal" 
                    FilterControlAltText="Filter SSU column" HeaderText="SSU" SortExpression="SSU" 
                    UniqueName="SSU">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="WBS" DataType="System.Decimal" 
                    FilterControlAltText="Filter WBS column" HeaderText="WBS" SortExpression="WBS" 
                    UniqueName="WBS">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="stock" DataType="System.Int32" 
                    FilterControlAltText="Filter stock column" HeaderText="stock" 
                    SortExpression="stock" UniqueName="stock">
                </telerik:GridBoundColumn>
            </Columns>
        </MasterTableView>
    </telerik:RadGrid>
    <asp:SqlDataSource ID="sql_data" runat="server" 
        ConnectionString="<%$ ConnectionStrings:conn_str %>" 
        ProviderName="<%$ ConnectionStrings:conn_str.ProviderName %>" 
        SelectCommand="sp_report_transaction_copy" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:Parameter DefaultValue="2021-03" Name="ParPeriod" Type="String" />
            <%--<asp:Parameter DefaultValue="" Name="ParCatalogID" Type="String"/>
            <asp:Parameter DefaultValue="summary" Name="ParType" Type="String"/>--%>
        </SelectParameters><%--'summary','2021-03',''--%>
    </asp:SqlDataSource>
        
    </form>
</body>
</html>
