﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/WhiteDesign.Master" CodeBehind="MasterCodeList.aspx.vb" Inherits="BMSS.MasterCodeList" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Code List</h1>
    </div>
</div>

<asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <div class="row">
            <form role="form">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <%--<div class="panel-heading">
                            <label></label>
                        </div>--%>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>Category ID</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <%--<asp:DropDownList ID="ddl_cat_id" runat="server" class="form-control" 
                                            DataSourceID="sql_cat_id" DataTextField="DescX" 
                                            DataValueField="code" ResolvedRenderMode="Classic" autofocus  AppendDataBoundItems="True">
                                                    <asp:ListItem Value="-1" Selected="True">-- Select Category ID -- </asp:ListItem>
                                            </asp:DropDownList>--%> 
                                            <telerik:RadComboBox ID="rcb_cat_id" runat="server"
                                            EnableAutomaticLoadOnDemand="false" EnableVirtualScrolling="True" 
                                            EmptyMessage="Select Category ID"
                                                DataSourceID="sql_cat_id" DataTextField="DescX" 
                                                DataValueField="code" Skin="Bootstrap" Width="100%">
                                            </telerik:RadComboBox>
                                            <asp:SqlDataSource ID="sql_cat_id" runat="server" 
                                                ConnectionString="<%$ ConnectionStrings:conn_str %>" 
                                                ProviderName="<%$ ConnectionStrings:conn_str.ProviderName %>" 
                                                SelectCommand="SELECT cat_id, code, description, concat(code, ' - ', description) as DescX FROM code_list WHERE (cat_id ='CAT') and active = 1 order by cat_id ASC">
                                            </asp:SqlDataSource>  
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Code</label>
                                        </div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="form-group">
                                            <asp:TextBox ID="txt_code" runat="server" class="form-control" ></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Description</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <asp:TextBox ID="txt_description" runat="server" class="form-control"  ></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Order No</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <%--<asp:TextBox ID="txt_order_no" runat="server" class="form-control" ></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
                                            ErrorMessage="Only Numbers allowed" ValidationExpression="^\d+(\.\d{1,2})?$" 
                                                ControlToValidate="txt_order_no" ForeColor="Red"/>--%>
                                            <telerik:RadNumericTextBox ID="rn_order_no" runat="server" Skin="Bootstrap" NumberFormat-DecimalDigits="0">
                                            </telerik:RadNumericTextBox>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Active</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <asp:CheckBox ID="cb_active" runat="server" Enabled="false" Checked="true"/>
                                        </div>
                                    </div>
                                </div> 
                            </div>
                        </div>

                        <div class="panel-body">
                            <div class="form-group">
                                <asp:Button ID="btn_submit" runat="server" Text="Submit" class="btn btn-primary"/> 
                                <asp:Button ID="btn_clear" runat="server" Text="Clear" class="btn btn-primary"/> 
                                <asp:Button ID="btn_add_new_cat_id" runat="server" Text="Add New Cat ID" class="btn btn-primary"/>
                            </div>
                            <div class="form-group">
                                <asp:Label ID="lbl_msg" runat="server" Text="" class="text-warning"></asp:Label>
                                <asp:Label ID="lbl_old_code" runat="server" Text="" Visible="false"></asp:Label>
                            </div>
                        </div>

                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <telerik:RadGrid ID="rg_code_list" runat="server" DataSourceID="sql_code_list" 
                                        AllowPaging="True" PageSize="25"
                                        CssClass="table table-striped table-bordered table-hover" 
                                        Skin="Bootstrap" >
                                        <ClientSettings>
                                            <Scrolling AllowScroll="True" ScrollHeight="450px" UseStaticHeaders="True" />
                                        </ClientSettings>
                                        <MasterTableView AutoGenerateColumns="False" DataKeyNames="cat_id" 
                                            DataSourceID="sql_code_list">
                                            <Columns>
                                                <telerik:GridButtonColumn ButtonType="ImageButton" ImageUrl="~/Styles/edit.png" CommandName="choose" UniqueName="Edit" ButtonCssClass="imageButtonClass">
                                                    <HeaderStyle HorizontalAlign="Center" /> 
                                                    <ItemStyle HorizontalAlign="Center" /> 
                                                </telerik:GridButtonColumn>
                                                <telerik:GridBoundColumn DataField="cat_id" 
                                                    FilterControlAltText="Filter cat_id column" HeaderText="Category ID" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left"
                                                    SortExpression="cat_id" UniqueName="cat_id">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="code" 
                                                    FilterControlAltText="Filter code column" HeaderText="Code" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left"
                                                    SortExpression="code" UniqueName="code">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="description" 
                                                    FilterControlAltText="Filter description column" HeaderText="Description" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" 
                                                    SortExpression="description" UniqueName="description">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="seq_no" 
                                                    FilterControlAltText="Filter seq_no column" HeaderText="Order No" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" 
                                                    SortExpression="seq_no" UniqueName="seq_no">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="create_by"
                                                    FilterControlAltText="Filter create_by column" HeaderText="Create By" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" 
                                                    SortExpression="create_by" UniqueName="create_by">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="create_date"
                                                    FilterControlAltText="Filter create_date column" HeaderText="Create Date" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" 
                                                    SortExpression="create_date" UniqueName="create_date">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="update_by"
                                                    FilterControlAltText="Filter update_by column" HeaderText="Update By" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" 
                                                    SortExpression="update_by" UniqueName="update_by">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="update_date"
                                                    FilterControlAltText="Filter update_date column" HeaderText="Update Date" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" 
                                                    SortExpression="update_date" UniqueName="update_date">
                                                </telerik:GridBoundColumn>
                                                <%--<telerik:GridBoundColumn DataField="active"
                                                    FilterControlAltText="Filter active column" HeaderText="Active" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" 
                                                    SortExpression="active" UniqueName="active">
                                                </telerik:GridBoundColumn>--%>
                                                <telerik:GridCheckBoxColumn UniqueName="active" HeaderText="Active" DataField="active" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                                </telerik:GridCheckBoxColumn>
                                            </Columns>
                                        </MasterTableView>
                                        <ClientSettings Selecting-AllowRowSelect="true" EnablePostBackOnRowClick="true">
                                            <Selecting AllowRowSelect="True" />
                                        </ClientSettings>
                                    </telerik:RadGrid>
                                    <asp:SqlDataSource ID="sql_code_list" runat="server" 
                                        ConnectionString="<%$ ConnectionStrings:conn_str %>" 
                                        ProviderName="<%$ ConnectionStrings:conn_str.ProviderName %>" 
                                        SelectCommand="SELECT * FROM code_list where cat_id <> 'CAT' order by cat_id, seq_no asc"></asp:SqlDataSource>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    
                </div>
            </form>
        </div>
    </ContentTemplate>
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="btn_submit" EventName="Click" />
        <asp:AsyncPostBackTrigger ControlID="btn_clear" EventName="Click" />
        <asp:AsyncPostBackTrigger ControlID="btn_add_new_cat_id" EventName="Click" />
    </Triggers>
</asp:UpdatePanel>
</asp:Content>
