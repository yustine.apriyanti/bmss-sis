﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/WhiteDesign.Master" CodeBehind="StockMoveOut.aspx.vb" Inherits="BMSS.StockMoveOut" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Stock Movement OUT</h1>
    </div>
</div>

<asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
    <ContentTemplate>  
        <div class="row">
            <form role="form">
                <telerik:RadWindowManager RenderMode="Lightweight" runat="server" id="RadWindowManager1"></telerik:RadWindowManager>
                <script type="text/javascript">
                    function refreshToItemData(arg) {
                        window.scrollTo(0, 450);
                        window.location.reload();
                    }
                </script>
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <label>Stock OUT</label>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Issue No</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <asp:TextBox ID="txt_ir_no" runat="server" class="form-control" ReadOnly="true"></asp:TextBox>
                                        </div>
                                    </div>   
                                         
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Date</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <telerik:RadDatePicker ID="rd_date" runat="server" Skin="Bootstrap" Culture="en-US">
                                                <Calendar ID="Calendar1" UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" ViewSelectorText="x" runat="server"></Calendar>
                                                <DateInput ID="DateInput1" DisplayDateFormat="yyyy-MM-dd" DateFormat="yyyy-MM-dd" runat="server"></DateInput>
                                                <DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>
                                            </telerik:RadDatePicker>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Division</label>&nbsp;
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="*" 
                                            ControlToValidate="rcb_division" CssClass="error_msg" ValidationGroup="add">
                                            </asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">                                        
                                            <telerik:RadComboBox ID="rcb_division" runat="server" 
                                            EnableAutomaticLoadOnDemand="False" EnableVirtualScrolling="True" 
                                            EmptyMessage="Select division"
                                                DataSourceID="sql_division" DataTextField="desX" 
                                                DataValueField="code" Skin="Bootstrap" Width="100%">
                                            </telerik:RadComboBox>
                                            <asp:SqlDataSource ID="sql_division" runat="server" 
                                                ConnectionString="<%$ ConnectionStrings:conn_str %>" 
                                                ProviderName="<%$ ConnectionStrings:conn_str.ProviderName %>" 
                                                SelectCommand="SELECT cat_id, code, description, concat(code, ' - ', description) as desX FROM code_list WHERE (cat_id ='DIV') and active = 1 order by seq_no ASC">
                                            </asp:SqlDataSource>
                                        </div>
                                    </div>
                            
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>User Request Name</label>&nbsp;
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="*" 
                                            ControlToValidate="txt_user_req_name" CssClass="error_msg" ValidationGroup="add">
                                            </asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <asp:TextBox ID="txt_user_req_name" runat="server" class="form-control"  ></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="panel-body">
                            <div class="row"> 
                                <div class="col-lg-12">
                                    <telerik:RadGrid ID="rg_temp_catalog" runat="server" Skin="Bootstrap" AutoGenerateColumns="false"
                                    MasterTableView-NoDetailRecordsText="No Records" MasterTableView-NoMasterRecordsText="No Records"  OnItemCommand="rg_temp_catalog_ItemCommand" >
                                        <ClientSettings>
                                            <Scrolling AllowScroll="True" ScrollHeight="150px" UseStaticHeaders="True" />
                                        </ClientSettings>
                                        <MasterTableView>
                                            <Columns>
                                                <telerik:GridTemplateColumn HeaderText="Catalog ID">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_item_catalog_id" runat="server" Text='<%# Bind("catalog_id") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Center" Font-Bold="True" Width="75px"/> 
                                                    <ItemStyle HorizontalAlign="Left"  Width="75px"/> 
                                                </telerik:GridTemplateColumn>
                                                <telerik:GridTemplateColumn HeaderText="Merk - Item Name">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_item_description" runat="server" Text='<%# Bind("description") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Center" Font-Bold="True" Width="250px"/> 
                                                    <ItemStyle HorizontalAlign="Left" Width="250px"/> 
                                                </telerik:GridTemplateColumn>
                                                <telerik:GridTemplateColumn HeaderText="UOI">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_item_unit" runat="server" Text='<%# Bind("unit") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Center" Font-Bold="True"  Width="75px"/> 
                                                    <ItemStyle HorizontalAlign="Center" Width="75px"/> 
                                                </telerik:GridTemplateColumn>
                                                <telerik:GridTemplateColumn HeaderText="Stock">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_item_stock" runat="server" Text='<%# Bind("stock") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Center" Font-Bold="True" Width="75px"/> 
                                                    <ItemStyle HorizontalAlign="Right" Width="75px"/> 
                                                </telerik:GridTemplateColumn>
                                                <telerik:GridTemplateColumn HeaderText="Quantity">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_item_quantity" runat="server" Text='<%# Bind("quantity") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Center" Font-Bold="True" Width="75px" /> 
                                                    <ItemStyle HorizontalAlign="Right" Width="75px"/> 
                                                </telerik:GridTemplateColumn>
                                                <telerik:GridButtonColumn ButtonType="ImageButton" ImageUrl="~/Styles/edit.png" CommandName="choose" UniqueName="Edit" ButtonCssClass="imageButtonClass">
                                                    <HeaderStyle HorizontalAlign="Center" Width="35px" /> 
                                                    <ItemStyle HorizontalAlign="Center" Width="35px"/> 
                                                </telerik:GridButtonColumn>
                                                <telerik:GridButtonColumn ConfirmText="Are you sure you want to delete this record?" ConfirmDialogType="RadWindow" ConfirmTitle="Delete" ButtonType="ImageButton" ImageUrl="~/Styles/cancel.png" CommandName="cancel" UniqueName="Delete" ButtonCssClass="imageButtonClass">
                                                    <HeaderStyle HorizontalAlign="Center" Width="35px"/> 
                                                    <ItemStyle HorizontalAlign="Center" Width="35px"/> 
                                                </telerik:GridButtonColumn>
                                            </Columns>
                                        </MasterTableView>
                                    </telerik:RadGrid>
                                </div>
                            </div>
                        </div> 

                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Catalog</label>&nbsp;
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator01" runat="server" ErrorMessage="*" 
                                            ControlToValidate="rcb_catalog" CssClass="error_msg" ValidationGroup="add_item">
                                            </asp:RequiredFieldValidator>
                                        </div>
                                    </div> 
                                    <div class="col-sm-8">
                                        <div class="form-group">                                            
                                            <telerik:RadComboBox ID="rcb_catalog" runat="server" AutoPostBack="true" 
                                            EnableAutomaticLoadOnDemand="True" EnableVirtualScrolling="True" 
                                            EmptyMessage="Select catalog" DropDownAutoWidth="Enabled" 
                                                DataSourceID="sql_catalog" DataTextField="DescX" 
                                                DataValueField="catalog_id" Filter="Contains" Skin="Bootstrap" Width="100%">
                                            </telerik:RadComboBox>
                                            <asp:SqlDataSource ID="sql_catalog" runat="server" 
                                                ConnectionString="<%$ ConnectionStrings:conn_str %>" 
                                                ProviderName="<%$ ConnectionStrings:conn_str.ProviderName %>" 
                                                SelectCommand="SELECT catalog_id, item_name, concat(IF(ISNULL(merk),'', CONCAT('[', merk, '] - ')), item_name) as DescX FROM catalog WHERE active = 1 order by catalog_id ASC">
                                            </asp:SqlDataSource>
                                        </div>
                                    </div>
                                                        
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Unit</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <asp:TextBox ID="txt_uoi" runat="server" class="form-control" ReadOnly="true"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Stock</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <asp:TextBox ID="txt_soh" runat="server" class="form-control" ReadOnly="true"></asp:TextBox>
                                        </div>
                                    </div>
                            
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Quantity</label>&nbsp;
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*" 
                                            ControlToValidate="rn_qty" CssClass="error_msg" ValidationGroup="add_item">
                                            </asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">                                            
                                            <telerik:RadNumericTextBox ID="rn_qty" runat="server" Skin="Bootstrap" NumberFormat-DecimalDigits="0">
                                            </telerik:RadNumericTextBox>
                                        </div>
                                    </div>                                    
                                                               
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                        </div>
                                    </div>
                                                       
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <asp:Button ID="btn_update" runat="server" Text="Update" class="btn btn-primary btn-sm" Visible="false"/>
                                            <asp:Button ID="btn_addItems" runat="server" Text="Add Item" class="btn btn-primary btn-sm" ValidationGroup="add_item"/>  
                                            <asp:Label ID="lbl_stock" runat="server" Text="" class="text-warning"></asp:Label>
                                            <asp:Label ID="lbl_index" runat="server" Text="" Visible="false"></asp:Label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6">

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Notes</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <asp:TextBox ID="txt_notes" runat="server" class="form-control" TextMode="MultiLine" Height="75px" ></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="panel-footer">
                            <div class="form-group">
                                <asp:Button ID="btn_move_out" runat="server" Text="Submit" class="btn btn-primary" ValidationGroup="add"/> 
                                <asp:Button ID="btn_clear" runat="server" Text="Clear" class="btn btn-primary"/> 
                            </div>
                            <div class="form-group">
                                <asp:Label ID="lbl_msg" runat="server" Text="" class="text-warning"></asp:Label>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </ContentTemplate>
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="rcb_catalog" EventName="SelectedIndexChanged" />
    </Triggers>
</asp:UpdatePanel>

</asp:Content>
