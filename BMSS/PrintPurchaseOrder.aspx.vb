﻿Imports Telerik.Reporting

Public Class PrintPurchaseOrder
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("login") Is Nothing Then
            Response.Redirect("Login.aspx")
        Else
            If Not IsPostBack Then
                Dim str As String = Request.QueryString("no")
                Dim raReport As Report = New PrintPO()
                raReport.ReportParameters("popar").Value = str
                ReportViewer1.Report = raReport
            End If
        End If
    End Sub

    Protected Sub btn_print_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_print.Click

    End Sub
End Class