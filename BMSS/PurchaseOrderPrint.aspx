﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="PurchaseOrderPrint.aspx.vb" Inherits="BMSS.PurchaseOrderPrint" %>

<%@ Register Assembly="Telerik.ReportViewer.WebForms, Version=5.1.11.713, Culture=neutral, PublicKeyToken=a9d7983dfcc261be"
    Namespace="Telerik.ReportViewer.WebForms" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<div>
    <p align="center"><b>PURCHASE ORDER PRINT</b></p>
   
    <telerik:ReportViewer ID="ReportViewer1" runat="server" Height="700px" Width="900px"></telerik:ReportViewer>
     <script type="text/javascript">  
        function PrintReport()  
        {  
            <%=ReportViewer1.ClientID %>.PrintReport();  
        }  
     </script> 
</div>
</asp:Content>
