﻿Imports MySql.Data
Imports MySql.Data.MySqlClient

Public Class Login
    Inherits System.Web.UI.Page

    Dim oConn As New MySqlConnection(ConfigurationManager.ConnectionStrings("conn_str").ConnectionString)

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Protected Sub btn_login_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_login.Click
        oConn.Open()
        Dim oDatareader As MySqlDataReader
        Dim sql_check_login As New MySqlCommand("Select * from login_user where username = '" & txt_username.Text & "' and password = '" & txt_password.Text & "' and active = 1", oConn)
        oDatareader = sql_check_login.ExecuteReader()
        If oDatareader.HasRows() Then
            Session("login") = True
            Session("username") = txt_username.Text
            oDatareader.Close()
            Dim sqlCmd As New MySqlCommand("SELECT Group_ID FROM sys_user_group_sc where User_ID = '" & txt_username.Text & "'", oConn)
            oDatareader = sqlCmd.ExecuteReader()
            If oDatareader.HasRows() Then
                If oDatareader.Read() Then
                    Session("profile") = oDatareader("Group_ID").ToString()
                End If
            End If
            oDatareader.Close()
            If txt_password.Text = "1234" Then
                Response.Redirect("~/UpdatePassword.aspx")
            Else
                Response.Redirect("~/Default.aspx")
            End If
        Else
            oDatareader.Close()
            Session("login") = False
            lbl_msg.Text = "Invalid username and password"
        End If
        oConn.Close()
    End Sub
End Class