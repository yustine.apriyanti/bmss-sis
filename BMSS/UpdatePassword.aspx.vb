﻿Imports MySql.Data
Imports MySql.Data.MySqlClient

Public Class UpdatePassword
    Inherits System.Web.UI.Page

    Dim oConn As New MySqlConnection(ConfigurationManager.ConnectionStrings("conn_str").ConnectionString)
    Dim create_by As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("login") Is Nothing Then
            Response.Redirect("Login.aspx")
        Else
            If Not IsPostBack Then

            End If
        End If
    End Sub

    Protected Sub btn_submit_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_submit.Click
        create_by = Session("username").ToString()
        Dim create_date As String = Date.Now.ToString("yyyy-MM-dd HH:mm:ss")
        Try
            oConn.Open()
            Dim sqlCmd As New MySqlCommand("update login_user set password = '" & txt_new_password.Text & "', update_by = UPPER('" & create_by & "'), update_date = '" & create_date & "' where username = '" & Session("username").ToString() & "'", oConn)
            sqlCmd.ExecuteNonQuery()
            oConn.Close()
        Catch ex As Exception
            lbl_msg.Text = ex.Message()
        Finally
            lbl_msg.Text = "Update password success"
        End Try
    End Sub
End Class