﻿Imports Telerik.Web.UI
Imports MySql.Data.MySqlClient
Imports System.Drawing

Public Class ReportTransaction
    Inherits System.Web.UI.Page

    Dim oConn As New MySqlConnection(ConfigurationManager.ConnectionStrings("conn_str").ConnectionString)

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("login") Is Nothing Then
            Response.Redirect("Login.aspx")
        Else
            If Not IsPostBack Then
                ddl_period.SelectedValue = Date.Now.ToString("yyyy-MM")
            End If
        End If
    End Sub

    Protected Sub rg_data_PreRender(ByVal sender As Object, ByVal e As EventArgs) Handles rg_data.PreRender
        For i As Integer = 0 To rg_data.MasterTableView.GroupByExpressions.Count - 1 - 1
            If rg_data.MasterTableView.GroupByExpressions(i).GroupByFields(0).FieldName = "descX" Then
                rg_data.MasterTableView.GroupByExpressions.RemoveAt(i)
            End If
        Next
        Dim field As GridGroupByField = New GridGroupByField()
        field.FieldName = "descX"
        field.HeaderText = "Catalog ID"
        Dim ex As GridGroupByExpression = New GridGroupByExpression()
        ex.GroupByFields.Add(field)
        ex.SelectFields.Add(field)
        rg_data.MasterTableView.GroupByExpressions.Add(ex)
        rg_data.Rebind()

        '<GroupByExpressions>
        '    <telerik:GridGroupByExpression>
        '        <SelectFields>
        '            <telerik:GridGroupByField FieldAlias="descX" FieldName="descX" HeaderText="Catalog ID"></telerik:GridGroupByField>
        '        </SelectFields>
        '        <GroupByFields>
        '            <telerik:GridGroupByField FieldName="descX"></telerik:GridGroupByField>
        '        </GroupByFields>
        '    </telerik:GridGroupByExpression>
        '</GroupByExpressions>
    End Sub

    Protected Sub rg_data_ColumnCreated(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridColumnCreatedEventArgs) Handles rg_data.ColumnCreated
        If TypeOf e.Column Is GridBoundColumn Then
            Dim boundColumn As GridBoundColumn = TryCast(e.Column, GridBoundColumn)
            If boundColumn.DataField = "descX" Then
                boundColumn.Visible = False
            ElseIf boundColumn.DataField = "trans_date" Then
                boundColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center
                boundColumn.HeaderStyle.Width = 85
                boundColumn.HeaderText = "Date"
                boundColumn.ItemStyle.HorizontalAlign = HorizontalAlign.Center
                boundColumn.ItemStyle.Width = 85
            ElseIf boundColumn.DataField = "user_request_name" Then
                boundColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center
                boundColumn.HeaderStyle.Width = 85
                boundColumn.HeaderText = "Name"
                boundColumn.ItemStyle.HorizontalAlign = HorizontalAlign.Center
                boundColumn.ItemStyle.Width = 85
            ElseIf boundColumn.DataField = "price" Then
                boundColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center
                boundColumn.HeaderStyle.Width = 60
                boundColumn.HeaderText = "Price"
                boundColumn.ItemStyle.HorizontalAlign = HorizontalAlign.Center
                boundColumn.ItemStyle.Width = 60
            ElseIf boundColumn.DataField = "stock" Then
                boundColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center
                boundColumn.HeaderStyle.Width = 50
                boundColumn.HeaderText = "STOCK"
                boundColumn.ItemStyle.HorizontalAlign = HorizontalAlign.Center
                boundColumn.ItemStyle.Width = 50
            ElseIf boundColumn.DataField = "RECV" Then
                boundColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center
                boundColumn.HeaderStyle.Width = 50
                boundColumn.HeaderText = "RECV"
                boundColumn.ItemStyle.HorizontalAlign = HorizontalAlign.Center
                boundColumn.ItemStyle.Width = 50
                boundColumn.Aggregate = GridAggregateFunction.Sum
                boundColumn.FooterText = " "
                boundColumn.FooterAggregateFormatString = "{0}"
                boundColumn.FooterStyle.HorizontalAlign = HorizontalAlign.Center
                boundColumn.FooterStyle.Font.Bold = True
            End If
            Dim sqlCmd As New MySqlCommand("SELECT cat_id, code, description, concat(code, ' - ', description) as desX FROM code_list WHERE (cat_id ='DIV') and active = 1 order by seq_no ASC", oConn)
            oConn.Open()
            Dim oDatareader As MySqlDataReader
            oDatareader = sqlCmd.ExecuteReader()
            If oDatareader.HasRows() Then
                While oDatareader.Read()
                    If boundColumn.DataField = oDatareader("code").ToString() Then
                        boundColumn.HeaderStyle.HorizontalAlign = HorizontalAlign.Center
                        boundColumn.HeaderStyle.Width = 50
                        boundColumn.ItemStyle.HorizontalAlign = HorizontalAlign.Center
                        boundColumn.ItemStyle.Width = 50
                        boundColumn.Aggregate = GridAggregateFunction.Sum
                        boundColumn.FooterText = " "
                        boundColumn.FooterAggregateFormatString = "{0}"
                        boundColumn.FooterStyle.HorizontalAlign = HorizontalAlign.Center
                        boundColumn.FooterStyle.Font.Bold = True
                    End If
                End While
            End If
            oDatareader.Close()
            oConn.Close()
        End If
    End Sub

    Protected Sub rg_data_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles rg_data.ItemDataBound
        oConn.Close()
        If (TypeOf e.Item Is GridDataItem) Then
            Dim dataItem As GridDataItem = CType(e.Item, GridDataItem)
            If (dataItem("RECV").Text > 0) Then
                dataItem("RECV").ForeColor = Color.Blue
                dataItem("RECV").Font.Bold = True
            End If
            Dim sqlCmd As New MySqlCommand("SELECT cat_id, code, description, concat(code, ' - ', description) as desX FROM code_list WHERE (cat_id ='DIV') and active = 1 order by seq_no ASC", oConn)
            oConn.Open()
            Dim oDatareader As MySqlDataReader
            oDatareader = sqlCmd.ExecuteReader()
            If oDatareader.HasRows() Then
                While oDatareader.Read()
                    If (dataItem(oDatareader("code").ToString()).Text < 0) Then
                        dataItem(oDatareader("code").ToString()).ForeColor = Color.Red
                        dataItem(oDatareader("code").ToString()).Font.Bold = True
                    End If
                End While
            End If
            oDatareader.Close()
            oConn.Close()
        End If
    End Sub

End Class