﻿Imports MySql.Data.MySqlClient

Public Class StockMoveOut
    Inherits BaseClass

    Dim oConn As New MySqlConnection(ConfigurationManager.ConnectionStrings("conn_str").ConnectionString)
    Dim create_by As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("login") Is Nothing Then
            Response.Redirect("Login.aspx")
        Else
            If Not IsPostBack Then
                txt_ir_no.Text = GenerateNo()
                rd_date.SelectedDate = Date.Now()
                GetTempTableCatalog()
                BindGrid()
            End If
        End If
    End Sub

    Function GenerateNo() As String
        Dim ir_kode As String = "IR"
        Dim kode As Integer
        Dim no As String
        Dim ir_no As String = ""
        Dim now As String = DateTime.Now.ToString("yyMM")
        oConn.Open()
        Dim sqlCmd2 As New MySqlCommand("select * from transaction where trans_type = 'OUT' ", oConn)
        Dim oDatareader As MySqlDataReader
        oDatareader = sqlCmd2.ExecuteReader()
        If oDatareader.HasRows() Then
            oDatareader.Close()
            Dim sqlCmd As New MySqlCommand("SELECT IFNULL(MAX(RIGHT(ir_no,3)),0) + 1, IFNULL(LEFT(ir_no,6), '" & ir_kode & now & "') FROM transaction where ir_no like '" & ir_kode & "%' and LEFT(ir_no,6) = '" & ir_kode & now & "' LIMIT 1", oConn)
            oDatareader = sqlCmd.ExecuteReader()

            If oDatareader.Read() Then
                kode = oDatareader(0).ToString()
                ir_no = oDatareader(1).ToString()
                oDatareader.Close()
            End If
            If ir_no = ir_kode & now Then
                no = Convert.ToInt16(kode.ToString())
            Else
                no = 1
            End If
        Else
            no = 1
        End If
        oConn.Close()

        Return ir_kode & now & Right("00" + no.ToString(), 3)
    End Function

    Sub ClearData()
        txt_ir_no.Text = GenerateNo()
        rd_date.SelectedDate = Date.Now()
        rcb_division.ClearSelection()
        txt_user_req_name.Text = ""
        rcb_catalog.ClearSelection()
        txt_soh.Text = ""
        txt_uoi.Text = ""
        rn_qty.Text = ""
        txt_notes.Text = ""
        lbl_msg.Text = ""
    End Sub

    Sub ClearItem()
        lbl_index.Text = ""
        lbl_stock.Text = ""
        rcb_catalog.Text = ""
        rcb_catalog.ClearSelection()
        txt_soh.Text = ""
        txt_uoi.Text = ""
        rn_qty.Text = ""
    End Sub

    Protected Sub btn_move_out_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_move_out.Click
        Dim item_name As String = ""
        Dim merk As String = ""
        Dim ir_no As String = txt_ir_no.Text
        Dim seq_no, qty_issue, last_stock, stock As Integer
        create_by = Session("username").ToString()
        Dim create_date As String = Date.Now.ToString("yyyy-MM-dd HH:mm:ss")
        Dim trans_date As String = Format(rd_date.SelectedDate, "yyyy-MM-dd")
        Dim oDatareader As MySqlDataReader
        Dim msg As String = ""

        Try
            ViewState("dt") = Session("dt")
            Dim dt As DataTable = TryCast((ViewState("dt")), DataTable)
            Dim dr As DataRow = dt.NewRow()

            If dt.Rows.Count > 0 Then
                oConn.Open()
                For Each dr In dt.Rows
                    Dim catalog_id As String = dr.Field(Of String)("catalog_id")
                    Dim unit As String = dr.Field(Of String)("unit")
                    Dim quantity As Integer = dr.Field(Of Integer)("quantity")

                    Dim sql_catalog As New MySqlCommand("select * from catalog where catalog_id = '" & catalog_id & "'", oConn)
                    oDatareader = sql_catalog.ExecuteReader()
                    If oDatareader.HasRows() Then
                        If oDatareader.Read() Then
                            item_name = oDatareader("item_name").ToString()
                            merk = oDatareader("merk").ToString()
                            last_stock = Integer.Parse(oDatareader("soh").ToString())
                        End If
                    End If
                    oDatareader.Close()

                    Dim sql_seq_no As New MySqlCommand("select IF(ISNULL(seq_no),0, MAX(seq_no)+1) from transaction where catalog_id = '" & catalog_id & "'", oConn)
                    oDatareader = sql_seq_no.ExecuteReader()
                    If oDatareader.HasRows() Then
                        If oDatareader.Read() Then
                            seq_no = Int32.Parse(oDatareader(0).ToString())
                        End If
                    End If
                    oDatareader.Close()

                    If Integer.Parse(quantity) <= last_stock Then
                        qty_issue = Integer.Parse(quantity) * -1
                        stock = last_stock + qty_issue

                        If seq_no = 0 Then
                            Dim sql_insert_ic As New MySqlCommand("INSERT INTO transaction(trans_date, trans_type, seq_no, catalog_id, item_name, unit, quantity, stock, price, merk, create_by, create_date, notes) values(DATE_FORMAT('" & trans_date & "','%Y-%m-%d'), 'IC', 0, '" & catalog_id & "', '" & item_name & "', '" & unit & "', 0, 0, 0, '" & merk & "', UPPER('" & create_by & "'), '" & create_date & "', 'DAFTAR CATALOG BARU')", oConn)
                            sql_insert_ic.ExecuteNonQuery()
                            seq_no = seq_no + 1
                        End If

                        Dim sql_insert As New MySqlCommand("INSERT INTO transaction (trans_date, trans_type, seq_no, catalog_id, item_name, unit, quantity, stock, price, merk, po_no, ir_no, division, user_request_name, notes, create_by, create_date) VALUES ('" & trans_date & "', 'OUT', '" & seq_no & "', '" & catalog_id & "', '" & item_name & "', '" & unit & "', " & qty_issue & ", " & stock & ", null, '" & merk & "', '', '" & ir_no & "', '" & rcb_division.SelectedValue.ToString() & "', UPPER('" & txt_user_req_name.Text & "'), UPPER('" & txt_notes.Text & "'), UPPER('" & create_by & "'), '" & create_date & "')", oConn)
                        sql_insert.ExecuteNonQuery()

                        Dim sql_update As New MySqlCommand("UPDATE catalog SET soh = " & stock & " WHERE catalog_id = '" & catalog_id & "'", oConn)
                        sql_update.ExecuteNonQuery()

                    Else
                        msg = msg & "Stock " & item_name & "not available. "
                    End If
                Next
                GetTempTableCatalog()
                BindGrid()

                oConn.Close()
                ClearData()
                If msg = "" Then
                    lbl_msg.Text = "Insert transaction success"
                    lbl_msg.CssClass = "text-success"
                Else
                    lbl_msg.Text = "Insert transaction success. " & msg
                    lbl_msg.CssClass = "text-warning"
                End If
            Else
                Dim message As String = "Data Item empty"
                RadWindowManager1.RadAlert(message, 300, 100, "Message", "refreshToItemData")
            End If
        Catch ex As Exception
            oConn.Close()
            MessageException("Insert transaction failed", ex, lbl_msg, "text-warning")
        End Try
        oConn.Close()
    End Sub

    Protected Sub btn_clear_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_clear.Click
        GetTempTableCatalog()
        BindGrid()
        ClearData()
    End Sub

    Protected Sub rcb_catalog_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles rcb_catalog.SelectedIndexChanged
        Dim sqlCmd As New MySqlCommand("select * from catalog where catalog_id = '" & rcb_catalog.SelectedValue.ToString() & "' ", oConn)
        oConn.Open()
        Dim oDatareader As MySqlDataReader
        oDatareader = sqlCmd.ExecuteReader()
        If oDatareader.HasRows() Then
            If oDatareader.Read() Then
                txt_soh.Text = Integer.Parse(oDatareader("soh").ToString())
                txt_uoi.Text = oDatareader("uoi").ToString()
            End If
        End If
        oDatareader.Close()
        oConn.Close()
    End Sub

    Protected Sub rg_temp_catalog_ItemCommand(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs) Handles rg_temp_catalog.ItemCommand
        ViewState("dt") = Session("dt")
        Dim dt As DataTable = TryCast((ViewState("dt")), DataTable)
        Dim i As Integer = e.Item.ItemIndex
        lbl_stock.Text = ""

        If e.CommandName = "cancel" Then
            dt.Rows.RemoveAt(i)
            Session("dt") = dt
            BindGrid()
        ElseIf e.CommandName = "choose" Then
            lbl_index.Text = i
            rcb_catalog.Text = ""
            rcb_catalog.ClearSelection()
            rcb_catalog.SelectedValue = dt.Rows(i)("catalog_id")
            rcb_catalog.Text = dt.Rows(i)("description")
            txt_soh.Text = dt.Rows(i)("stock")
            txt_uoi.Text = dt.Rows(i)("unit")
            rn_qty.Text = dt.Rows(i)("quantity")
            btn_update.Visible = True
            btn_addItems.Text = "Cancel"
        End If
    End Sub

    Protected Sub BindGrid()
        rg_temp_catalog.DataSource = TryCast(Session("dt"), DataTable)
        rg_temp_catalog.DataBind()
        ClearItem()
    End Sub

    Protected Sub btn_addItems_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_addItems.Click
        If btn_addItems.Text = "Cancel" Then
            btn_addItems.Text = "Add Item"
            btn_update.Visible = False
            ClearItem()
        Else
            Dim dt As DataTable = TryCast((Session("dt")), DataTable)
            Dim dr As DataRow = dt.NewRow()

            dr("catalog_id") = rcb_catalog.SelectedValue.ToString()
            dr("description") = rcb_catalog.Text
            dr("stock") = txt_soh.Text
            dr("unit") = txt_uoi.Text
            dr("quantity") = rn_qty.Text

            If dr("quantity") <= dr("stock") Then
                dt.Rows.Add(dr)
                Session("dt") = dt
                BindGrid()
            Else
                lbl_stock.Text = "Stock not available"
            End If
        End If
    End Sub

    Protected Sub btn_update_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_update.Click
        If lbl_index.Text <> "" Then
            ViewState("dt") = Session("dt")
            Dim dt As DataTable = TryCast((ViewState("dt")), DataTable)
            Dim i As Integer = lbl_index.Text
            If Integer.Parse(rn_qty.Text) <= dt.Rows(i)("stock") Then
                dt.Rows(i)("catalog_id") = rcb_catalog.SelectedValue.ToString()
                dt.Rows(i)("description") = rcb_catalog.Text
                dt.Rows(i)("stock") = txt_soh.Text
                dt.Rows(i)("unit") = txt_uoi.Text
                dt.Rows(i)("quantity") = rn_qty.Text
                Session("dt") = dt
                BindGrid()
                btn_update.Visible = False
                btn_addItems.Text = "Add Item"
            Else
                lbl_stock.Text = "Stock not available"
            End If
        Else
            lbl_index.Text = ""
        End If
    End Sub
End Class