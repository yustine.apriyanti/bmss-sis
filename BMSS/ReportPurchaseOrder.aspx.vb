﻿Imports Telerik.Web.UI

Public Class ReportPurchaseOrder
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("login") Is Nothing Then
            Response.Redirect("Login.aspx")
        Else
            If Not IsPostBack Then
                ddl_period.SelectedValue = Date.Now.ToString("yyyy-MM")
            End If
        End If
    End Sub

    Protected Sub rg_data_PreRender(ByVal sender As Object, ByVal e As EventArgs) Handles rg_data.PreRender
        For i As Integer = 0 To rg_data.MasterTableView.GroupByExpressions.Count - 1 - 1
            If rg_data.MasterTableView.GroupByExpressions(i).GroupByFields(0).FieldName = "descX" Then
                rg_data.MasterTableView.GroupByExpressions.RemoveAt(i)
            End If
        Next
        Dim field As GridGroupByField = New GridGroupByField()
        field.FieldName = "po_no"
        field.HeaderText = "PO No"
        Dim ex As GridGroupByExpression = New GridGroupByExpression()
        ex.GroupByFields.Add(field)
        ex.SelectFields.Add(field)
        rg_data.MasterTableView.GroupByExpressions.Add(ex)
        rg_data.Rebind()
    End Sub

    Protected Sub rg_data_ColumnCreated(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridColumnCreatedEventArgs) Handles rg_data.ColumnCreated
        If TypeOf e.Column Is GridBoundColumn Then
            Dim boundColumn As GridBoundColumn = TryCast(e.Column, GridBoundColumn)
            If boundColumn.DataField = "po_no" Then
                boundColumn.Visible = False
            End If
        End If
    End Sub
End Class