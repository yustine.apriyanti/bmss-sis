﻿Imports MySql.Data
Imports MySql.Data.MySqlClient

Public Class Site
    Inherits System.Web.UI.MasterPage

    Dim oConn As New MySqlConnection(ConfigurationManager.ConnectionStrings("conn_str").ConnectionString)

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("login") = True Then
            lbl_username.Text = Session("username")
        End If

        If Session("login") Is Nothing Then
            Session.Clear()
            Session.RemoveAll()
            Response.Redirect("LoginForm.aspx")
        End If
    End Sub

    Protected Sub link_logout_Click(ByVal sender As Object, ByVal e As EventArgs) Handles link_logout.Click
        DeleteTemp()
        Session.Clear()
        Session.RemoveAll()
        MySqlConnection.ClearAllPools()
    End Sub

    Sub DeleteTemp()
        oConn.Open()
        Dim sqlCmd2 As New MySqlCommand("delete from temp_po where created_by = '" & Session("username").ToString() & "'", oConn)

        sqlCmd2.ExecuteNonQuery()
        oConn.Close()
        Response.Redirect("LoginForm.aspx")
    End Sub
End Class